#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "JsonUtil.h"
#include "IabCategory.h"
#include "GUtil.h"
#include "DateTimeUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>

IabCategory::IabCategory() {
        id = 0;
}

IabCategory::~IabCategory() {

}

void IabCategory::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "name", name, value);
        JsonUtil::addMemberToValue_FromPair (doc, "iabStandardName", iabStandardName, value);
        JsonUtil::addMemberToValue_FromPair (doc, "createdAt", DateTimeUtil::dateTimeToStr(createdAt), value);
        JsonUtil::addMemberToValue_FromPair (doc, "updatedAt", DateTimeUtil::dateTimeToStr(updatedAt), value);
}

std::shared_ptr<IabCategory> IabCategory::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<IabCategory> iabCategory = std::make_shared<IabCategory>();

        iabCategory->id = JsonUtil::GetIntSafely(value, "id");
        iabCategory->name = JsonUtil::GetStringSafely(value, "name");

        iabCategory->iabStandardName = JsonUtil::GetStringSafely(value, "iabStandardName");
        iabCategory->createdAt = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value, "createdAt"));
        iabCategory->updatedAt = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value, "updatedAt"));
        return iabCategory;
}

std::string IabCategory::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}
