#ifndef IabCategory_h
#define IabCategory_h


#include "JsonTypeDefs.h"

#include <unordered_map>
class DateTimeUtil;
#include "Poco/DateTime.h"
class IabCategory;


class IabCategory {

public:
int id;
std::string name;
std::string iabStandardName;
Poco::DateTime createdAt;
Poco::DateTime updatedAt;

IabCategory();

virtual ~IabCategory();

static std::shared_ptr<IabCategory> fromJson(std::string json);

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

std::shared_ptr<IabCategory> fromJsonValue(RapidJsonValueType value);

std::string toJson();
};

#endif
