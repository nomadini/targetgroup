#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "JsonUtil.h"
#include "IabSubCategory.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
IabSubCategory::IabSubCategory() {
        id = 0;
        parentIabCategoryId = 0;
}

IabSubCategory::~IabSubCategory() {

}

void IabSubCategory::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "name", name, value);
        JsonUtil::addMemberToValue_FromPair (doc, "iabStandardName", iabStandardName, value);
        JsonUtil::addMemberToValue_FromPair (doc, "parentIabCategoryId", parentIabCategoryId, value);

        JsonUtil::addMemberToValue_FromPair (doc, "createdAt", DateTimeUtil::dateTimeToStr(createdAt), value);
        JsonUtil::addMemberToValue_FromPair (doc, "updatedAt", DateTimeUtil::dateTimeToStr(updatedAt), value);
}
std::shared_ptr<IabSubCategory> IabSubCategory::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<IabSubCategory> iabSubCategory = std::make_shared<IabSubCategory>();

        iabSubCategory->id = JsonUtil::GetIntSafely(value, "id");
        iabSubCategory->parentIabCategoryId = JsonUtil::GetIntSafely(value, "parentIabCategoryId");
        iabSubCategory->name = JsonUtil::GetStringSafely(value, "name");

        iabSubCategory->iabStandardName = JsonUtil::GetStringSafely(value, "iabStandardName");
        iabSubCategory->createdAt = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value, "createdAt"));
        iabSubCategory->updatedAt = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value, "updatedAt"));
        return iabSubCategory;
}

std::string IabSubCategory::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}
