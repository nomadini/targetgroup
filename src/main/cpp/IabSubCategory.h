#ifndef IabSubCategory_H
#define IabSubCategory_H


#include "JsonTypeDefs.h"

class DateTimeUtil;
#include <unordered_map>
class IabSubCategory;
#include "Poco/DateTime.h"


class IabSubCategory {

public:
int id;
int parentIabCategoryId;

std::string name;
std::string iabStandardName;
Poco::DateTime createdAt;
Poco::DateTime updatedAt;

IabSubCategory();

virtual ~IabSubCategory();

static std::shared_ptr<IabSubCategory> fromJson(std::string json);

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);

std::shared_ptr<IabSubCategory> fromJsonValue(RapidJsonValueType value);

std::string toJson();
};

#endif
