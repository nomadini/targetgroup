

#include "GUtil.h"
#include "TargetGroup.h"
#include "StringUtil.h"
#include "Creative.h"
#include "MySqlTargetGroupFilterCountDbRecordService.h"
#include "MySqlDriver.h"
#include "PacingPlan.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "CollectionUtil.h"
#include "ConverterUtil.h"
#include "DateTimeUtil.h"
#include <boost/foreach.hpp>


MySqlTargetGroupFilterCountDbRecordService::
MySqlTargetGroupFilterCountDbRecordService(MySqlDriver* driver,
                                           std::string hostname,
                                           std::string appVersion) {
        this->driver = driver;
        this->hostname = hostname;
        this->appVersion = appVersion;
}

void MySqlTargetGroupFilterCountDbRecordService::
insert(std::vector<std::shared_ptr<TargetGroupFilterCountDbRecord> > allObjects) {
        if (allObjects.empty()) {
                return;
        }

        std::string queryStr =
                "INSERT INTO TargetGroupFilterCountDbRecord"
                " ("
                " HOSTNAME,"
                " APP_VERSION,"
                " TARGET_GROUP_ID,"
                " FILTER_NAME,"
                " FILTER_TYPE,"
                " NUM_FAILURES,"
                " NUM_PASSES,"
                " NUM_TOTAL,"
                " NUM_TOTAL_REQUEST,"

                " PERC_FAILURES,"
                " TIME_REPORTED"
                " ) VALUES __VALUES_OF_ALLROW__  ;";


        std::vector<std::string> allValues;

        for(auto obj :  allObjects) {
                auto oneRecordValues = convertRecordToValueString(obj);
                allValues.push_back(oneRecordValues);
                // MLOG(3) << "queryStr : " << queryStr;
        }

        auto properAllValues = StringUtil::joinArrayAsString(allValues, ",");
        queryStr = StringUtil::replaceString (queryStr, "__VALUES_OF_ALLROW__", properAllValues);
        MLOG(10) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);


}
std::string MySqlTargetGroupFilterCountDbRecordService::
convertRecordToValueString(std::shared_ptr<TargetGroupFilterCountDbRecord> record) {

        std::string oneRowValue =
                " "
                " ("
                " '__HOSTNAME__',"
                " '__APP_VERSION__',"
                " '__TARGET_GROUP_ID__',"
                " '__FILTER_NAME__', "
                " '__FILTER_TYPE__', "
                " __NUM_FAILURES__, "
                " __NUM_PASSES__, "
                " __NUM_TOTAL__, "
                " __NUM_TOTAL_REQUEST__, "
                " __PERC_FAILURES__, "
                " '__TIME_REPORTED__' "
                " ) ";


        oneRowValue = StringUtil::replaceString (oneRowValue, "__HOSTNAME__",
                                                 StringUtil::toStr(hostname));

        oneRowValue = StringUtil::replaceString (oneRowValue, "__APP_VERSION__",
                                                 StringUtil::toStr(appVersion));

        oneRowValue = StringUtil::replaceString (oneRowValue, "__TARGET_GROUP_ID__",
                                                 StringUtil::toStr(record->entityId));

        oneRowValue = StringUtil::replaceString (oneRowValue, "__FILTER_NAME__",
                                                 record->filterName);

        oneRowValue = StringUtil::replaceString (oneRowValue, "__FILTER_TYPE__",
                                                 record->filterType);

        oneRowValue = StringUtil::replaceString (oneRowValue, "__NUM_FAILURES__",
                                                 StringUtil::toStr(record->numberOfFailures->getValue()));

        oneRowValue = StringUtil::replaceString (oneRowValue, "__NUM_PASSES__",
                                                 StringUtil::toStr(record->numberOfPasses->getValue()));



        oneRowValue = StringUtil::replaceString (oneRowValue, "__NUM_TOTAL__",
                                                 StringUtil::toStr(record->totalNumberOfCalls->getValue()));

        oneRowValue = StringUtil::replaceString (oneRowValue, "__NUM_TOTAL_REQUEST__",
                                                 StringUtil::toStr(record->totalNumberOfRequest->getValue()));


        oneRowValue = StringUtil::replaceString (oneRowValue, "__PERC_FAILURES__",
                                                 StringUtil::toStr(record->percentageOfFailures->getValue()));

        oneRowValue = StringUtil::replaceString (oneRowValue, "__TIME_REPORTED__", DateTimeUtil::dateTimeToStr(record->timeReported));
        return oneRowValue;
}

std::shared_ptr<TargetGroupFilterCountDbRecord>
MySqlTargetGroupFilterCountDbRecordService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        auto obj  = std::make_shared<TargetGroupFilterCountDbRecord>();
        obj->entityId = MySqlDriver::getString(res, "TARGET_GROUP_ID");
        obj->hostname = MySqlDriver::getString(res, "HOSTNAME");
        obj->appVersion = MySqlDriver::getString(res, "APP_VERSION");
        obj->filterName = MySqlDriver::getString(res, "FILTER_NAME");
        obj->filterType = MySqlDriver::getString(res, "FILTER_TYPE");
        obj->numberOfFailures->setValue(res->getInt ("NUM_FAILURES"));
        obj->numberOfPasses->setValue(res->getInt ("NUM_PASSES"));
        obj->totalNumberOfCalls->setValue(res->getInt ("NUM_TOTAL"));
        obj->totalNumberOfRequest->setValue(res->getInt ("NUM_TOTAL_REQUEST"));
        obj->percentageOfFailures->setValue(res->getInt ("PERC_FAILURES"));
        obj->timeReported = MySqlDriver::parseDateTime(res, "TIME_REPORTED");
        return obj;
}

std::vector<std::shared_ptr<TargetGroupFilterCountDbRecord> >
MySqlTargetGroupFilterCountDbRecordService::readAllByHostInLastNMinutes(std::string hostname, int lastNminute) {
        std::string query = "SELECT "
                            " MAX(ID) AS ID,"
                            " HOSTNAME,"
                            " APP_VERSION,"
                            " TARGET_GROUP_ID,"
                            " FILTER_NAME, "
                            " FILTER_TYPE, "
                            " SUM(NUM_FAILURES) AS NUM_FAILURES, "
                            " SUM(NUM_PASSES) AS NUM_PASSES,"
                            " SUM(NUM_TOTAL) AS NUM_TOTAL, "
                            " SUM(NUM_TOTAL_REQUEST) AS NUM_TOTAL_REQUEST, "
                            " AVG(PERC_FAILURES) AS PERC_FAILURES, "
                            " MAX(TIME_REPORTED) AS TIME_REPORTED "
                            " FROM `TargetGroupFilterCountDbRecord` where "
                            "  TIME_REPORTED >= '__TIME_REPORTED__' and"
                            "  HOSTNAME = '__HOSTNAME__' "
                            "  group by HOSTNAME, APP_VERSION, TARGET_GROUP_ID, FILTER_NAME, FILTER_TYPE";

        query = StringUtil::replaceString (query, "__HOSTNAME__", hostname);

        query = StringUtil::replaceString (query, "__TIME_REPORTED__",
                                           DateTimeUtil::
                                           getNowPlusSecondsInMySqlFormat(lastNminute * -60));
        auto res = driver->executeQuery (query);
        std::vector<std::shared_ptr<TargetGroupFilterCountDbRecord> >  allPerfs;
        while (res->next ()) {
                MLOG(10) << "reading TargetGroupFilterCountDbRecord with id " << res->getInt (1);
                allPerfs.push_back (mapResultSetToObject(res));
        }

        return allPerfs;

}

void MySqlTargetGroupFilterCountDbRecordService::deleteDataOlderThanNDays(int days) {
        std::string query = "DELETE  "
                            " FROM `TargetGroupFilterCountDbRecord` where "
                            "  TIME_REPORTED <= '__TIME_REPORTED__' ";


        query = StringUtil::replaceString (query, "__TIME_REPORTED__",
                                           DateTimeUtil::
                                           getNowPlusDaysInMySqlFormat(-1 * days));

        driver->executedUpdateStatement (query);
        MLOG(3) << "query to delete was : "<<query;
}

MySqlTargetGroupFilterCountDbRecordService::
~MySqlTargetGroupFilterCountDbRecordService() {
}
