#ifndef MySqlTargetGroupFilterCountDbRecordService_h
#define MySqlTargetGroupFilterCountDbRecordService_h


#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "mysql_connection.h"
#include <unordered_map>
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "TargetGroupTestHelper.h"
#include "TargetGroupFilterCountDbRecord.h"

class MySqlTargetGroupFilterCountDbRecordService;




class MySqlTargetGroupFilterCountDbRecordService {

public:
std::string hostname;
std::string appVersion;
MySqlDriver* driver;

MySqlTargetGroupFilterCountDbRecordService(
        MySqlDriver* driver,
        std::string hostname,
        std::string appVersion);

void insert(std::vector<std::shared_ptr<TargetGroupFilterCountDbRecord>> allObjects);
void deleteDataOlderThanNDays(int days);

std::string convertRecordToValueString(std::shared_ptr<TargetGroupFilterCountDbRecord> record);

std::vector<std::shared_ptr<TargetGroupFilterCountDbRecord> > readAllByHostInLastNMinutes(std::string module, int lastNminute);

static std::shared_ptr<TargetGroupFilterCountDbRecord> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);

virtual ~MySqlTargetGroupFilterCountDbRecordService();
};

#endif
