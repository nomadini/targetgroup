#include "GUtil.h"
#include "TargetGroup.h"
#include "Creative.h"
#include "MySqlTargetGroupService.h"
#include "MySqlDriver.h"
#include "PacingPlan.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "CollectionUtil.h"
#include "ConverterUtil.h"
#include "DateTimeUtil.h"
#include <boost/foreach.hpp>

std::string MySqlTargetGroupService::coreSelectFields = " "
                                                        "targetgroup.ID , "
                                                        "targetgroup.NAME, "
                                                        "targetgroup.STATUS,"
                                                        "targetgroup.CAMPAIGN_ID,"
                                                        "targetgroup.MAX_IMPRESSION,"
                                                        "targetgroup.DAILY_MAX_IMPRESSION,"
                                                        "targetgroup.MAX_BUDGET,"
                                                        "targetgroup.DAILY_MAX_BUDGET,"
                                                        "targetgroup.pacing_method,"
                                                        "targetgroup.pacing_speed,"
                                                        "targetgroup.FREQUENCY_IN_SEC,"
                                                        "targetgroup.frequency_unit,"
                                                        "targetgroup.`START_DATE`,"
                                                        "targetgroup.`END_DATE`,"
                                                        "targetgroup.`created_at`,"
                                                        "targetgroup.`updated_at`, "
                                                        "targetgroup.IAB_CATEGORY,"
                                                        "targetgroup.description,"
                                                        "targetgroup.PACING_PLAN,"
                                                        "targetgroup.`AD_POSITION`, "
                                                        "targetgroup.`TYPE_OF_TARGETING`, "
                                                        "targetgroup.`device_type_targets`, "
                                                        "targetgroup.`os_type_targets`, "
                                                        "targetgroup.`NumberOfBidsLimitInInADayPerBidder`, "
                                                        "targetgroup.`NumberOfBidsLimitInLastHourPerBidder`, "
                                                        "targetgroup.`NumberOfBidsAllowedPerMinute` ";

MySqlTargetGroupService::MySqlTargetGroupService(MySqlDriver* driver) {
        this->driver = driver;

}

void MySqlTargetGroupService::deleteAll() {
        driver->deleteAll("targetgroup");
}

void MySqlTargetGroupService::insert(std::shared_ptr<TargetGroup> tg) {
        std::string queryStr =
                " INSERT INTO `targetgroup`"
                " ("
                " `NAME`,"
                " `STATUS`,"
                " `description`,"
                " `IAB_CATEGORY`,"
                " `CAMPAIGN_ID`,"
                " `MAX_IMPRESSION`,"
                " `DAILY_MAX_IMPRESSION`,"
                " `MAX_BUDGET`,"
                " `DAILY_MAX_BUDGET`,"
                " `pacing_method`,"
                " `pacing_speed`,"
                " `PACING_PLAN`,"
                " `FREQUENCY_IN_SEC`,"
                " `frequency_unit`,"
                " `START_DATE`,"
                " `END_DATE`,"
                " `AD_POSITION`,"
                " `TYPE_OF_TARGETING`,"
                " `device_type_targets`,"
                " `os_type_targets`,"
                " `created_at`,"
                " `updated_at`)"
                " VALUES"
                " ("
                " '__NAME__',"
                " '__STATUS__',"
                " '__description__',"
                " '__IAB_CATEGORY__',"
                " __CAMPAIGN_ID__,"
                " __MAX_IMPRESSION__,"
                " __DAILY_MAX_IMPRESSION__,"
                " __MAX_BUDGET__,"
                " __DAILY_MAX_BUDGET__,"
                " '__pacing_method__',"
                " '__pacing_speed__',"
                " '__PACING_PLAN__',"
                " __FREQUENCY_IN_SEC__,"
                " __frequency_unit__,"
                " '__START_DATE__',"
                " '__END_DATE__',"
                " '__AD_POSITION__',"
                " '__TYPE_OF_TARGETING__',"
                " '__device_type_targets__',"
                " '__os_type_targets__',"
                " '__created_at__',"
                " '__updated_at__');";

        MLOG(3) << "inserting new targetGroup in db : " << tg->toJson ();

        queryStr = StringUtil::replaceString (queryStr, "__NAME__",
                                              tg->getName());
        queryStr = StringUtil::replaceString (queryStr, "__STATUS__",
                                              tg->getStatus());
        queryStr = StringUtil::replaceString (queryStr, "__description__",
                                              tg->getDescription());

        queryStr = StringUtil::replaceString (queryStr, "__IAB_CATEGORY__",
                                              JsonArrayUtil::convertArrayOfStringsToJsonArrayString (*tg->getIabCategory()));

        queryStr = StringUtil::replaceString (queryStr, "__CAMPAIGN_ID__",
                                              StringUtil::toStr (tg->getCampaignId()));


        queryStr = StringUtil::replaceString (queryStr, "__MAX_IMPRESSION__",
                                              StringUtil::toStr (tg->getMaxImpression()));
        queryStr = StringUtil::replaceString (queryStr, "__DAILY_MAX_IMPRESSION__",
                                              StringUtil::toStr (tg->getDailyMaxImpression()));
        queryStr = StringUtil::replaceString (queryStr, "__MAX_BUDGET__",
                                              StringUtil::toStr (tg->getMaxBudget()));
        queryStr = StringUtil::replaceString (queryStr, "__DAILY_MAX_BUDGET__",
                                              StringUtil::toStr (tg->getDailyMaxBudget()));

        queryStr = StringUtil::replaceString (queryStr, "__pacing_method__",
                                              tg->pacingMethod);
        queryStr = StringUtil::replaceString (queryStr, "__pacing_speed__",
                                              tg->pacingSpeed);

        queryStr = StringUtil::replaceString (queryStr, "__PACING_PLAN__",
                                              StringUtil::toStr (tg->getPacingPlan()->toJson ()));

        queryStr = StringUtil::replaceString (queryStr, "__FREQUENCY_IN_SEC__",
                                              StringUtil::toStr (tg->getFrequencyInSec()));

        queryStr = StringUtil::replaceString (queryStr, "__frequency_unit__", tg->frequencyUnit);

        queryStr = StringUtil::replaceString (queryStr, "__START_DATE__",
                                              DateTimeUtil::dateTimeToStr(tg->getStartDate()));
        queryStr = StringUtil::replaceString (queryStr, "__END_DATE__",
                                              DateTimeUtil::dateTimeToStr(tg->getEndDate()));

        queryStr = StringUtil::replaceString (queryStr, "__AD_POSITION__",
                                              JsonArrayUtil::convertListToJson (tg->getAdPositions()->getKeys()));

        queryStr = StringUtil::replaceString (queryStr, "__TYPE_OF_TARGETING__",
                                              JsonArrayUtil::convertListToJson (tg->typesOfTargeting->getKeys()));
        queryStr = StringUtil::replaceString (queryStr, "__device_type_targets__",
                                              JsonArrayUtil::convertListToJson (tg->deviceTypeTargets->getKeys()));
        queryStr = StringUtil::replaceString (queryStr, "__os_type_targets__",
                                              JsonArrayUtil::convertListToJson (tg->osTypeTargets->getKeys()));


        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        //MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);
        tg->setId(driver->getLastInsertedId ());

        MLOG(3) << "tg id inserted in db is  : " << tg->getId();

}

std::shared_ptr<TargetGroup> MySqlTargetGroupService::readTargetGroupById(int id) {

        std::shared_ptr<TargetGroup> tg;
        std::string queryStr = "SELECT " +
                               coreSelectFields +
                               " FROM targetgroup JOIN campaign ON targetgroup.CAMPAIGN_ID = campaign.ID "
                               " and  targetgroup.ID = __ID__";





        queryStr = StringUtil::replaceString (queryStr, "__ID__", StringUtil::toStr (id));

        auto res = driver->executeQuery (queryStr);

        while (res->next ()) {
                tg = mapResultSetToTargetGroup (res);
        }



        if (tg == nullptr) {
                throwEx("no tg for id " + StringUtil::toStr(id));
        }
        return tg;
}

std::vector<std::shared_ptr<TargetGroup> > MySqlTargetGroupService::readAllEntities() {
        return readAllTargetGroups();
}

std::vector<std::shared_ptr<TargetGroup> > MySqlTargetGroupService::readAllTargetGroups() {

        std::vector<std::shared_ptr<TargetGroup> > allObjects;
        std::string query =
                "SELECT " + coreSelectFields +" FROM targetgroup join campaign on targetgroup.CAMPAIGN_ID = campaign.ID";


        auto res = driver->executeQuery (query);

        while (res->next ()) {

                MLOG(3) << "targetgroup obj loaded : id " << res->getInt (1);         // getInt(1) returns the first column

                allObjects.push_back (mapResultSetToTargetGroup (res));
        }


        return allObjects;
}


void MySqlTargetGroupService::updateTargetGroupStatus(std::shared_ptr<TargetGroup> tg) {
        MLOG(3) << "updating tg status to  " << tg->getStatus() << " for targetGroupId : " << tg->getId();
        std::string query (
                "UPDATE targetgroup SET STATUS = '__STATUS__' WHERE ID = '__ID__';");
        query = StringUtil::replaceString (query, "__STATUS__", tg->getStatus());
        query = StringUtil::replaceString (query, "__ID__", tg->idAsString);

        driver->executedUpdateStatement (query);

}

std::shared_ptr<TargetGroup> MySqlTargetGroupService::mapResultSetToTargetGroup(std::shared_ptr<ResultSetHolder> res) {
        MLOG(3) << "targetgroup obj loaded : id  " << res->getInt (1); // getInt(1) returns the first column
        std::shared_ptr<TargetGroup> obj  = std::make_shared<TargetGroup> ();
        obj->setId(res->getInt ("id"));
        obj->idAsString = "tg" + StringUtil::toStr(obj->getId());

        obj->setName(res->getString ("NAME"));
        obj->setStatus (StringUtil::toLowerCase (res->getString ("STATUS")));
        obj->setCampaignId(res->getInt ("CAMPAIGN_ID"));
        obj->setMaxImpression(res->getDouble ("MAX_IMPRESSION"));
        obj->setDailyMaxImpression(res->getInt ("DAILY_MAX_IMPRESSION"));
        obj->setMaxBudget(res->getInt ("MAX_BUDGET"));
        obj->setDailyMaxBudget(res->getInt ("DAILY_MAX_BUDGET"));

        int frequInSecond = res->getInt ("FREQUENCY_IN_SEC");
        std::string frequencyUnit = res->getString ("frequency_unit");
        obj->pacingMethod = res->getString ("pacing_method");
        if (obj->pacingMethod.empty() || StringUtil::equalsIgnoreCase("null", obj->pacingMethod)) {
                obj->pacingMethod = TargetGroup::PACING_METHOD_HYBRID;
        }

        obj->pacingSpeed = res->getString ("pacing_speed");
        if (obj->pacingSpeed.empty() || StringUtil::equalsIgnoreCase("null", obj->pacingSpeed)) {
                obj->pacingSpeed = TargetGroup::PACING_SPEED_EVEN;
        }
        if (StringUtil::equalsIgnoreCase(frequencyUnit, "minute")) {
                frequInSecond = frequInSecond * 60;
        } else if(StringUtil::equalsIgnoreCase(frequencyUnit, "hour")) {
                frequInSecond = frequInSecond * 60 * 60;
        } else if(StringUtil::equalsIgnoreCase(frequencyUnit, "day")) {
                frequInSecond = frequInSecond * 60 * 60 * 24;
        }
        obj->frequencyUnit = frequencyUnit;
        obj->setFrequencyInSec(frequInSecond);
        obj->setStartDate(DateTimeUtil::parseDateTime(res->getString ("START_DATE")));
        obj->setEndDate(DateTimeUtil::parseDateTime(res->getString ("END_DATE")));
        obj->setCreatedAt(DateTimeUtil::parseDateTime(res->getString ("created_at")));
        obj->setUpdatedAt(DateTimeUtil::parseDateTime(res->getString ("updated_at")));

        auto iabCategories = res->getString ("IAB_CATEGORY");
        if (iabCategories.empty() || StringUtil::equalsIgnoreCase("null", iabCategories)) {
                iabCategories = "[]";
        }
        parseAndSetIabCategories(iabCategories, obj);

        obj->setDescription(res->getString ("description"));

        std::string rawPacingPlanFromDb(res->getString ("PACING_PLAN"));
        parseAndSetPacingPlan(rawPacingPlanFromDb, obj);

        std::string adPositionsFromDb(res->getString ("AD_POSITION"));
        parseAndSetMaps(adPositionsFromDb,
                        "ANY",
                        obj->adPositions);

        std::string typesOfTargetingFromDb(res->getString ("TYPE_OF_TARGETING"));
        parseAndSetMaps(
                typesOfTargetingFromDb,
                TargetGroup::TARGETTING_TYPE_SEGMENT_TARGETING,
                obj->typesOfTargeting);

        std::string deviceTypeTargetsFromDb(res->getString ("device_type_targets"));
        parseAndSetMaps(
                deviceTypeTargetsFromDb,
                "[]",
                obj->deviceTypeTargets);

        std::string osTypeTargetsFromDb(res->getString ("os_type_targets"));
        parseAndSetMaps(
                osTypeTargetsFromDb,
                "[]",
                obj->osTypeTargets);

        obj->getNumberOfBidsLimitInInADayPerBidder()->setValue(res->getInt ("NumberOfBidsLimitInInADayPerBidder"));
        obj->getNumberOfBidsLimitInLastHourPerBidder()->setValue(res->getInt ("NumberOfBidsLimitInLastHourPerBidder"));
        obj->setNumberOfBidsAllowedPerMinute(res->getInt ("NumberOfBidsAllowedPerMinute"));

        MLOG(3) << "this is the loaded tg : " << obj->toJson ();
        obj->validate ();
        return obj;

}


void MySqlTargetGroupService::parseAndSetIabCategories(std::string iabCategoryIdRaw, std::shared_ptr<TargetGroup> obj) {
        /*
           this is a sample of iab categories
           [{"id":"IAB1","text":"Arts & Entertainment","nodeId":0},{"id":"IAB1-1","text":"Books & Literature","nodeId":1},{"id":"IAB2","text":"Automotive","nodeId":8},{"id":"IAB2-1","text":"Auto Parts","nodeId":9},{"id":"IAB2-2","text":"Auto Repair","nodeId":10},{"id":"IAB2-3","text":"Buying/Selling Cars","nodeId":11},{"id":"IAB2-4","text":"Car Culture","nodeId":12},{"id":"IAB2-5","text":"Certified Pre-Owned","nodeId":13},{"id":"IAB2-6","text":"Convertible","nodeId":14}]
         */
        //
        MLOG(2)<< "iabCategoryIdRaw : "<< iabCategoryIdRaw;
        auto doc = parseJsonSafely(iabCategoryIdRaw);
        assertAndThrow(doc->IsArray ());
        for (rapidjson::SizeType i = 0; i < doc->Size (); i++) {
                RapidJsonValueType value = (*doc)[i];
                std::string id = JsonUtil::GetStringSafely (value, "id");
                std::string name = JsonUtil::GetStringSafely (value, "text");
                if(!StringUtil::containsCaseInSensitive(id, "-")) {
                        //name has a '-'  so it is a subCategory
                        obj->setIabSubCategory(id);
                } else {
                        obj->setIabCategory(id);
                }

                if(!StringUtil::containsCaseInSensitive(id, "iab")) {

                        throwEx("wrong id found for iabCategories " + id + ", tg id : "+ _toStr(obj->id));
                }
        }
}

void MySqlTargetGroupService::parseAndSetMaps(
        std::string arrayOfValues,
        std::string defaultValue,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicBoolean> > map) {

        MLOG(3) << "arrayOfValues read from db: "<< arrayOfValues;

        if (arrayOfValues.empty () || StringUtil::equalsIgnoreCase(arrayOfValues, "null")) {
                arrayOfValues = "[\"" + defaultValue + "\"]";
                MLOG(0) << "arrayOfValues was empty so setting it to " << arrayOfValues;
        }

        std::vector<std::string> values;
        JsonArrayUtil::getArrayOfObjectsFromJsonString (arrayOfValues, values);
        for(std::string value :  values) {
                auto val = std::make_shared<gicapods::AtomicBoolean>();
                map->put(value, val);
        }
}

void MySqlTargetGroupService::parseAndSetPacingPlan(
        std::string rawPacingPlanFromDb,
        std::shared_ptr<TargetGroup> obj) {
        if (rawPacingPlanFromDb.empty ()) {

                LOG(WARNING) << "rawPacingPlanFromDb is empty, using the hard coded one";
                rawPacingPlanFromDb = StringUtil::toStr (PacingPlan::defaultPlanInJson);
        }
        MLOG(3) << "rawPacingPlanFromDb " << rawPacingPlanFromDb;
        obj->setPacingPlan(PacingPlan::fromJson (rawPacingPlanFromDb));
}


MySqlTargetGroupService::~MySqlTargetGroupService() {
}
