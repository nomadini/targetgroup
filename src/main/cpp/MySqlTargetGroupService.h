#ifndef MySqlTargetGroupService_h
#define MySqlTargetGroupService_h


#include "TargetGroupTypeDefs.h"
#include "mysql_connection.h"
#include <unordered_map>
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "TargetGroupTestHelper.h"
#include "DataProvider.h"
#include "ConcurrentHashMap.h"
#include "AtomicBoolean.h"

class MySqlTargetGroupService;




class MySqlTargetGroupService : public DataProvider<TargetGroup> {

public:

MySqlDriver* driver;

MySqlTargetGroupService(MySqlDriver* driver);

static std::string coreSelectFields;

std::shared_ptr<TargetGroup> readTargetGroupById(int id);

virtual ~MySqlTargetGroupService();

void updateTargetGroupStatus(std::shared_ptr<TargetGroup> tg);

std::vector<std::shared_ptr<TargetGroup> > readAllTargetGroups();

void insert(std::shared_ptr<TargetGroup> tg);

virtual void deleteAll();

std::shared_ptr<TargetGroup> mapResultSetToTargetGroup(std::shared_ptr<ResultSetHolder> res);

std::vector<std::shared_ptr<TargetGroup> > readAllEntities();

void parseAndSetPacingPlan(std::string rawPacingPlanFromDb, std::shared_ptr<TargetGroup> obj);

void parseAndSetIabCategories(std::string iabCategoriesFromDb, std::shared_ptr<TargetGroup> obj);

void parseAndSetMaps(std::string arrayOfValues,
                     std::string defaultValue,
                     std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicBoolean> > map);

};

#endif
