#include "GUtil.h"
#include "JsonArrayUtil.h"
#include "JsonUtil.h"
#include "PacingPlan.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>

/*
   "{\"timeZone\" :\"America/New_York\", \"hourPercentages\" : [ {\"8\":30}, {\"12\":50},  {\"18\":80}  , {\"22\":100} ]}";
   a pacing plan like above means that user can spend up to 30 percent of its budget or impressions in the first 8 hours.
   user can spend up to 50 percent of impressions or budget  in the first 12 hours (hour is inclusive here Aka closed)
   user can spend up to 80 percent of impressions or budget in the first 18 hours and so on.
 */
PacingPlan::PacingPlan() {

}

PacingPlan::~PacingPlan() {

}

bool fillMapWithHour(std::shared_ptr<PacingPlan> pacingPlan,int hour, int hourCandidate) {
        auto pairPtr = pacingPlan->hourToCumulativePercentageLimit.find (hourCandidate);
        if(pairPtr != pacingPlan->hourToCumulativePercentageLimit.end()) {
                //found the candidate
                pacingPlan->hourToCumulativePercentageLimit.insert(std::make_pair(hour, pairPtr->second));
                return true;
        } else {
                if (hourCandidate > 23) {
                        pacingPlan->hourToCumulativePercentageLimit.insert(std::make_pair(hour, 0));
                        return true;
                }
        }
        return false;
}

void PacingPlan::set(int limit, int hourArg) {
        if (hourArg > 24) {
                throwEx("wrong argument for hour");
        }

        if (limit < 0) {
                throwEx("wrong argument for limit");
        }
        hourToCumulativePercentageLimit.insert (std::make_pair(hourArg, limit));
}


std::shared_ptr<PacingPlan> PacingPlan::fromJson(std::string json) {
        MLOG(10) << "pacing plan to convert to json : " << json;

        std::shared_ptr<PacingPlan> plan  = std::make_shared<PacingPlan> ();
        //this is the default pacing plan in db  "{\"timeZone\" :\"America/New_York\", \"hourPercentages\" : [ {\"8\":30}, {\"12\":50},  {\"18\":80}  , {\"22\":100} ]}";
        DocumentPtr doc = parseJsonSafely(json);
        if(!doc->HasMember("timeZone") || !((*doc)["timeZone"].IsString())) {
                throwEx("timeZone is required for pacing plan");
        }
        plan->timeZone = (*doc)["timeZone"].GetString();
        RapidJsonValueTypeNoRef hourPercentages;
        if (doc->HasMember("hourPercentages")) {
                hourPercentages = (*doc)["hourPercentages"];
        }else {
                throwEx("hourPercentages is required in pacingPlan");
        }

        std::vector<int> foundHours;
        assertAndThrow(hourPercentages.IsArray ());
        for (rapidjson::SizeType i = 0; i < hourPercentages.Size (); i++) // rapidjson uses SizeType instead of size_t.
        {
                for (auto m = hourPercentages[i].MemberBegin ();
                     m != hourPercentages[i].MemberEnd (); ++m) {
                        if (m->name.IsString () && m->value.IsInt ()) {
                                //   MLOG(10)<<"m->name : "<< m->name.GetString() << "   m->value : " <<  m->value.GetInt();
                                int hour = ConverterUtil::convertTo<int> (m->name.GetString());
                                plan->hourToCumulativePercentageLimit.insert (std::make_pair(hour, m->value.GetInt())); //this is percentage
                        } else {
                                throwEx("pacing plan doesnt have string name or int value :  " + json);
                        }
                }
        }

        for ( int hour = 0; hour < 24; hour++) {
                while(plan->hourToCumulativePercentageLimit.find(hour) == plan->hourToCumulativePercentageLimit.end()) {
                        //hour is lacking
                        int hourCandidate = hour + 1;
                        bool mapIsFilled = false;
                        do {
                                mapIsFilled = fillMapWithHour(plan, hour, hourCandidate);
                                hourCandidate++;
                        } while(mapIsFilled == false);
                }


        }

        for ( int hour = 23; hour >=0; hour--) {
                auto pair = plan->hourToCumulativePercentageLimit.find(hour);
                if(pair->second != 100) {
                        pair->second = 100;
                } else {
                        break;
                }
        }


        for ( int hour = 0; hour < 24; hour++) {
                auto pair = plan->hourToCumulativePercentageLimit.find(hour);
                //   MLOG(10) << "hour : " << pair->first << " , perc : " << pair->second;
        }

        // MLOG(10) << "this is the pacing plan constructed in json :" << plan->toJson ();
        return plan;
}

std::string PacingPlan::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "timeZone",
                                            timeZone, value);

        if (this->hourToCumulativePercentageLimit.size () != 24) {
                throwEx(
                        "hours are not complete");
        }

        RapidJsonValueTypeNoRef myArray (rapidjson::kArrayType);
        for (int i = 0; i < 24; i++) {
                RapidJsonValueTypeNoRef objectValue (rapidjson::kObjectType);

                auto pairPtr = this->hourToCumulativePercentageLimit.find (i);

                JsonUtil::addMemberToValue_FromPair (doc.get(),
                                                     StringUtil::toStr (pairPtr->first),
                                                     pairPtr->second,
                                                     objectValue);

                JsonArrayUtil::addMemberToArray(doc.get(), myArray,  objectValue);

        }

        JsonUtil::addMemberToValue_FromPair(doc.get(),
                                            "hourPercentages",
                                            myArray,
                                            value);
        return JsonUtil::valueToString (value);
}
