#ifndef PacingPlan_h
#define PacingPlan_h




#include <unordered_map>
#include <memory>
#include <vector>
#include <memory>
class PacingPlan;



class PacingPlan {

public:
/*
    hour represents the hour and cumulativeImpressionLimitByHour is the limit
    that we can show number of impressions by that hour in UTC.
 */
std::unordered_map<int, int> hourToCumulativePercentageLimit;
std::string timeZone; //by default the time zone is America/New_York
static constexpr const char* defaultPlanInJson= "{\"timeZone\" :\"America/New_York\", \"hourPercentages\" : [ {\"8\":30}, {\"12\":50},  {\"18\":80}  , {\"22\":100} ]}";

PacingPlan();

void set(int limit, int hourArg);


~PacingPlan();

static std::shared_ptr<PacingPlan> fromJson(std::string json);

std::string toJson();
};

#endif
