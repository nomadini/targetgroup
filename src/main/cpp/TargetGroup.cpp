#include "PacingPlan.h"
#include "TargetGroup.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "TestUtil.h"
#include "StringUtil.h"

#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormat.h"
#include "DateTimeUtil.h"

#include "CollectionUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>

std::string TargetGroup::TARGETTING_TYPE_SEGMENT_TARGETING = "SEGMENT_TARGETING";
std::string TargetGroup::TARGETTING_TYPE_SEGMENTLESS_TARGETING = "SEGMENTLESS_TARGETING";
std::string TargetGroup::TARGETTING_TYPE_GEO_FEATURE_TARGETING = "GEO_FEATURE_TARGETING";
std::string TargetGroup::TARGETTING_TYPE_RECENCY_TARGETING = "RECENCY_TARGETING";
std::string TargetGroup::TARGETTING_TYPE_TOP_MGRS_TARGETING = "TOP_MGRS_TARGETING";

std::string TargetGroup::PACING_METHOD_BUDGET = "budget";
std::string TargetGroup::PACING_METHOD_IMPRESSION = "impression";
std::string TargetGroup::PACING_METHOD_HYBRID = "hybrid";
std::string TargetGroup::PACING_SPEED_EVEN = "even";
std::string TargetGroup::PACING_SPEED_ASAP = "asap";

std::string TargetGroup::getEntityName() {
        static std::string entityName = "targetgroup";
        return entityName;
}

int TargetGroup::getClosestMaxHourIndexToNow() {
        throwEx("remove the function later");

}

TargetGroup::TargetGroup() {
        pacingPlan = std::make_shared<PacingPlan> ();
        fakeTargetGroupForValidationPurposes = false;

        adPositions = std::make_shared<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicBoolean> >();
        typesOfTargeting = std::make_shared<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicBoolean> >();
        deviceTypeTargets = std::make_shared<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicBoolean> >();
        osTypeTargets = std::make_shared<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicBoolean> >();

        id = 0;
        campaignId = 0;
        maxImpression = 0;
        dailyMaxImpression = 0;
        maxBudget = 0;
        dailyMaxBudget = 0;
        frequencyInSec = 0;
        numberOfBidsAllowedPerMinute = 100;//by default we can make 1000 bids for a tg,  change it for different campaign sizes!!

        numberOfBidsLimitInLastHourPerBidder = std::make_shared<gicapods::AtomicLong>();
        numberOfBidsLimitInInADayPerBidder = std::make_shared<gicapods::AtomicLong>();

        iabCategory = std::make_shared<std::vector<std::string> >();
        iabSubCategory = std::make_shared<std::vector<std::string> > ();
        idAsString = "notInitialized";
        pacingBasedHourlyCalculatedImpressionLimit = std::make_shared<gicapods::AtomicLong>();
        pacingBasedHourlyCalculatedBudgetLimit = std::make_shared<gicapods::AtomicDouble>();
}

void TargetGroup::validate() {
        assertAndThrow(id > 0);

        TestUtil::assertNotEmpty(name);
        TestUtil::assertNotEmpty(status);
        TestUtil::assertNotEmpty(frequencyUnit);

        assertAndThrow(campaignId > 0);

        if (StringUtil::equalsIgnoreCase(TargetGroup::PACING_METHOD_IMPRESSION, pacingMethod) ||
            StringUtil::equalsIgnoreCase(TargetGroup::PACING_METHOD_HYBRID, pacingMethod)) {
                assertAndThrow(maxImpression > 0);
                assertAndThrow(dailyMaxImpression > 0);

                assertAndThrow(maxImpression < 10000000); //sanity checks
                assertAndThrow(dailyMaxImpression < 10000000);//sanity checks
        }

        if (StringUtil::equalsIgnoreCase(TargetGroup::PACING_METHOD_BUDGET, pacingMethod) ||
            StringUtil::equalsIgnoreCase(TargetGroup::PACING_METHOD_HYBRID, pacingMethod)) {
                assertAndThrow(maxBudget > 0);
                assertAndThrow(dailyMaxBudget > 0);

                assertAndThrow(maxBudget < 100000); //sanity checks
                assertAndThrow(dailyMaxBudget < 100000);//sanity checks
        }

        TestUtil::assertNotNull(pacingPlan.get ());
        assertAndThrow(frequencyInSec > 0);

}

std::string TargetGroup::toString() {
        return this->toJson ();
}


void TargetGroup::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "name", name, value);
        JsonUtil::addMemberToValue_FromPair (doc, "status", status, value);
        JsonUtil::addMemberToValue_FromPair (doc, "frequencyUnit", frequencyUnit, value);
        JsonUtil::addMemberToValue_FromPair (doc, "description", description, value);
        JsonUtil::addMemberToValue_FromPair (doc, "pacingMethod", pacingMethod, value);
        JsonUtil::addMemberToValue_FromPair (doc, "pacingSpeed", pacingSpeed, value);
        JsonUtil::addMemberToValue_FromPair (doc, "campaignId", campaignId, value);

        JsonArrayUtil::addMemberToValue_FromPair(doc, "iabCategories", *iabCategory, value);
        JsonArrayUtil::addMemberToValue_FromPair(doc, "iabSubCategories", *iabSubCategory, value);
        JsonUtil::addMemberToValue_FromPair (doc, "maxImpression", maxImpression, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dailyMaxImpression", dailyMaxImpression, value);
        JsonUtil::addMemberToValue_FromPair (doc, "maxBudget", maxBudget, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dailyMaxBudget", dailyMaxBudget, value);
        JsonUtil::addMemberToValue_FromPair (doc, "frequencyInSec", frequencyInSec, value);

        JsonUtil::addMemberToValue_FromPair (doc, "startDate", DateTimeUtil::dateTimeToStr(startDate), value);
        JsonUtil::addMemberToValue_FromPair (doc, "endDate", DateTimeUtil::dateTimeToStr(endDate), value);

        JsonUtil::addMemberToValue_FromPair (doc, "createdAt", DateTimeUtil::dateTimeToStr(createdAt), value);
        JsonUtil::addMemberToValue_FromPair (doc, "updatedAt", DateTimeUtil::dateTimeToStr(updatedAt), value);

        JsonUtil::addMemberToValue_FromPair (doc, "pacingPlan", pacingPlan->toJson (), value);

        JsonUtil::addMemberToValue_FromPair (doc, "numberOfBidsLimitInLastHourPerBidder", numberOfBidsLimitInLastHourPerBidder->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "numberOfBidsLimitInInADayPerBidder", numberOfBidsLimitInInADayPerBidder->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "numberOfBidsAllowedPerMinute", numberOfBidsAllowedPerMinute, value);

        JsonArrayUtil::addMemberToValue_FromPair (doc, "adPositions", adPositions->getKeys(), value);
        JsonArrayUtil::addMemberToValue_FromPair (doc, "typesOfTargeting", typesOfTargeting->getKeys(), value);
        JsonArrayUtil::addMemberToValue_FromPair (doc, "deviceTypeTargets", deviceTypeTargets->getKeys(), value);
        JsonArrayUtil::addMemberToValue_FromPair (doc, "osTypeTargets", osTypeTargets->getKeys(), value);

        JsonUtil::addMemberToValue_FromPair (doc, "pacingBasedHourlyCalculatedImpressionLimit", pacingBasedHourlyCalculatedImpressionLimit->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "pacingBasedHourlyCalculatedBudgetLimit", pacingBasedHourlyCalculatedBudgetLimit->getValue(), value);


}

std::shared_ptr<TargetGroup> TargetGroup::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<TargetGroup> targetGroup = std::make_shared<TargetGroup>();

        targetGroup->setId(JsonUtil::GetIntSafely(value, "id"));
        targetGroup->idAsString = StringUtil::toStr(targetGroup->getId());
        targetGroup->setName(JsonUtil::GetStringSafely(value, "name"));

        targetGroup->setDescription(JsonUtil::GetStringSafely(value,"description"));
        targetGroup->pacingMethod = JsonUtil::GetStringSafely(value,"pacingMethod");
        targetGroup->pacingSpeed = JsonUtil::GetStringSafely(value,"pacingSpeed");
        targetGroup->setCampaignId(JsonUtil::GetIntSafely(value,"campaignId"));
        targetGroup->setStatus(JsonUtil::GetStringSafely(value,"status"));

        targetGroup->frequencyUnit = JsonUtil::GetStringSafely(value,"frequencyUnit");

        targetGroup->setCreatedAt (DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"createdAt")));
        targetGroup->setUpdatedAt (DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"updatedAt")));

        targetGroup->setIabCategories (JsonArrayUtil::getArrayOfStringsFromMemberInValue (value, "iabCategories"));

        targetGroup->setIabSubCategories (JsonArrayUtil::getArrayOfStringsFromMemberInValue (value, "iabSubCategories"));

        targetGroup->setMaxImpression (JsonUtil::GetLongSafely(value,"maxImpression"));
        targetGroup->setDailyMaxImpression (JsonUtil::GetLongSafely(value,"dailyMaxImpression"));
        targetGroup->setMaxBudget (JsonUtil::GetDoubleSafely(value,"maxBudget"));
        targetGroup->setDailyMaxBudget (JsonUtil::GetDoubleSafely(value,"dailyMaxBudget"));

        targetGroup->setFrequencyInSec (JsonUtil::GetIntSafely(value,"frequencyInSec"));

        targetGroup->setStartDate (DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"startDate")));
        targetGroup->setEndDate (DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"endDate")));


        auto pacingPlan = PacingPlan::fromJson (JsonUtil::GetStringSafely(value,"pacingPlan"));
        targetGroup->setPacingPlan (pacingPlan);


        targetGroup->getNumberOfBidsLimitInLastHourPerBidder()->setValue(JsonUtil::GetLongSafely(value, "numberOfBidsLimitInLastHourPerBidder"));
        targetGroup->getNumberOfBidsLimitInInADayPerBidder()->setValue(JsonUtil::GetLongSafely(value, "numberOfBidsLimitInInADayPerBidder"));
        targetGroup->setNumberOfBidsAllowedPerMinute(JsonUtil::GetIntSafely(value, "numberOfBidsAllowedPerMinute"));


        auto adPositions = JsonArrayUtil::getArrayOfStringsFromMemberInValue (value, "adPositions");
        for (std::string pos :  adPositions) {
                auto val = std::make_shared<gicapods::AtomicBoolean>();
                targetGroup->getAdPositions()->put(pos, val);
        }


        auto typesOfTargeting = JsonArrayUtil::getArrayOfStringsFromMemberInValue (value, "typesOfTargeting");
        for (std::string pos :  typesOfTargeting) {
                auto val = std::make_shared<gicapods::AtomicBoolean>();
                targetGroup->typesOfTargeting->put(pos, val);
        }

        auto deviceTypeTargets = JsonArrayUtil::getArrayOfStringsFromMemberInValue (value, "deviceTypeTargets");
        for (std::string pos :  deviceTypeTargets) {
                auto val = std::make_shared<gicapods::AtomicBoolean>();
                targetGroup->deviceTypeTargets->put(pos, val);
        }

        auto osTypeTargets = JsonArrayUtil::getArrayOfStringsFromMemberInValue (value, "osTypeTargets");
        for (std::string pos :  osTypeTargets) {
                auto val = std::make_shared<gicapods::AtomicBoolean>();
                targetGroup->osTypeTargets->put(pos, val);
        }

        targetGroup->pacingBasedHourlyCalculatedImpressionLimit->setValue(JsonUtil::GetLongSafely(value, "pacingBasedHourlyCalculatedImpressionLimit"));
        targetGroup->pacingBasedHourlyCalculatedBudgetLimit->setValue(JsonUtil::GetDoubleSafely(value, "pacingBasedHourlyCalculatedBudgetLimit"));


        return targetGroup;
}
std::string TargetGroup::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}

TargetGroup::~TargetGroup() {

}


int TargetGroup::getId() {
        return id;
}

void TargetGroup::setId(int id) {
        this->id = id;
}

std::string TargetGroup::getName() {
        return name;
}

void TargetGroup::setName(std::string name) {
        this->name = name;
}

std::string TargetGroup::getStatus() {
        return status;
}

void TargetGroup::setStatus(std::string status) {
        this->status = StringUtil::toLowerCase (status);
}

std::shared_ptr<PacingPlan> TargetGroup::getPacingPlan() {
        return pacingPlan;
}
std::string TargetGroup::getDescription() {
        return description;
}

void TargetGroup::setDescription(std::string description) {
        this->description = description;
}

std::shared_ptr<std::vector<std::string> > TargetGroup::getIabCategory() {
        return iabCategory;
}

void TargetGroup::setIabCategory(std::string iabCategory) {
        this->iabCategory->push_back (StringUtil::toLowerCase (iabCategory));
}

std::shared_ptr<std::vector<std::string> > TargetGroup::getIabSubCategory() {
        return iabSubCategory;
}

void TargetGroup::setIabSubCategory(std::string iabSubCategory) {
        this->iabSubCategory->push_back(StringUtil::toLowerCase (iabSubCategory));
}

void TargetGroup::setIabCategories(std::vector<std::string> iabCategories) {

        this->iabSubCategory->clear ();
        for(std::string cat :  iabCategories) {
                this->iabCategory->push_back (cat);
        }
}

void TargetGroup::setIabSubCategories(std::vector<std::string> iabSubCategories) {
        this->iabSubCategory->clear ();
        for(std::string cat :  iabSubCategories) {
                this->iabSubCategory->push_back (cat);
        }

}

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicBoolean> > TargetGroup::getAdPositions() {
        return adPositions;
}

void TargetGroup::setAdPositions(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicBoolean> > adPositions) {
        this->adPositions = adPositions;
}

int TargetGroup::getCampaignId() {
        return campaignId;
}

void TargetGroup::setCampaignId(int campaignId) {
        this->campaignId = campaignId;
}

long TargetGroup::getMaxImpression() {
        return maxImpression;
}

void TargetGroup::setMaxImpression(long maxImpression) {
        this->maxImpression = maxImpression;
}

long TargetGroup::getDailyMaxImpression() {
        return dailyMaxImpression;
}

void TargetGroup::setDailyMaxImpression(long dailyMaxImpression) {
        this->dailyMaxImpression = dailyMaxImpression;
}

double TargetGroup::getMaxBudget() {
        return maxBudget;
}

void TargetGroup::setMaxBudget(double maxBudget) {
        this->maxBudget = maxBudget;
}

double TargetGroup::getDailyMaxBudget() {
        return dailyMaxBudget;
}

void TargetGroup::setDailyMaxBudget(double dailyMaxBudget) {
        this->dailyMaxBudget = dailyMaxBudget;
}

void TargetGroup::setPacingPlan(std::shared_ptr<PacingPlan> pacingPlan) {
        this->pacingPlan = pacingPlan;
}

int TargetGroup::getFrequencyInSec() {
        return frequencyInSec;
}

void TargetGroup::setFrequencyInSec(int frequencyInSec) {
        this->frequencyInSec = frequencyInSec;
}

Poco::DateTime TargetGroup::getStartDate() {
        return startDate;
}

void TargetGroup::setStartDate(Poco::DateTime startDate) {
        this->startDate = startDate;
}

Poco::DateTime TargetGroup::getEndDate() {
        return endDate;
}

void TargetGroup::setEndDate(Poco::DateTime endDate) {
        this->endDate = endDate;
}

Poco::DateTime TargetGroup::getCreatedAt() {
        return createdAt;
}

void TargetGroup::setCreatedAt(Poco::DateTime createdAt) {
        this->createdAt = createdAt;
}

Poco::DateTime TargetGroup::getUpdatedAt() {
        return updatedAt;
}

void TargetGroup::setUpdatedAt(Poco::DateTime updatedAt) {
        this->updatedAt = updatedAt;
}


void TargetGroup::setNumberOfBidsLimitInLastHourPerBidder(std::shared_ptr<gicapods::AtomicLong> numberOfBidsLimitInLastHourPerBidder) {
        this->numberOfBidsLimitInLastHourPerBidder = numberOfBidsLimitInLastHourPerBidder;
}

std::shared_ptr<gicapods::AtomicLong> TargetGroup::getNumberOfBidsLimitInInADayPerBidder() {
        return this->numberOfBidsLimitInInADayPerBidder;
}

int TargetGroup::getNumberOfBidsAllowedPerMinute() {
        return numberOfBidsAllowedPerMinute;
}

void TargetGroup::setNumberOfBidsAllowedPerMinute(int numberOfBidsAllowedPerMinute) {
        this->numberOfBidsAllowedPerMinute = numberOfBidsAllowedPerMinute;
}


std::shared_ptr<gicapods::AtomicLong> TargetGroup::getNumberOfBidsLimitInLastHourPerBidder() {
        return numberOfBidsLimitInLastHourPerBidder;
}
