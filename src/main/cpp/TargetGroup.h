#ifndef TargetGroup_h
#define TargetGroup_h


#include "PacingPlan.h"
#include "AtomicLong.h"
#include "AtomicDouble.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTime.h"
#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "AtomicLong.h"
#include "AtomicBoolean.h"
#include "ConcurrentHashMap.h"
#include "JsonTypeDefs.h"



#include "TargetGroupTypeDefs.h"

class TargetGroup {

private:
public:

static std::string TARGETTING_TYPE_SEGMENT_TARGETING;
static std::string TARGETTING_TYPE_SEGMENTLESS_TARGETING;
static std::string TARGETTING_TYPE_GEO_FEATURE_TARGETING;
static std::string TARGETTING_TYPE_RECENCY_TARGETING;
static std::string TARGETTING_TYPE_TOP_MGRS_TARGETING;

static std::string PACING_METHOD_BUDGET;
static std::string PACING_METHOD_IMPRESSION;
static std::string PACING_METHOD_HYBRID;

static std::string PACING_SPEED_EVEN;
static std::string PACING_SPEED_ASAP;

static std::string getEntityName();

bool fakeTargetGroupForValidationPurposes;

std::string name;
std::string status;
std::string pacingMethod;
std::string pacingSpeed;
std::string description;
std::shared_ptr<std::vector<std::string> > iabCategory;    //add this to table in database
std::shared_ptr<std::vector<std::string> > iabSubCategory;    //add this to table in database
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicBoolean> > adPositions;
std::string frequencyUnit;

/*
   this will check if we are bidding based on the types that are expected by admin or not.
   for example if tg has SEGMENT and GEO types , we cannot bid in RON mode for this target group
   by doing this, if there is ever a problem in loading targets, we make sure that we don't do RON bidding for tgs that we shouldnt
 */
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicBoolean> > typesOfTargeting;

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicBoolean> > deviceTypeTargets;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicBoolean> > osTypeTargets;

int campaignId;

long maxImpression;
long dailyMaxImpression;

double maxBudget;
double dailyMaxBudget;

std::shared_ptr<PacingPlan> pacingPlan;

int frequencyInSec;
Poco::DateTime startDate;
Poco::DateTime endDate;     //2004-04-31 in a format that mysql understands

Poco::DateTime createdAt;
Poco::DateTime updatedAt;

//these are the hard limits that are set in db,
//we use these in order to lower the risk of bidding like crazy
std::shared_ptr<gicapods::AtomicLong> numberOfBidsLimitInLastHourPerBidder;
std::shared_ptr<gicapods::AtomicLong> numberOfBidsLimitInInADayPerBidder;
int numberOfBidsAllowedPerMinute;

int id;     //keep this public because of CacheService
//a lot of times we need the id as an string in the bidder filters, so we create it here once
std::string idAsString;



//this is the hourly limit that is calculated based on pacing
std::shared_ptr<gicapods::AtomicLong> pacingBasedHourlyCalculatedImpressionLimit;
std::shared_ptr<gicapods::AtomicDouble> pacingBasedHourlyCalculatedBudgetLimit;



TargetGroup();


static void loadingTargetGroups();

void validate();

std::string toString();

std::string toJson();

virtual ~TargetGroup();

int getId();
std::string getName();
std::string getStatus();
std::string getDescription();
int getClosestMaxHourIndexToNow();
std::shared_ptr<std::vector<std::string> > getIabCategory();
std::shared_ptr<std::vector<std::string> > getIabSubCategory();
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicBoolean> > getAdPositions();
int getCampaignId();
long getMaxImpression();
long getDailyMaxImpression();
double getMaxBudget();
double getDailyMaxBudget();

std::shared_ptr<PacingPlan> getPacingPlan();
int getFrequencyInSec();
Poco::DateTime getStartDate();
Poco::DateTime getEndDate();
Poco::DateTime getCreatedAt();
Poco::DateTime getUpdatedAt();
std::unordered_map<std::string, std::shared_ptr<gicapods::AtomicLong> > getFilterToCounterMap();

std::shared_ptr<gicapods::AtomicLong> getNumberOfBidsLimitInInADayPerBidder();

void setId(int id);

void setName(std::string name);
void setStatus(std::string status);
void setDescription(std::string description);

void setUpdatedAt(Poco::DateTime updatedAt);
void setCreatedAt(Poco::DateTime createdAt);
void setIabCategory(std::string iabCategory);

void setIabCategories(std::vector<std::string> iabCategories);
void setIabSubCategories(std::vector<std::string> iabSubCategories);
void setIabSubCategory(std::string iabSubCategory);
void setAdPositions(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicBoolean> > adPositions);
void setCampaignId(int campaignId);
void setMaxImpression(long maxImpression);
void setDailyMaxImpression(long dailyMaxImpression);

void setMaxBudget(double maxBudget);
void setDailyMaxBudget(double dailyMaxBudget);

void setPacingPlan(std::shared_ptr<PacingPlan> pacingPlan);
void setFrequencyInSec(int frequencyInSec);
void setStartDate(Poco::DateTime startDate);
void setEndDate(Poco::DateTime endDate);
void setNumberOfBidsLimitInLastHourPerBidder(std::shared_ptr<gicapods::AtomicLong> numberOfBidsLimitInLastHourPerBidder);
std::shared_ptr<gicapods::AtomicLong> getNumberOfBidsLimitInLastHourPerBidder();


void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
static std::shared_ptr<TargetGroup> fromJsonValue(RapidJsonValueType value);


int getNumberOfBidsAllowedPerMinute();
void setNumberOfBidsAllowedPerMinute(int numberOfBidsAllowedPerMinute);
};

#endif
