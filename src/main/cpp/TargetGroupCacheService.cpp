#include "TargetGroup.h"
#include "StringUtil.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "TargetGroupCacheService.h"
#include "GeoLocation.h"
#include "GeoSegment.h"
#include "TargetGroupSegmentMap.h"

#include "MySqlGeoSegmentListService.h"
#include "MySqlGeoLocationService.h"
#include "MySqlTargetGroupDayPartTargetMapService.h"
#include "MySqlTargetGroupGeoSegmentListMapService.h"
#include "MySqlTargetGroupCreativeMapService.h"
#include "MySqlSegmentService.h"
#include "MySqlTargetGroupSegmentMapService.h"
#include "ConfigService.h"
#include "JsonArrayUtil.h"


TargetGroupCacheService::TargetGroupCacheService(HttpUtilService* httpUtilService,
                                                 std::string dataMasterUrl,
                                                 EntityToModuleStateStats* entityToModuleStateStats,std::string appName) :
        CacheService<TargetGroup>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName)
{
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->allCurrentTargetGroups = std::make_shared<std::vector<std::shared_ptr<TargetGroup> > > ();
}

void TargetGroupCacheService::clearOtherCaches() {
        MLOG(10) << "clearing all the targetgroup caches";

        allCurrentTargetGroups->clear();
}

void TargetGroupCacheService::populateOtherMapsAndLists(std::vector<std::shared_ptr<TargetGroup> > allTargetGroups) {
        for(std::shared_ptr<TargetGroup> tg :  allTargetGroups) {
                addTargetGroupToCache(tg);
        }
}


std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > TargetGroupCacheService::getAllCurrentTargetGroups() {
        return allCurrentTargetGroups;
}

void TargetGroupCacheService::addTargetGroupToCache(std::shared_ptr<TargetGroup> tg) {
        if (isCurrent(tg)) {
                allCurrentTargetGroups->push_back(tg);
        } else {
                LOG(ERROR) << "tg is not current : "<< tg->toJson();
        }
}

bool TargetGroupCacheService::isCurrent(std::shared_ptr<TargetGroup> tg) {
        Poco::DateTime now;
        return (now >= tg->getStartDate() && now <= tg->getEndDate());
}

std::shared_ptr<TargetGroupCacheService>
TargetGroupCacheService::getInstance(gicapods::ConfigService* configService) {
        static auto instance =
                std::make_shared<TargetGroupCacheService>(
                        HttpUtilService::getInstance(configService).get(),
                        configService->get("dataMasterUrl"),
                        EntityToModuleStateStats::getInstance(configService).get(),
                        configService->get("appName"));
        return instance;
}
