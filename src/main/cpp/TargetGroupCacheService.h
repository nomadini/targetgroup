#ifndef TargetGroupCacheService_h
#define TargetGroupCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "EntityRealTimeDeliveryInfo.h"
#include <tbb/concurrent_hash_map.h>
class MySqlDriver;
#include "HttpUtilService.h"
#include "CacheService.h"
#include "EntityProviderService.h"
#include "CacheService.h"
class EntityToModuleStateStats;

class TargetGroupCacheService;



class TargetGroupCacheService : public CacheService<TargetGroup> {


public:

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > allCurrentTargetGroups;

EntityToModuleStateStats* entityToModuleStateStats;

TargetGroupCacheService(
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string appName);

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > getAllCurrentTargetGroups();

void addTargetGroupToCache(std::shared_ptr<TargetGroup> tg);

void clearOtherCaches();

bool isCurrent(std::shared_ptr<TargetGroup> tg);

void populateOtherMapsAndLists(std::vector<std::shared_ptr<TargetGroup> > allEntities);
static std::shared_ptr<TargetGroupCacheService> getInstance(gicapods::ConfigService* configService);
};


#endif
