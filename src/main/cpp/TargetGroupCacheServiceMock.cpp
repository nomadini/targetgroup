#include "TargetGroup.h"
#include "StringUtil.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "TargetGroupSegmentMap.h"

#include "MySqlTargetGroupService.h"
#include "MySqlGeoSegmentListService.h"
#include "MySqlGeoLocationService.h"
#include "MySqlTargetGroupDayPartTargetMapService.h"
#include "MySqlTargetGroupGeoSegmentListMapService.h"
#include "MySqlTargetGroupCreativeMapService.h"
#include "MySqlSegmentService.h"
#include "MySqlTargetGroupSegmentMapService.h"
#include "JsonArrayUtil.h"
#include "GeoLocation.h"
#include "GeoSegment.h"
#include "TargetGroupCacheServiceMock.h"


TargetGroupCacheServiceMock::
TargetGroupCacheServiceMock(
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string appName) :
        TargetGroupCacheService
                (httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName)
{

}
