#ifndef TargetGroupCacheServiceMock_h
#define TargetGroupCacheServiceMock_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "EntityRealTimeDeliveryInfo.h"
#include "TargetGroupCacheService.h"
#include <tbb/concurrent_hash_map.h>
class TargetGroupCacheService;
class MySqlDriver;
#include "HttpUtilService.h"
#include "gmock/gmock.h"
class EntityToModuleStateStats;

class TargetGroupCacheServiceMock;


class TargetGroupCacheServiceMock : public TargetGroupCacheService {
public:
TargetGroupCacheServiceMock(
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string appName);
MOCK_METHOD1(processReadAll, std::string(std::string));
};


#endif
