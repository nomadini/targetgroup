#include "PacingPlan.h"
#include "TargetGroupFilterCountDbRecord.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "AtomicDouble.h"
#include "StringUtil.h"

#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormat.h"
#include "DateTimeUtil.h"
#include "CollectionUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>
#include "NetworkUtil.h"
#include "JsonArrayUtil.h"
TargetGroupFilterCountDbRecord::TargetGroupFilterCountDbRecord() {
        numberOfFailures = std::make_shared<gicapods::AtomicLong>(0);
        numberOfPasses = std::make_shared<gicapods::AtomicLong>(0);
        totalNumberOfCalls = std::make_shared<gicapods::AtomicLong>(0);
        totalNumberOfRequest = std::make_shared<gicapods::AtomicLong>(0);
        percentageOfFailures = std::make_shared<gicapods::AtomicDouble>(0);
        Poco::DateTime now;
        timeReported = now;
}

TargetGroupFilterCountDbRecord::~TargetGroupFilterCountDbRecord() {

}

std::string TargetGroupFilterCountDbRecord::mapToJson(
        std::shared_ptr<std::unordered_map<int, std::shared_ptr<TargetGroupFilterCountDbRecord> > > targetGroupIdToPerformanceMetricMap) {
        std::vector<std::shared_ptr<TargetGroupFilterCountDbRecord> > allMetrics;

        for(auto const& entry :  *targetGroupIdToPerformanceMetricMap) {
                allMetrics.push_back(entry.second);
        }
        std::string json = JsonArrayUtil::convertListToJson(allMetrics);
        return json;
}

std::string TargetGroupFilterCountDbRecord::toString() {
        return toJson ();
}

std::shared_ptr<TargetGroupFilterCountDbRecord> TargetGroupFilterCountDbRecord::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<TargetGroupFilterCountDbRecord> obj = std::make_shared<TargetGroupFilterCountDbRecord>();

        obj->entityId = JsonUtil::GetStringSafely(value, "entityId");
        obj->entityType = JsonUtil::GetStringSafely(value, "entityType");
        obj->filterName = JsonUtil::GetStringSafely(value, "filterName");
        obj->filterType = JsonUtil::GetStringSafely(value, "filterType");
        obj->hostname = JsonUtil::GetStringSafely(value, "hostname");
        obj->appVersion = JsonUtil::GetStringSafely(value, "appVersion");
        obj->numberOfFailures->setValue(JsonUtil::GetLongSafely(value, "numberOfFailures"));
        obj->numberOfPasses->setValue(JsonUtil::GetLongSafely(value, "numberOfPasses"));
        obj->totalNumberOfCalls->setValue(JsonUtil::GetLongSafely(value, "totalNumberOfCalls"));
        obj->totalNumberOfRequest->setValue(JsonUtil::GetLongSafely(value, "totalNumberOfRequest"));

        obj->percentageOfFailures->setValue(JsonUtil::GetDoubleSafely(value, "percentageOfFailures"));

        obj->timeReported = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value, "timeReported"));

        return obj;
}


void TargetGroupFilterCountDbRecord::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

        JsonUtil::addMemberToValue_FromPair (doc, "entityId", entityId, value);

        JsonUtil::addMemberToValue_FromPair (doc, "entityType", entityType, value);

        JsonUtil::addMemberToValue_FromPair (doc, "filterName", filterName, value);
        JsonUtil::addMemberToValue_FromPair (doc, "filterType", filterType, value);
        JsonUtil::addMemberToValue_FromPair (doc, "hostname", hostname, value);
        JsonUtil::addMemberToValue_FromPair (doc, "appVersion", appVersion, value);

        JsonUtil::addMemberToValue_FromPair (doc, "timeReported", DateTimeUtil::dateTimeToStr(timeReported), value);

        JsonUtil::addMemberToValue_FromPair (doc, "numberOfFailures", numberOfFailures->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "numberOfPasses", numberOfPasses->getValue(), value);

        JsonUtil::addMemberToValue_FromPair (doc, "totalNumberOfCalls", totalNumberOfCalls->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "totalNumberOfRequest", totalNumberOfRequest->getValue(), value);

        JsonUtil::addMemberToValue_FromPair (doc, "percentageOfFailures", percentageOfFailures->getValue(), value);
}

std::string TargetGroupFilterCountDbRecord::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}
