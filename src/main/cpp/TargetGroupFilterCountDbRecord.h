#ifndef TargetGroupFilterCountDbRecord_h
#define TargetGroupFilterCountDbRecord_h

#include "AtomicLong.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTime.h"
#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "AtomicLong.h"
#include "JsonTypeDefs.h"

#include "AtomicDouble.h"

class TargetGroupFilterCountDbRecord;




class TargetGroupFilterCountDbRecord {

public:

std::string entityId;    //targetGroupId is string here, because of ALL cases. that captures all targetGroups...
std::string entityType;
std::string filterName;
std::string filterType;
std::string hostname;
std::string appVersion;

std::shared_ptr<gicapods::AtomicLong> numberOfFailures;
std::shared_ptr<gicapods::AtomicLong> numberOfPasses;
std::shared_ptr<gicapods::AtomicLong> totalNumberOfCalls;
std::shared_ptr<gicapods::AtomicLong> totalNumberOfRequest;
std::shared_ptr<gicapods::AtomicDouble> percentageOfFailures;

Poco::DateTime timeReported;

TargetGroupFilterCountDbRecord();
virtual ~TargetGroupFilterCountDbRecord();

static std::string mapToJson(std::shared_ptr<std::unordered_map<int, std::shared_ptr<TargetGroupFilterCountDbRecord> > > targetGroupIdToPerformanceMetricMap);


std::string toString();
static std::shared_ptr<TargetGroupFilterCountDbRecord> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::string toJson();
};

#endif
