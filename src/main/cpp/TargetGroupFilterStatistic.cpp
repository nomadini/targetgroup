#include "PacingPlan.h"
#include "TargetGroupFilterStatistic.h"
#include "JsonUtil.h"
#include "StringUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "TestUtil.h"
#include "StringUtil.h"

#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormat.h"
#include "DateTimeUtil.h"
#include "CollectionUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>


TargetGroupFilterStatistic::TargetGroupFilterStatistic() {
        targetGroupId = 0;
        numberOfFailures = std::make_shared<gicapods::AtomicLong>();
        numberOfPasses = std::make_shared<gicapods::AtomicLong>();
}

std::string TargetGroupFilterStatistic::toString() {
        return this->toJson ();
}


void TargetGroupFilterStatistic::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "numberOfFailures", numberOfFailures->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "numberOfPasses", numberOfPasses->getValue(), value);
}

std::shared_ptr<TargetGroupFilterStatistic> TargetGroupFilterStatistic::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<TargetGroupFilterStatistic> targetGroup = std::make_shared<TargetGroupFilterStatistic>();

        targetGroup->targetGroupId = JsonUtil::GetIntSafely(value, "targetGroupId");
        targetGroup->numberOfFailures->setValue(JsonUtil::GetLongSafely(value, "numberOfFailures"));
        targetGroup->numberOfPasses->setValue(JsonUtil::GetLongSafely(value, "numberOfPasses"));

        return targetGroup;
}

std::string TargetGroupFilterStatistic::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}

TargetGroupFilterStatistic::~TargetGroupFilterStatistic() {
}
