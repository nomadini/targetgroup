#ifndef TargetGroupFilterStatistic_h
#define TargetGroupFilterStatistic_h


#include "PacingPlan.h"
#include "AtomicLong.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTime.h"
#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "AtomicLong.h"
#include "JsonTypeDefs.h"


class TargetGroupFilterStatistic;




class TargetGroupFilterStatistic {

public:

int targetGroupId;
std::shared_ptr<gicapods::AtomicLong> numberOfFailures;
std::shared_ptr<gicapods::AtomicLong> numberOfPasses;

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::shared_ptr<TargetGroupFilterStatistic> fromJsonValue(RapidJsonValueType value);
std::string toJson();
std::string toString();

TargetGroupFilterStatistic();
~TargetGroupFilterStatistic();
};

#endif
