#include "TargetGroup.h"
#include "DateTimeUtil.h"
#include "StringUtil.h"
#include "PacingPlan.h"
#include "TargetGroupTestHelper.h"
#include "RandomUtil.h"
std::shared_ptr<TargetGroup> TargetGroupTestHelper::createSampleTargetGroup() {
								std::shared_ptr<TargetGroup> tg = std::make_shared<TargetGroup>();
								tg->setId(RandomUtil::sudoRandomNumber(1000));
								tg->setName("name" + StringUtil::toStr (tg->getId ()));
								tg->setStatus("active");
								tg->setDescription("description " + StringUtil::toStr (tg->getId ()));
								tg->setCampaignId( RandomUtil::sudoRandomNumber(1000));
								tg->setDailyMaxBudget(200.0);
								tg->setMaxBudget(2000.0);
								tg->setDailyMaxImpression(10000);
								tg->setMaxImpression(10000000);
								tg->setIabCategory("IAB3");
								tg->setIabSubCategory("IAB3-1");
								tg->setFrequencyInSec(3);
								Poco::DateTime now;
								tg->setStartDate(now);
								Poco::Timespan span (30 * Poco::Timespan::DAYS);
								tg->setEndDate(now + span);
								tg->setPacingPlan (PacingPlan::fromJson(StringUtil::toStr(PacingPlan::defaultPlanInJson)));
								tg->setCreatedAt(now);
								tg->setUpdatedAt(now);

								tg->getNumberOfBidsLimitInInADayPerBidder()->setValue(10000);
								tg->setNumberOfBidsAllowedPerMinute(100);
								tg->frequencyUnit= "day";

								tg->validate();
								return tg;
}
