/*
 * TargetGroupTestHelper.h
 *
 *  Created on: Aug 28, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_TARGETGROUPTESTHELPER_H_
#define GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_TARGETGROUPTESTHELPER_H_



#include "TargetGroupTypeDefs.h"
class TargetGroup;
class DateTimeUtil;
#include "PacingPlan.h"

class TargetGroupTestHelper {
public :

	static std::shared_ptr<TargetGroup> createSampleTargetGroup();

};



#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_TARGETGROUPTESTHELPER_H_ */
