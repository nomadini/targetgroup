
#include "TargetGroupTestUtil.h"
#include "TargetGroup.h"
#include "TestsCommon.h"
void TargetGroupTestUtil::areEqual(std::shared_ptr<TargetGroup> tg1, std::shared_ptr<TargetGroup> tg2) {
        EXPECT_THAT(tg1->getId(),
                    testing::Eq(tg2->getId()));
        EXPECT_THAT(tg1->getName(),
                    testing::Eq(tg2->getName()));
        EXPECT_THAT(tg1->getStatus(),
                    testing::Eq(tg2->getStatus()));
        EXPECT_THAT(tg1->getDescription(),
                    testing::Eq(tg2->getDescription()));
        EXPECT_THAT(tg1->getIabCategory()->at(0),
                    testing::Eq(tg2->getIabCategory()->at(0)));

        EXPECT_THAT(*tg1->getIabSubCategory(),
                    testing::Eq(*tg2->getIabSubCategory()));

        EXPECT_THAT(tg1->getCampaignId(),
                    testing::Eq(tg2->getCampaignId()));
        EXPECT_THAT(tg1->getMaxImpression(),
                    testing::Eq(tg2->getMaxImpression()));
        EXPECT_THAT(tg1->getDailyMaxImpression(),
                    testing::Eq(tg2->getDailyMaxImpression()));
        EXPECT_THAT(tg1->getMaxBudget(),
                    testing::Eq(tg2->getMaxBudget()));
        EXPECT_THAT(tg1->getDailyMaxBudget(),
                    testing::Eq(tg2->getDailyMaxBudget()));

        EXPECT_THAT(tg1->getPacingPlan()->toJson(),
                    testing::Eq(tg2->getPacingPlan()->toJson()));

        EXPECT_THAT(tg1->getFrequencyInSec(),
                    testing::Eq(tg2->getFrequencyInSec()));

        EXPECT_THAT(tg1->getStartDate(),
                    testing::Eq(tg2->getStartDate()));
        EXPECT_THAT(tg1->getEndDate (),
                    testing::Eq(tg2->getEndDate()));
        EXPECT_THAT(tg1->getCreatedAt (),
                    testing::Eq(tg2->getCreatedAt ()));
        EXPECT_THAT(tg1->getUpdatedAt (),
                    testing::Eq(tg2->getUpdatedAt ()));


}
