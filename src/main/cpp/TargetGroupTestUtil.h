#ifndef TargetGroupTestUtil_h
#define TargetGroupTestUtil_h

#include <gtest/gtest.h>


#include "TargetGroupTypeDefs.h"
class TargetGroup;

class TargetGroupTestUtil {

private:

public:
static void areEqual(std::shared_ptr<TargetGroup> tg1, std::shared_ptr<TargetGroup> tg2);
};

#endif
