#include "PacingPlan.h"
#include "TgBiddingPerformanceMetricInBiddingPeriod.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "TestUtil.h"
#include "StringUtil.h"

#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormat.h"
#include "DateTimeUtil.h"
#include "CollectionUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>
#include "NetworkUtil.h"
#include "JsonArrayUtil.h"

TgBiddingPerformanceMetricInBiddingPeriod::TgBiddingPerformanceMetricInBiddingPeriod() {
        numberOfBids = std::make_shared<gicapods::AtomicLong>();
        sumOfBidPrices = std::make_shared<gicapods::AtomicDouble>();
        confirmedWinsByAdServers = std::make_shared<gicapods::AtomicLong>();
        targetGroupId = 0;
}

TgBiddingPerformanceMetricInBiddingPeriod::~TgBiddingPerformanceMetricInBiddingPeriod() {

}

std::string TgBiddingPerformanceMetricInBiddingPeriod::mapToJson(
        std::shared_ptr<std::unordered_map<int, std::shared_ptr<TgBiddingPerformanceMetricInBiddingPeriod>> > targetGroupIdToPerformanceMetricMap) {
        std::vector<std::shared_ptr<TgBiddingPerformanceMetricInBiddingPeriod>> allMetrics;
        for(auto const& entry :  *targetGroupIdToPerformanceMetricMap) {
                allMetrics.push_back(entry.second);
        }
        std::string json =
                JsonArrayUtil::convertListToJson
                <TgBiddingPerformanceMetricInBiddingPeriod>
                        (allMetrics);
        return json;
}

std::string TgBiddingPerformanceMetricInBiddingPeriod::toString() {
        return toJson ();
}

std::shared_ptr<TgBiddingPerformanceMetricInBiddingPeriod> TgBiddingPerformanceMetricInBiddingPeriod::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<TgBiddingPerformanceMetricInBiddingPeriod> tgBiddingPerformanceMetricInBiddingPeriod =
                std::make_shared<TgBiddingPerformanceMetricInBiddingPeriod>();

        tgBiddingPerformanceMetricInBiddingPeriod->targetGroupId = JsonUtil::GetLongSafely(value, "targetGroupId");
        tgBiddingPerformanceMetricInBiddingPeriod->numberOfBids->setValue(JsonUtil::GetLongSafely(value, "numberOfBids"));
        tgBiddingPerformanceMetricInBiddingPeriod->confirmedWinsByAdServers->setValue(JsonUtil::GetLongSafely(value, "confirmedWinsByAdServers"));
        tgBiddingPerformanceMetricInBiddingPeriod->sumOfBidPrices->setValue(JsonUtil::GetDoubleSafely(value, "sumOfBidPrices"));

        return tgBiddingPerformanceMetricInBiddingPeriod;
}


void TgBiddingPerformanceMetricInBiddingPeriod::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "numberOfBids", numberOfBids->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "confirmedWinsByAdServers", confirmedWinsByAdServers->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "sumOfBidPrices", sumOfBidPrices->getValue(), value);
}

std::string TgBiddingPerformanceMetricInBiddingPeriod::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}
