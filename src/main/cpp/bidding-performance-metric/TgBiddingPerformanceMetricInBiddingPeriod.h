#ifndef TgBiddingPerformanceMetricInBiddingPeriod_h
#define TgBiddingPerformanceMetricInBiddingPeriod_h

#include "AtomicLong.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTime.h"
#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "AtomicLong.h"
#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"
#include "AtomicDouble.h"
class TgBiddingPerformanceMetricInBiddingPeriod;


/*
   encapsulates all the metrics of a tg that a bidder collects in every bidding period,
   it will be cleared after right before caches, it will be converted to BidConfirmedWinsContainerPerTargetGroupAndBidder
   before reloading caches to be persisted
 */
class TgBiddingPerformanceMetricInBiddingPeriod {

public:
int targetGroupId;
std::shared_ptr<gicapods::AtomicLong> numberOfBids;
std::shared_ptr<gicapods::AtomicLong> confirmedWinsByAdServers;
std::shared_ptr<gicapods::AtomicDouble> sumOfBidPrices;

TgBiddingPerformanceMetricInBiddingPeriod();
virtual ~TgBiddingPerformanceMetricInBiddingPeriod();

static std::string mapToJson(std::shared_ptr<std::unordered_map<int, std::shared_ptr<TgBiddingPerformanceMetricInBiddingPeriod>> > targetGroupIdToPerformanceMetricMap);


std::string toString();
static std::shared_ptr<TgBiddingPerformanceMetricInBiddingPeriod> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::string toJson();
};

#endif
