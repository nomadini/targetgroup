#include "PacingPlan.h"
#include "TgBiddingPerformanceMetricInBiddingPeriodService.h"
#include "JsonUtil.h"
#include "TargetGroup.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "TestUtil.h"
#include "StringUtil.h"

#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormat.h"
#include "DateTimeUtil.h"
#include "CollectionUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>
#include "NetworkUtil.h"
#include "JsonArrayUtil.h"
#include "MySqlTgBiddingPerformanceMetricDtoService.h"
#include "TgBiddingPerformanceFilterMeasures.h"
#include "TgBiddingPerformanceMetricInBiddingPeriod.h"
TgBiddingPerformanceMetricInBiddingPeriodService::TgBiddingPerformanceMetricInBiddingPeriodService() {

}

TgBiddingPerformanceMetricInBiddingPeriodService::~TgBiddingPerformanceMetricInBiddingPeriodService() {

}

/*
   we persist all the minutely entries in tgBiddingPerformanceMap except the
   current minute, because the current minute entry is being updated by the flow of bid requests
   we also delete all the deleted entries from the map
 */
void TgBiddingPerformanceMetricInBiddingPeriodService::persistNumberOfBidsMadeByBidderForAllTargetGroups() {
        // get upgradable access
        boost::upgrade_lock<boost::shared_mutex> lock(_access);

        std::vector<std::shared_ptr<BidConfirmedWinsContainerPerTargetGroupAndBidder> > allMetrics;
        MLOG(10) << " map size : "<<tgBiddingPerformanceMap->size();
        auto newMap = tgBiddingPerformanceMap->moveMap();
        for(auto&& entry: *newMap) {
                auto bidConfirmedWinsContainerPerTargetGroupAndBidder = std::make_shared<BidConfirmedWinsContainerPerTargetGroupAndBidder>();
                bidConfirmedWinsContainerPerTargetGroupAndBidder->targetGroupId = entry.first;

                bidConfirmedWinsContainerPerTargetGroupAndBidder->timeReported = DateTimeUtil::getCurrentDateWithSecondPercision();
                bidConfirmedWinsContainerPerTargetGroupAndBidder->hostname = NetworkUtil::getHostName();

                if(entry.second->numberOfBids->getValue() == 0 &&
                   entry.second->sumOfBidPrices->getValue() == 0) {
                        //if we have not bid on this target group, we don't save anything in db
                        continue;
                }
                bidConfirmedWinsContainerPerTargetGroupAndBidder->numberOfBids->setValue(
                        entry.second->numberOfBids->getValue());
                bidConfirmedWinsContainerPerTargetGroupAndBidder->sumOfBidPrices->setValue(
                        entry.second->sumOfBidPrices->getValue());

                bidConfirmedWinsContainerPerTargetGroupAndBidder->confirmedWinsByAdServers->setValue(
                        entry.second->confirmedWinsByAdServers->getValue());
                allMetrics.push_back(bidConfirmedWinsContainerPerTargetGroupAndBidder);
        }
        MLOG(10) << " allMetrics size : "<<allMetrics.size();
        mySqlTgBiddingPerformanceMetricDtoService->insert(allMetrics);
        tgBiddingPerformanceMap->clear();
}

void TgBiddingPerformanceMetricInBiddingPeriodService::updateBidMetrics(int chosenTargetGroupId, double bidPrice) {
        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);

        auto entry = tgBiddingPerformanceMap->getOrCreate(chosenTargetGroupId);
        entry->targetGroupId = chosenTargetGroupId;
        entry->numberOfBids->increment();
        entry->sumOfBidPrices->addValue(bidPrice);
}

void TgBiddingPerformanceMetricInBiddingPeriodService::updateConfirmedWins(int chosenTargetGroupId, int value) {
        // get shared access
        boost::shared_lock<boost::shared_mutex> lock(_access);

        auto entry = tgBiddingPerformanceMap->getOrCreate(chosenTargetGroupId);
        entry->targetGroupId = chosenTargetGroupId;
        entry->confirmedWinsByAdServers->addValue(value);
}
