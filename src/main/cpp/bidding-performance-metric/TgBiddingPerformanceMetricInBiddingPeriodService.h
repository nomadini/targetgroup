#ifndef TgBiddingPerformanceMetricInBiddingPeriodService_h
#define TgBiddingPerformanceMetricInBiddingPeriodService_h

#include "AtomicLong.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTime.h"
#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "AtomicLong.h"
#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"
#include "AtomicDouble.h"
#include "ConcurrentHashMap.h"
#include <boost/thread.hpp>
class TargetGroup;
class TgBiddingPerformanceMetricInBiddingPeriod;
class MySqlTgBiddingPerformanceMetricDtoService;

class TgBiddingPerformanceMetricInBiddingPeriodService;




/*
   contains the map that different of tgId to TgBiddingPerformanceMetric, all the operations in different
   modules of bidder on the map, will be done through this container
 */
class TgBiddingPerformanceMetricInBiddingPeriodService {
boost::shared_mutex _access;
public:

std::shared_ptr<gicapods::ConcurrentHashMap<int, TgBiddingPerformanceMetricInBiddingPeriod> > tgBiddingPerformanceMap;
MySqlTgBiddingPerformanceMetricDtoService* mySqlTgBiddingPerformanceMetricDtoService;

TgBiddingPerformanceMetricInBiddingPeriodService();

virtual ~TgBiddingPerformanceMetricInBiddingPeriodService();

void persistNumberOfBidsMadeByBidderForAllTargetGroups();

void updateBidMetrics(int chosenTargetGroupId, double bidPrice);
void updateConfirmedWins(int chosenTargetGroupId, int value);

};

#endif
