#include "PacingPlan.h"
#include "TgFilterMeasuresService.h"
#include "JsonUtil.h"
#include "TargetGroup.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "TestUtil.h"
#include "StringUtil.h"

#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormat.h"
#include "DateTimeUtil.h"
#include "CollectionUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>
#include "NetworkUtil.h"
#include "JsonArrayUtil.h"
#include "MySqlTgBiddingPerformanceMetricDtoService.h"
#include "TgBiddingPerformanceFilterMeasures.h"

TgFilterMeasuresService::TgFilterMeasuresService() {

}

TgFilterMeasuresService::~TgFilterMeasuresService() {

}

//this is called every hour by Async Job, to reset the tg hourly measures
void TgFilterMeasuresService::resetAllTargetGroupHourlyPerformanceMetrics() {

        //BOOST_FOREACH_MAP
        for(auto const& entry :  *targetGroupIdToFilterMeasuresMap) {
                entry.second->numberOfBidsInLastHourByThisBidder->setValue(0);
        }

}

void TgFilterMeasuresService::resetAllTargetGroupMinutelyPerformanceMetrics() {

        //BOOST_FOREACH_MAP
        for(auto const& entry :  *targetGroupIdToFilterMeasuresMap) {
                entry.second->numberOfBidsInLastMinuteByThisBidder->setValue(0);
                entry.second->sumOfBidPricesInLastMinuteByThisBidder->setValue(0);
        }
}

//this is called by bidder async services, every time data is reloaded
void TgFilterMeasuresService::readPerformanceMetricFromDb() {
        std::string hostname = NetworkUtil::getHostName();
        mySqlTgBiddingPerformanceMetricDtoService->readsumOfDailyBiddingInfoByThisHost(targetGroupIdToFilterMeasuresMap, hostname);

        mySqlTgBiddingPerformanceMetricDtoService->readsumOfHourlyBiddingInfoByThisHost(targetGroupIdToFilterMeasuresMap, hostname);

        mySqlTgBiddingPerformanceMetricDtoService->readSumOfLastHourWinsByAllBidders(targetGroupIdToFilterMeasuresMap);
}

void TgFilterMeasuresService::initFilterMeasuresAndPerformanceMapsForCurrentTgs(
        std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > candidateTargetGroups) {

        for (auto tg :  *candidateTargetGroups) {
                assertAndThrow(tg->getId() > 0);
                getOrCreateEntryInFilterMeasuresMap(tg->getId());
        }
}

void TgFilterMeasuresService::updateBidMetrics(int chosenTargetGroupId, double bidPrice) {

        auto tgBiddingPerformanceFilterMeasures = getOrCreateEntryInFilterMeasuresMap(chosenTargetGroupId);

        tgBiddingPerformanceFilterMeasures->numberOfBidsInLastMinuteByThisBidder->increment();
        tgBiddingPerformanceFilterMeasures->numberOfBidsInLastHourByThisBidder->increment();
        tgBiddingPerformanceFilterMeasures->numberOfBidsMadeTodayByThisBidder->increment();
        //make sure the filter for this works!!! TODO : add a test
        MLOG(2) << "tgPerformance incremented to  "<< tgBiddingPerformanceFilterMeasures->toJson();
}

long TgFilterMeasuresService::getNumberOfBidsMadeTodayByThisBidderForTg(int targetGroupId) {
        auto tgBiddingPerformanceFilterMeasures = getOrCreateEntryInFilterMeasuresMap(targetGroupId);
        return tgBiddingPerformanceFilterMeasures->numberOfBidsMadeTodayByThisBidder->getValue();
}

double TgFilterMeasuresService::getSumOfOfPlatformCostsInLastHourForTg(int targetGroupId) {
        auto tgBiddingPerformanceFilterMeasures = getOrCreateEntryInFilterMeasuresMap(targetGroupId);
        return tgBiddingPerformanceFilterMeasures->sumOfPlatformCostWonInLastHour->getValue();
}

long TgFilterMeasuresService::getNumberOfWinsInLastHourForTg(int targetGroupId) {
        auto tgBiddingPerformanceFilterMeasures = getOrCreateEntryInFilterMeasuresMap(targetGroupId);
        return tgBiddingPerformanceFilterMeasures->sumOfImpressionsWonInLastHour->getValue();

}


long TgFilterMeasuresService::
getNumberOfBidsInLastMinuteByThisBidderForTg(int targetGroupId) {
        auto tgBiddingPerformanceFilterMeasures = getOrCreateEntryInFilterMeasuresMap(targetGroupId);
        return tgBiddingPerformanceFilterMeasures->numberOfBidsInLastMinuteByThisBidder->getValue();

}

long TgFilterMeasuresService::getNumberOfBidsInLastHourByThisBidderForTg(int targetGroupId) {

        auto tgBiddingPerformanceFilterMeasures = getOrCreateEntryInFilterMeasuresMap(targetGroupId);
        return tgBiddingPerformanceFilterMeasures->numberOfBidsInLastHourByThisBidder->getValue();
}

std::shared_ptr<TgBiddingPerformanceFilterMeasures> TgFilterMeasuresService::getOrCreateEntryInFilterMeasuresMap(int targetGroupId) {
        tbb::concurrent_hash_map<int, std::shared_ptr<TgBiddingPerformanceFilterMeasures>>::accessor fAccessor;
        if(!targetGroupIdToFilterMeasuresMap->find(fAccessor, targetGroupId)) {
                auto filterMeasurePerfEntry = std::make_shared<TgBiddingPerformanceFilterMeasures>();
                filterMeasurePerfEntry->targetGroupId = targetGroupId;
                targetGroupIdToFilterMeasuresMap->insert(fAccessor, targetGroupId);
                fAccessor->second = filterMeasurePerfEntry;
        }
        return fAccessor->second;
}
