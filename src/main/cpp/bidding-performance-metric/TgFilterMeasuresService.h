#ifndef TgFilterMeasuresService_h
#define TgFilterMeasuresService_h

#include "AtomicLong.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTime.h"
#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "AtomicLong.h"
#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"
#include "AtomicDouble.h"

class TgBiddingPerformanceFilterMeasures;
class TargetGroup;
class MySqlTgBiddingPerformanceMetricDtoService;



/**
   it encapsulates all the operations that are done on targetGroupIdToFilterMeasuresMap
   targetGroupIdToFilterMeasuresMap is used in some of the filters in bidders
 */
class TgFilterMeasuresService {

public:
tbb::concurrent_hash_map<int, std::shared_ptr<TgBiddingPerformanceFilterMeasures> >* targetGroupIdToFilterMeasuresMap;
MySqlTgBiddingPerformanceMetricDtoService* mySqlTgBiddingPerformanceMetricDtoService;

TgFilterMeasuresService();

virtual ~TgFilterMeasuresService();

void resetAllTargetGroupMinutelyPerformanceMetrics();

void readPerformanceMetricFromDb();

void initFilterMeasuresAndPerformanceMapsForCurrentTgs(std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > candidateTargetGroups);

void updateBidMetrics(int chosenTargetGroupId, double bidPrice);

long getNumberOfBidsMadeTodayByThisBidderForTg(int targetGroupId);

long getNumberOfBidsInLastMinuteByThisBidderForTg(int targetGroupId);

double getSumOfOfPlatformCostsInLastHourForTg(int targetGroupId);
long getNumberOfWinsInLastHourForTg(int targetGroupId);

long getNumberOfBidsInLastHourByThisBidderForTg(int targetGroupId);

void resetAllTargetGroupHourlyPerformanceMetrics();

std::shared_ptr<TgBiddingPerformanceFilterMeasures> getOrCreateEntryInFilterMeasuresMap(int targetGroupId);

// std::shared_ptr<TargetGroup> getLowestDelieveredTgInCurrentHour(
//         std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > allTgs
//         );

};

#endif
