#include "PacingPlan.h"
#include "ConfirmedWinRequest.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "TestUtil.h"
#include "StringUtil.h"

#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormat.h"
#include "DateTimeUtil.h"
#include "CollectionUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>
#include "NetworkUtil.h"
#include "JsonArrayUtil.h"
#include "ConcurrentHashMap.h"
// #include "HashMap.h"

#include "HashMap.h"
// #include "HashMap.cpp"
#include "EntityToModuleStateStats.h"


ConfirmedWinRequest::ConfirmedWinRequest() {
        targetGroupIdWins = std::make_shared<gicapods::HashMap<int, IntWrapper > > ();
}

ConfirmedWinRequest::~ConfirmedWinRequest() {

}


std::string ConfirmedWinRequest::toString() {
        return toJson ();
}

std::shared_ptr<ConfirmedWinRequest> ConfirmedWinRequest::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<ConfirmedWinRequest> obj = std::make_shared<ConfirmedWinRequest>();
        std::unordered_map<int, int> simpleMap;
        JsonMapUtil::read_Map_From_Value(value, simpleMap);
        for (auto& pair : simpleMap) {
                auto intWrapper = std::make_shared<IntWrapper>();
                intWrapper->setValue(pair.second);
                obj->targetGroupIdWins->map->insert(std::make_pair(pair.first, intWrapper));
        }

        return obj;
}

std::shared_ptr<ConfirmedWinRequest> ConfirmedWinRequest::fromJson(std::string json) {

        std::shared_ptr<ConfirmedWinRequest> obj = std::make_shared<ConfirmedWinRequest>();
        auto doc = parseJsonSafely(json);

        std::unordered_map<int, int> simpleMap;
        JsonMapUtil::read_Map_From_Value(*doc, simpleMap);
        for (auto& pair : simpleMap) {
                auto intWrapper = std::make_shared<IntWrapper>();
                intWrapper->setValue(pair.second);
                obj->targetGroupIdWins->map->insert(std::make_pair(pair.first, intWrapper));
        }

        return obj;
}


void ConfirmedWinRequest::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonMapUtil::addMapAsMemberToValue (
                doc,
                *targetGroupIdWins->map,
                value);
}


std::string ConfirmedWinRequest::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}
