#ifndef ConfirmedWinRequest_h
#define ConfirmedWinRequest_h

#include "AtomicLong.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTime.h"
#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "AtomicLong.h"
#include "JsonTypeDefs.h"


#include "Poco/DateTime.h"
namespace gicapods {

template <class K, class V>
class ConcurrentHashMap;

template <class K, class V>
class HashMap;

}
#include "AtomicDouble.h"
class IntWrapper;
class ConfirmedWinRequest;
class EntityRealTimeDeliveryInfo;
class EntityToModuleStateStats;


/**
 */
class ConfirmedWinRequest {

public:
std::shared_ptr<gicapods::HashMap<int, IntWrapper > > targetGroupIdWins;

ConfirmedWinRequest();
virtual ~ConfirmedWinRequest();

std::string toString();
static std::shared_ptr<ConfirmedWinRequest> fromJsonValue(RapidJsonValueType value);
static std::shared_ptr<ConfirmedWinRequest> fromJson(std::string json);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::string toJson();
};

#endif
