#include "PacingPlan.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "TestUtil.h"
#include "StringUtil.h"

#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormat.h"
#include "DateTimeUtil.h"
#include "CollectionUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>
#include "NetworkUtil.h"
#include "JsonArrayUtil.h"

BidConfirmedWinsContainerPerTargetGroupAndBidder::BidConfirmedWinsContainerPerTargetGroupAndBidder() {
        targetGroupId = 0;
        numberOfBids =  std::make_shared<gicapods::AtomicLong>();
        confirmedWinsByAdServers = std::make_shared<gicapods::AtomicLong>();
        sumOfBidPrices = std::make_shared<gicapods::AtomicDouble>();
        timeReported = DateTimeUtil::getCurrentDateWithSecondPercision();
        hostname = NetworkUtil::getHostName();
}

BidConfirmedWinsContainerPerTargetGroupAndBidder::~BidConfirmedWinsContainerPerTargetGroupAndBidder() {

}

std::string BidConfirmedWinsContainerPerTargetGroupAndBidder::mapToJson(
        std::shared_ptr<std::unordered_map<int, std::shared_ptr<BidConfirmedWinsContainerPerTargetGroupAndBidder>> > targetGroupIdToPerformanceMetricMap) {
        std::vector<std::shared_ptr<BidConfirmedWinsContainerPerTargetGroupAndBidder>> allMetrics;
        for(auto const& entry :  *targetGroupIdToPerformanceMetricMap) {
                allMetrics.push_back(entry.second);
        }
        std::string json = JsonArrayUtil::convertListToJson(allMetrics);
        return json;
}

std::string BidConfirmedWinsContainerPerTargetGroupAndBidder::toString() {
        return toJson ();
}

std::shared_ptr<BidConfirmedWinsContainerPerTargetGroupAndBidder> BidConfirmedWinsContainerPerTargetGroupAndBidder::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<BidConfirmedWinsContainerPerTargetGroupAndBidder> obj = std::make_shared<BidConfirmedWinsContainerPerTargetGroupAndBidder>();

        obj->targetGroupId = JsonUtil::GetIntSafely(value, "targetGroupId");
        obj->timeReported = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value, "timeReported"));
        obj->hostname = JsonUtil::GetStringSafely(value, "hostname");
        obj->numberOfBids->setValue(JsonUtil::GetLongSafely(value, "numberOfBids"));
        obj->confirmedWinsByAdServers->setValue(JsonUtil::GetLongSafely(value, "confirmedWinsByAdServers"));
        obj->sumOfBidPrices->setValue(JsonUtil::GetDoubleSafely(value, "sumOfBidPrices"));

        return obj;
}

void BidConfirmedWinsContainerPerTargetGroupAndBidder::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, value);

        JsonUtil::addMemberToValue_FromPair (doc, "timeReported",  DateTimeUtil::dateTimeToStr(timeReported), value);
        JsonUtil::addMemberToValue_FromPair (doc, "hostname", hostname, value);

        JsonUtil::addMemberToValue_FromPair (doc, "confirmedWinsByAdServers", confirmedWinsByAdServers->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "numberOfBids", numberOfBids->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "sumOfBidPrices", sumOfBidPrices->getValue(), value);
}

std::string BidConfirmedWinsContainerPerTargetGroupAndBidder::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}
