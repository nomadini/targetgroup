#ifndef BidConfirmedWinsContainerPerTargetGroupAndBidder_h
#define BidConfirmedWinsContainerPerTargetGroupAndBidder_h

#include "AtomicLong.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTime.h"
#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "AtomicLong.h"
#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"
#include "AtomicDouble.h"
class BidConfirmedWinsContainerPerTargetGroupAndBidder;


/*
   represents a record in db that has info or how many bids and sum of bids prices
   and confirmed bids has been recorded by a bidder
 */
class BidConfirmedWinsContainerPerTargetGroupAndBidder {

public:

int targetGroupId;
Poco::DateTime timeReported;//in second percision
std::string hostname;

std::shared_ptr<gicapods::AtomicLong> numberOfBids;
std::shared_ptr<gicapods::AtomicLong> confirmedWinsByAdServers;

std::shared_ptr<gicapods::AtomicDouble> sumOfBidPrices;

BidConfirmedWinsContainerPerTargetGroupAndBidder();
virtual ~BidConfirmedWinsContainerPerTargetGroupAndBidder();

static std::string mapToJson(std::shared_ptr<std::unordered_map<int, std::shared_ptr<BidConfirmedWinsContainerPerTargetGroupAndBidder>> > targetGroupIdToPerformanceMetricMap);


std::string toString();
static std::shared_ptr<BidConfirmedWinsContainerPerTargetGroupAndBidder> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::string toJson();
};

#endif
