


#include "PacingPlan.h"
#include "TgFilterMeasuresService.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "OverallBidWinContainerPerTargetGroup.h"
#include "StringUtil.h"

#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormat.h"
#include "DateTimeUtil.h"
#include "CollectionUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>
#include "NetworkUtil.h"
#include "JsonArrayUtil.h"

OverallBidWinContainerPerTargetGroup::OverallBidWinContainerPerTargetGroup() {
        targetGroupId = 0;
        numberOfBids = std::make_shared<gicapods::AtomicLong>();
        sumOfBidPrices = std::make_shared<gicapods::AtomicDouble>();

        numberOfWins = std::make_shared<gicapods::AtomicLong>();
        platformCostSpentOnWins = std::make_shared<gicapods::AtomicDouble>();

        timeReported = DateTimeUtil::getCurrentDateWithSecondPercision();

}

OverallBidWinContainerPerTargetGroup::~OverallBidWinContainerPerTargetGroup() {

}

std::string OverallBidWinContainerPerTargetGroup::toString() {
        return toJson ();
}

std::shared_ptr<OverallBidWinContainerPerTargetGroup> OverallBidWinContainerPerTargetGroup::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<OverallBidWinContainerPerTargetGroup> obj = std::make_shared<OverallBidWinContainerPerTargetGroup>();

        obj->targetGroupId = JsonUtil::GetIntSafely(value, "targetGroupId");
        obj->timeReported = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value, "timeReported"));
        obj->numberOfBids->setValue(JsonUtil::GetLongSafely(value, "numberOfBids"));
        obj->sumOfBidPrices->setValue(JsonUtil::GetDoubleSafely(value, "sumOfBidPrices"));

        obj->numberOfWins->setValue(JsonUtil::GetLongSafely(value, "numberOfWins"));
        obj->platformCostSpentOnWins->setValue(JsonUtil::GetDoubleSafely(value, "platformCostSpentOnWins"));

        return obj;
}

void OverallBidWinContainerPerTargetGroup::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, value);

        JsonUtil::addMemberToValue_FromPair (doc, "timeReported",  DateTimeUtil::dateTimeToStr(timeReported), value);

        JsonUtil::addMemberToValue_FromPair (doc, "numberOfBids", numberOfBids->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "sumOfBidPrices", sumOfBidPrices->getValue(), value);

        JsonUtil::addMemberToValue_FromPair (doc, "numberOfWins", numberOfWins->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "platformCostSpentOnWins", platformCostSpentOnWins->getValue(), value);
}

std::string OverallBidWinContainerPerTargetGroup::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}
