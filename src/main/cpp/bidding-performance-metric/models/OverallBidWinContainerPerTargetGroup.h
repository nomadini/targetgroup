#ifndef OverallBidWinContainerPerTargetGroup_h
#define OverallBidWinContainerPerTargetGroup_h

#include "AtomicLong.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTime.h"
#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"
#include "AtomicDouble.h"

class OverallBidWinContainerPerTargetGroup;


/*
   represents a view of performance that compares bids and wins recorded
   by all bidders and all adservers...
   this will be used by ComparatorService to check if we have a problem with
   our total win percentages
 */
class OverallBidWinContainerPerTargetGroup {

public:
int targetGroupId;
Poco::DateTime timeReported;

std::shared_ptr<gicapods::AtomicLong> numberOfBids;
std::shared_ptr<gicapods::AtomicDouble> sumOfBidPrices;

std::shared_ptr<gicapods::AtomicLong> numberOfWins;
std::shared_ptr<gicapods::AtomicDouble> platformCostSpentOnWins;

OverallBidWinContainerPerTargetGroup();
virtual ~OverallBidWinContainerPerTargetGroup();

std::string toString();
static std::shared_ptr<OverallBidWinContainerPerTargetGroup> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::string toJson();
};

#endif
