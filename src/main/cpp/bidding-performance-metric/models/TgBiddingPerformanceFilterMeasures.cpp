#include "PacingPlan.h"
#include "TgBiddingPerformanceFilterMeasures.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "GUtil.h"
#include "TestUtil.h"
#include "StringUtil.h"

#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormat.h"
#include "DateTimeUtil.h"
#include "CollectionUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>
#include "NetworkUtil.h"
#include "JsonArrayUtil.h"
TgBiddingPerformanceFilterMeasures::TgBiddingPerformanceFilterMeasures() {
        targetGroupId = 0;
        numberOfBidsInLastMinuteByThisBidder =  std::make_shared<gicapods::AtomicLong>();
        numberOfBidsInLastHourByThisBidder = std::make_shared<gicapods::AtomicLong>();
        numberOfBidsMadeTodayByThisBidder = std::make_shared<gicapods::AtomicLong>();

        sumOfBidPricesInLastMinuteByThisBidder =  std::make_shared<gicapods::AtomicDouble>();
        sumOfBidPricesInLastHourByThisBidder =  std::make_shared<gicapods::AtomicDouble>();
        sumOfBidPricesMadeTodayByThisBidder =  std::make_shared<gicapods::AtomicDouble>();

        sumOfExchangeCostWonInLastHour =  std::make_shared<gicapods::AtomicDouble>();
        sumOfPlatformCostWonInLastHour =  std::make_shared<gicapods::AtomicDouble>();
        sumOfAdvertiserCostWonInLastHour =  std::make_shared<gicapods::AtomicDouble>();
        sumOfImpressionsWonInLastHour =  std::make_shared<gicapods::AtomicLong>();
}

TgBiddingPerformanceFilterMeasures::~TgBiddingPerformanceFilterMeasures() {

}

std::string TgBiddingPerformanceFilterMeasures::mapToJson(
        tbb::concurrent_hash_map<int, std::shared_ptr<TgBiddingPerformanceFilterMeasures> > targetGroupIdToPerformanceMetricMap) {

        std::vector<std::shared_ptr<TgBiddingPerformanceFilterMeasures> > allMetrics;

        for(auto const& entry : targetGroupIdToPerformanceMetricMap) {
                allMetrics.push_back(entry.second);
        }
        std::string json = JsonArrayUtil::convertListToJson(allMetrics);
        return json;
}

std::string TgBiddingPerformanceFilterMeasures::toString() {
        return toJson ();
}

std::shared_ptr<TgBiddingPerformanceFilterMeasures> TgBiddingPerformanceFilterMeasures::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<TgBiddingPerformanceFilterMeasures> obj = std::make_shared<TgBiddingPerformanceFilterMeasures>();

        obj->targetGroupId = JsonUtil::GetIntSafely(value, "targetGroupId");

        obj->numberOfBidsMadeTodayByThisBidder->setValue(JsonUtil::GetLongSafely(value, "numberOfBidsMadeTodayByThisBidder"));
        obj->numberOfBidsInLastMinuteByThisBidder->setValue(JsonUtil::GetLongSafely(value, "numberOfBidsInLastMinuteByThisBidder"));
        obj->numberOfBidsInLastHourByThisBidder->setValue(JsonUtil::GetLongSafely(value, "numberOfBidsInLastHourByThisBidder"));

        obj->sumOfBidPricesInLastMinuteByThisBidder->setValue(JsonUtil::GetLongSafely(value, "sumOfBidPricesInLastMinuteByThisBidder"));
        obj->sumOfBidPricesInLastHourByThisBidder->setValue(JsonUtil::GetLongSafely(value, "sumOfBidPricesInLastHourByThisBidder"));
        obj->sumOfBidPricesMadeTodayByThisBidder->setValue(JsonUtil::GetLongSafely(value, "sumOfBidPricesMadeTodayByThisBidder"));

        return obj;
}


void TgBiddingPerformanceFilterMeasures::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "numberOfBidsInLastMinuteByThisBidder", numberOfBidsInLastMinuteByThisBidder->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "numberOfBidsInLastHourByThisBidder", numberOfBidsInLastHourByThisBidder->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "numberOfBidsMadeTodayByThisBidder", numberOfBidsMadeTodayByThisBidder->getValue(), value);


        JsonUtil::addMemberToValue_FromPair (doc, "sumOfBidPricesInLastMinuteByThisBidder", sumOfBidPricesInLastMinuteByThisBidder->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "sumOfBidPricesInLastHourByThisBidder", sumOfBidPricesInLastHourByThisBidder->getValue(), value);
        JsonUtil::addMemberToValue_FromPair (doc, "sumOfBidPricesMadeTodayByThisBidder", sumOfBidPricesMadeTodayByThisBidder->getValue(), value);
}


std::string TgBiddingPerformanceFilterMeasures::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}
