#ifndef TargetGroupBiddingPerformanceMetric_h
#define TargetGroupBiddingPerformanceMetric_h

#include "AtomicLong.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTime.h"
#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "AtomicLong.h"
#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"
#include "AtomicDouble.h"
class TgBiddingPerformanceFilterMeasures;



/*
   encapsulates all the criteria that we have filters in place to check against certain limits set in db

   each measure will be reset after its period is over.

   for example if a measure is about last hour, it will be reset every hour

 */
class TgBiddingPerformanceFilterMeasures {

public:
int targetGroupId;

/*
   helps us not to overbidding per minute for a tg
 */
std::shared_ptr<gicapods::AtomicLong> numberOfBidsInLastMinuteByThisBidder;

/*
   some hard coded limit will be compared against this, so we don't overbid for a tg
 */
std::shared_ptr<gicapods::AtomicLong> numberOfBidsInLastHourByThisBidder;

/*
   some hard coded limit will be compared against this, so we don't overbid for a tg
 */
std::shared_ptr<gicapods::AtomicLong> numberOfBidsMadeTodayByThisBidder;



std::shared_ptr<gicapods::AtomicDouble> sumOfBidPricesInLastMinuteByThisBidder;

std::shared_ptr<gicapods::AtomicDouble> sumOfBidPricesInLastHourByThisBidder;

std::shared_ptr<gicapods::AtomicDouble> sumOfBidPricesMadeTodayByThisBidder;


std::shared_ptr<gicapods::AtomicDouble> sumOfExchangeCostWonInLastHour;
std::shared_ptr<gicapods::AtomicDouble> sumOfPlatformCostWonInLastHour;
std::shared_ptr<gicapods::AtomicDouble> sumOfAdvertiserCostWonInLastHour;
std::shared_ptr<gicapods::AtomicLong> sumOfImpressionsWonInLastHour;

TgBiddingPerformanceFilterMeasures();
virtual ~TgBiddingPerformanceFilterMeasures();

static std::string mapToJson(
        tbb::concurrent_hash_map<int, std::shared_ptr<TgBiddingPerformanceFilterMeasures> > targetGroupIdToPerformanceMetricMap);


std::string toString();
static std::shared_ptr<TgBiddingPerformanceFilterMeasures> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
std::string toJson();
};

#endif
