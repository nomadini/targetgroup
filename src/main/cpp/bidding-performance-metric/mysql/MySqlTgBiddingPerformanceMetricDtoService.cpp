#include "GUtil.h"
#include "TargetGroup.h"
#include "Creative.h"
#include "MySqlTgBiddingPerformanceMetricDtoService.h"
#include "MySqlDriver.h"
#include "PacingPlan.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "CollectionUtil.h"
#include "ConverterUtil.h"
#include "DateTimeUtil.h"
#include <boost/foreach.hpp>


MySqlTgBiddingPerformanceMetricDtoService::
MySqlTgBiddingPerformanceMetricDtoService(MySqlDriver* driverArg) {
        this->driver = driverArg;
        this->coreFields =
                " TARGET_GROUP_ID,"
                " NUM_BIDS,"
                " CONFIRMED_WINS_BY_ADSERVERS,"
                " SUM_BID_PRICES,"
                " HOST_NAME,"
                " TIME_REPORTED";

        this->summedUpFields =
                " TARGET_GROUP_ID,"
                " SUM(NUM_BIDS) AS NUM_BIDS,"
                " SUM(CONFIRMED_WINS_BY_ADSERVERS) AS CONFIRMED_WINS_BY_ADSERVERS,"
                " SUM(SUM_BID_PRICES) AS SUM_BID_PRICES,"
                " MAX(HOST_NAME) AS HOST_NAME ," //in case hostname is not in a group by clause
                " MAX(TIME_REPORTED) AS TIME_REPORTED";
}

void MySqlTgBiddingPerformanceMetricDtoService::insert(
        std::vector<std::shared_ptr<BidConfirmedWinsContainerPerTargetGroupAndBidder>> allObjects) {
        if (allObjects.empty()) {
                return;
        }

        std::string queryStr ="INSERT INTO TgBiddingPerformanceMetric ("
                               + coreFields +
                               " ) VALUES __VALUES_OF_ALLROW__  ;";


        std::vector<std::string> allValues;

        for(auto obj :  allObjects) {
                auto oneRecordValues = convertRecordToValueString(obj);
                allValues.push_back(oneRecordValues);
                MLOG(3) << "queryStr : " << queryStr;
        }

        auto properAllValues = StringUtil::joinArrayAsString(allValues, ",");
        queryStr = StringUtil::replaceString (queryStr, "__VALUES_OF_ALLROW__", properAllValues);
        MLOG(3) << "insert queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);



}

std::string MySqlTgBiddingPerformanceMetricDtoService::
convertRecordToValueString(std::shared_ptr<BidConfirmedWinsContainerPerTargetGroupAndBidder> record) {
        if(record->numberOfBids->getValue() > 0) {
                assertAndThrow(record->sumOfBidPrices->getValue() > 0);
        }
        std::string oneRowValue =
                " "
                " ("
                " __TARGET_GROUP_ID__,"
                " __NUM_BIDS__,"
                " __CONFIRMED_WINS_BY_ADSERVERS__,"
                " __SUM_BID_PRICES__,"
                " '__HOST_NAME__',"
                " '__TIME_REPORTED__'"
                " ) ";

        oneRowValue = StringUtil::replaceString (oneRowValue, "__TARGET_GROUP_ID__",
                                                 StringUtil::toStr(record->targetGroupId));
        oneRowValue = StringUtil::replaceString (oneRowValue, "__NUM_BIDS__",
                                                 StringUtil::toStr(record->numberOfBids->getValue()));
        oneRowValue = StringUtil::replaceString (oneRowValue, "__CONFIRMED_WINS_BY_ADSERVERS__",
                                                 StringUtil::toStr(record->confirmedWinsByAdServers->getValue()));

        oneRowValue = StringUtil::replaceString (oneRowValue, "__SUM_BID_PRICES__",
                                                 StringUtil::toStr(record->sumOfBidPrices->getValue()));

        oneRowValue = StringUtil::replaceString (oneRowValue, "__HOST_NAME__",
                                                 record->hostname);

        oneRowValue = StringUtil::replaceString (oneRowValue, "__TIME_REPORTED__",
                                                 DateTimeUtil::dateTimeToStr(record->timeReported));

        return oneRowValue;
}


void MySqlTgBiddingPerformanceMetricDtoService::
readsumOfHourlyBiddingInfoByThisHost(
        tbb::concurrent_hash_map<int, std::shared_ptr<TgBiddingPerformanceFilterMeasures>>* allTargetGroupsMap, std::string hostName) {

        std::string queryStr =
                "SELECT "
                + summedUpFields +
                " FROM `TgBiddingPerformanceMetric` where "
                " TIME_REPORTED >= '__ONE_HOUR_AGO__' and HOST_NAME= '__HOST_NAME__' group by TARGET_GROUP_ID  ";


        queryStr = StringUtil::replaceString (queryStr, "__ONE_HOUR_AGO__",
                                              DateTimeUtil::getNowPlusHoursInMySqlFormat(-1));

        queryStr = StringUtil::replaceString (queryStr, "__HOST_NAME__",
                                              hostName);


        auto res = driver->executeQuery (queryStr);

        while (res->next ()) {
                tbb::concurrent_hash_map<int, std::shared_ptr<TgBiddingPerformanceFilterMeasures>>::accessor accessor;

                int targetGroupId = res->getInt("TARGET_GROUP_ID");
                if (allTargetGroupsMap->find(accessor, targetGroupId)) {
                        accessor->second->numberOfBidsInLastHourByThisBidder->setValue(res->getInt ("NUM_BIDS"));
                        accessor->second->sumOfBidPricesInLastHourByThisBidder->setValue(res->getDouble ("SUM_BID_PRICES"));

                }
        }


}


void MySqlTgBiddingPerformanceMetricDtoService::
readSumOfLastHourWinsByAllBidders(
        tbb::concurrent_hash_map<int, std::shared_ptr<TgBiddingPerformanceFilterMeasures>>* allTargetGroupsMap) {

        std::string queryStr =
                "SELECT "
                " targetgroup_id, "
                " sum(exchangeCostInCpm) as exchangeCostInCpm, "
                " sum(platformCostInCpm) as platformCostInCpm, "
                " sum(advertiserCostInCpm) as advertiserCostInCpm, "
                " sum(count) as numberOfImpressionsWon "
                " FROM `impression_compressed` where created_at >= '__ONE_HOUR_AGO__' group by targetgroup_id  ";


        queryStr = StringUtil::replaceString (queryStr, "__ONE_HOUR_AGO__",
                                              DateTimeUtil::getNowPlusHoursInMySqlFormat(-1));


        auto res = driver->executeQuery (queryStr);

        while (res->next ()) {
                tbb::concurrent_hash_map<int, std::shared_ptr<TgBiddingPerformanceFilterMeasures>>::accessor accessor;
                int targetGroupId = res->getInt("targetgroup_id");
                if (allTargetGroupsMap->find(accessor, targetGroupId)) {
                        accessor->second->sumOfExchangeCostWonInLastHour->setValue(res->getDouble ("exchangeCostInCpm"));
                        accessor->second->sumOfPlatformCostWonInLastHour->setValue(res->getDouble ("platformCostInCpm"));
                        accessor->second->sumOfAdvertiserCostWonInLastHour->setValue(res->getDouble ("advertiserCostInCpm"));
                        accessor->second->sumOfImpressionsWonInLastHour->setValue(res->getLong ("numberOfImpressionsWon"));
                }
        }
}

void MySqlTgBiddingPerformanceMetricDtoService::
readsumOfDailyBiddingInfoByThisHost(
        tbb::concurrent_hash_map<int, std::shared_ptr<TgBiddingPerformanceFilterMeasures>>* allTargetGroupsMap, std::string hostName) {


        std::string queryStr =
                "SELECT "
                + summedUpFields +
                " FROM `TgBiddingPerformanceMetric` where "
                " HOST_NAME ='__HOST_NAME__' and  TIME_REPORTED >= 'BEG_TODAY_DATE'  group by TARGET_GROUP_ID  ";

        queryStr = StringUtil::replaceString (queryStr, "BEG_TODAY_DATE",
                                              DateTimeUtil::dateTimeToStr(
                                                      DateTimeUtil::getCurrentDateWithDayPercision()
                                                      ));

        queryStr = StringUtil::replaceString (queryStr, "__HOST_NAME__",
                                              hostName);

        auto res = driver->executeQuery (queryStr);

        while (res->next ()) {
                tbb::concurrent_hash_map<int, std::shared_ptr<TgBiddingPerformanceFilterMeasures>>::accessor accessor;
                int targetGroupId = res->getInt(1);
                if (allTargetGroupsMap->find(accessor, targetGroupId)) {
                        accessor->second->numberOfBidsMadeTodayByThisBidder->setValue(res->getInt ("NUM_BIDS"));
                        accessor->second->sumOfBidPricesMadeTodayByThisBidder->setValue(res->getDouble ("SUM_BID_PRICES"));

                }
        }


}

std::vector<std::shared_ptr<BidConfirmedWinsContainerPerTargetGroupAndBidder>>
MySqlTgBiddingPerformanceMetricDtoService::
readAllDeliveryInfoPastDate(std::string hostName, Poco::DateTime timeReported) {

        std::vector<std::shared_ptr<BidConfirmedWinsContainerPerTargetGroupAndBidder>> all;


        std::string queryStr = "SELECT "
                               + summedUpFields +
                               " FROM `TgBiddingPerformanceMetric` "
                               " WHERE "
                               " HOST_NAME ='__HOST_NAME__' and "
                               " TIME_REPORTED >= 'BEG_TODAY_DATE' "
                               " GROUP BY TARGET_GROUP_ID  ";

        queryStr = StringUtil::replaceString (queryStr, "__HOST_NAME__",
                                              hostName);

        queryStr = StringUtil::replaceString (queryStr, "BEG_TODAY_DATE",
                                              DateTimeUtil::dateTimeToStr(
                                                      timeReported
                                                      ));

        MLOG(3) << "queryStr : "<< queryStr;
        auto res = driver->executeQuery (queryStr);

        while (res->next ()) {
                auto obj = std::make_shared<BidConfirmedWinsContainerPerTargetGroupAndBidder>();
                obj->targetGroupId = res->getInt ("TARGET_GROUP_ID");
                obj->numberOfBids->setValue(res->getInt ("NUM_BIDS"));
                obj->confirmedWinsByAdServers->setValue(res->getInt ("CONFIRMED_WINS_BY_ADSERVERS"));
                obj->sumOfBidPrices->setValue(res->getDouble ("SUM_BID_PRICES"));
                obj->hostname = MySqlDriver::getString( res, "HOST_NAME");
                obj->timeReported = MySqlDriver::parseDateTime(res, "TIME_REPORTED");

                all.push_back(obj);
        }
        return all;
}

void MySqlTgBiddingPerformanceMetricDtoService::deleteDataOlderThanNDays(int days) {
        std::string query = "DELETE  "
                            " FROM `TgBiddingPerformanceMetric` where "
                            "  TIME_REPORTED <= '__TIME_REPORTED__' ";


        query = StringUtil::replaceString (query, "__TIME_REPORTED__",
                                           DateTimeUtil::
                                           getNowPlusDaysInMySqlFormat(-1 * days));

        driver->executedUpdateStatement (query);
        MLOG(3) << "query to delete was : "<<query;
}

MySqlTgBiddingPerformanceMetricDtoService::~MySqlTgBiddingPerformanceMetricDtoService() {
}
