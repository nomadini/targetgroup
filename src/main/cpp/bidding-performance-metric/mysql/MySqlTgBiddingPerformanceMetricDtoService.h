#ifndef MySqlTgBiddingPerformanceMetricDtoService_h
#define MySqlTgBiddingPerformanceMetricDtoService_h


#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "mysql_connection.h"
#include <unordered_map>
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "TargetGroupTestHelper.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "TgBiddingPerformanceFilterMeasures.h"
class MySqlTgBiddingPerformanceMetricDtoService;




class MySqlTgBiddingPerformanceMetricDtoService {

public:

MySqlDriver* driver;

std::string coreFields;
std::string summedUpFields;

MySqlTgBiddingPerformanceMetricDtoService(MySqlDriver* driver);

void insert(std::vector<std::shared_ptr<BidConfirmedWinsContainerPerTargetGroupAndBidder>> allObjects);

std::string convertRecordToValueString(std::shared_ptr<BidConfirmedWinsContainerPerTargetGroupAndBidder> record);

void readsumOfHourlyBiddingInfoByThisHost(
        tbb::concurrent_hash_map<int, std::shared_ptr<TgBiddingPerformanceFilterMeasures> >* allTargetGroupsMap, std::string hostName);

void readsumOfDailyBiddingInfoByThisHost(
        tbb::concurrent_hash_map<int, std::shared_ptr<TgBiddingPerformanceFilterMeasures>>* allTargetGroupsMap, std::string hostName);

std::vector<std::shared_ptr<BidConfirmedWinsContainerPerTargetGroupAndBidder>>
readAllDeliveryInfoPastDate(std::string hostName, Poco::DateTime timeReported);

void readSumOfLastHourWinsByAllBidders(
        tbb::concurrent_hash_map<int, std::shared_ptr<TgBiddingPerformanceFilterMeasures>>* allTargetGroupsMap);

void deleteDataOlderThanNDays(int days);

virtual ~MySqlTgBiddingPerformanceMetricDtoService();

};

#endif
