#include "GUtil.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "MySqlTgPerformanceTrackService.h"
#include "MySqlDriver.h"
#include "PacingPlan.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "CollectionUtil.h"
#include "ConverterUtil.h"
#include "DateTimeUtil.h"
#include <boost/foreach.hpp>


MySqlTgPerformanceTrackService::
MySqlTgPerformanceTrackService(MySqlDriver* driver) {
        this->driver = driver;
}

std::vector<std::shared_ptr<OverallBidWinContainerPerTargetGroup>>
MySqlTgPerformanceTrackService::readAllTargetGroupPerformanceTracksBetweenDates(Poco::DateTime startDate, Poco::DateTime endDate) {
        NULL_CHECK(driver);
        assertAndThrow(endDate > startDate);

        std::vector<std::shared_ptr<OverallBidWinContainerPerTargetGroup>> all;


        std::string queryStr =
                "  select tg.ID as tgId ,"
                "  numberOfBids,"
                "  sumOfBidPrices,"
                "  numberOfWins,"
                "  platformCostSpentByAdServer "
                "     from targetgroup tg"
                "     left join ("
                "       SELECT targetgroup_id, "
                "              count(*) as numberOfWins,"
                "                sum(platformCostInCpm) / 1000 as platformCostSpentByAdServer "
                "              FROM impression_compressed WHERE created_at >= '__START_PERIOD__' AND "
                "              created_at <= '__END_PERIOD__' group by targetgroup_id) tgw on tg.ID = tgw.targetgroup_id "
                " "
                "     left join ( "
                "       SELECT TARGET_GROUP_ID,  "
                "              sum(NUM_BIDS) as numberOfBids,  "
                "            sum(SUM_BID_PRICES) / 1000 as sumOfBidPrices  "
                "            FROM TgBiddingPerformanceMetric WHERE TIME_REPORTED >= '__START_PERIOD__' AND "
                "                 TIME_REPORTED <= '__END_PERIOD__' group by TARGET_GROUP_ID)  tgb on tg.ID = tgb.TARGET_GROUP_ID "
                " "
                " group by tg.ID  ";

        queryStr = StringUtil::replaceString (queryStr, "__START_PERIOD__",
                                              DateTimeUtil::dateTimeToStr(
                                                      startDate
                                                      ));

        queryStr = StringUtil::replaceString (queryStr, "__END_PERIOD__",
                                              DateTimeUtil::dateTimeToStr(
                                                      endDate
                                                      ));
        auto res = driver->executeQuery (queryStr);

        while (res->next ()) {
                auto obj = std::make_shared<OverallBidWinContainerPerTargetGroup>();

                obj->targetGroupId = res->getInt ("tgId");
                obj->numberOfBids->setValue(res->getInt ("numberOfBids"));
                obj->sumOfBidPrices->setValue(res->getDouble ("sumOfBidPrices"));        //TODO : fix these later

                obj->numberOfWins->setValue(res->getInt ("numberOfWins"));
                obj->platformCostSpentOnWins->setValue(res->getDouble ("platformCostSpentByAdServer"));

                MLOG(3) << "reading TgBiddingPerformanceMetric : " << obj->toJson();

                all.push_back(obj);
        }



        return all;

}
MySqlTgPerformanceTrackService::~MySqlTgPerformanceTrackService() {
}
