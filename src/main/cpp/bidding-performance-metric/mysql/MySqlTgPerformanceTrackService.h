#ifndef MySqlTgPerformanceTrackService_h
#define MySqlTgPerformanceTrackService_h


#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "mysql_connection.h"
#include <unordered_map>
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "TargetGroupTestHelper.h"
#include "OverallBidWinContainerPerTargetGroup.h"
#include "TgBiddingPerformanceFilterMeasures.h"
#include "TgBiddingPerformanceFilterMeasures.h"
class DateTimeUtil;
class MySqlTgPerformanceTrackService;




class MySqlTgPerformanceTrackService {

public:

MySqlDriver* driver;

MySqlTgPerformanceTrackService(MySqlDriver* driver);

//will compare the number of bids and wins and the platform costs across all servers
//this function will compare the impresison table data with TgBiddingPerformanceMetric data
//we expect the queries to be the for the last hour or n minutes, so we use imprssion table
//rather than impressio_compressed table
std::vector<std::shared_ptr<OverallBidWinContainerPerTargetGroup>> readAllTargetGroupPerformanceTracksBetweenDates(Poco::DateTime startDate, Poco::DateTime endDate);
virtual ~MySqlTgPerformanceTrackService();
};

#endif
