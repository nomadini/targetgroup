#include "GUtil.h"
#include "MySqlTargetGroupOfferService.h"
#include "MySqlDriver.h"
#include "DateTimeUtil.h"
#include "TargetGroupOffer.h"
MySqlTargetGroupOfferService::MySqlTargetGroupOfferService(MySqlDriver* driver) : MySqlService(driver, nullptr) {
        this->driver = driver;
        allFields =
                " `id`, "
                " `targetgroup_id`, "
                " `offer_id`, "
                " `created_at`,"
                " `updated_at` ";
}

std::vector<std::shared_ptr<TargetGroupOffer> > MySqlTargetGroupOfferService::readAllEntities() {
        return this->readAll();
}
std::string MySqlTargetGroupOfferService::getSelectAllQueryStatement() {
        static std::string selectAllQueryStatement = " SELECT "
                                                     + allFields +
                                                     " FROM `targetgroup_offer_map`";
        return selectAllQueryStatement;
}

std::string MySqlTargetGroupOfferService::getInsertObjectSqlStatement(std::shared_ptr<TargetGroupOffer> campaign) {
        std::string queryStr =
                " INSERT INTO `targetgroup_offer_map`"
                " ("
                + allFields +
                ")"
                " VALUES"
                " ( "
                " __targetgroup_id__,"
                " __offer_id__,"
                " '__created_at__',"
                " '__updated_at__');";

        queryStr = StringUtil::replaceString (queryStr, "__targetgroup_id__", _toStr(campaign->targetGroupId));
        queryStr = StringUtil::replaceString (queryStr, "__offer_id__", _toStr(campaign->offerId));

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();

        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);
        return queryStr;
}

std::shared_ptr<TargetGroupOffer> MySqlTargetGroupOfferService::mapResultSetToObject(std::shared_ptr<ResultSetHolder> res) {
        std::shared_ptr<TargetGroupOffer> obj = std::make_shared<TargetGroupOffer> ();
        obj->id  = res->getInt ("id");
        obj->targetGroupId  = res->getInt ("targetgroup_id");
        obj->offerId  = res->getInt ("offer_id");
        obj->createdAt = DateTimeUtil::parseDateTime(res->getString ("created_at"));
        obj->updatedAt = DateTimeUtil::parseDateTime(res->getString ("updated_at"));

        MLOG(3) << "reading targetgroup_offer_map from mysql " << obj->toJson ();
        return obj;
}

std::string MySqlTargetGroupOfferService::getReadByIdSqlStatement(int id) {
        std::string query = "SELECT "
                            " `id`,"
                            + allFields +
                            " FROM targetgroup_offer_map where id = '__id__'";


        query = StringUtil::replaceString (query, "__id__", StringUtil::toStr(id));
        return query;
}

void MySqlTargetGroupOfferService::deleteAll() {
        driver->deleteAll("targetgroup_offer_map");
}

MySqlTargetGroupOfferService::~MySqlTargetGroupOfferService() {
}
