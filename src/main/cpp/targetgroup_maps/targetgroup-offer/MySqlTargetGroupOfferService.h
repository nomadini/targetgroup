//
// Created by Mahmoud Taabodi on 2/15/16.
//

#ifndef MySqlTargetGroupOfferService_h
#define MySqlTargetGroupOfferService_h

#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"
#include "MySqlService.h"

class TargetGroupOffer;
class MySqlTargetGroupOfferService;


class MySqlTargetGroupOfferService : public DataProvider<TargetGroupOffer>, public MySqlService<int, TargetGroupOffer> {

public:

MySqlDriver* driver;

MySqlTargetGroupOfferService(MySqlDriver* driver);
virtual ~MySqlTargetGroupOfferService();

std::string getSelectAllQueryStatement();
std::string allFields;
std::shared_ptr<TargetGroupOffer> mapResultSetToObject(std::shared_ptr<ResultSetHolder> res);
std::string getInsertObjectSqlStatement(std::shared_ptr<TargetGroupOffer> campaign);
std::string getReadByIdSqlStatement(int id);

//delete this later after you have merged MySqlService and DataProvider
std::vector<std::shared_ptr<TargetGroupOffer> > readAllEntities();
virtual void deleteAll();
};

#endif
