#include "Poco/DateTime.h"
#include "Poco/Timestamp.h"
#include "TargetGroupOffer.h"
#include "JsonUtil.h"
#include <memory>
#include "DateTimeUtil.h"

TargetGroupOffer::TargetGroupOffer() {
        id = 0;
        targetGroupId = 0;
        offerId = 0;

}


std::shared_ptr<TargetGroupOffer> TargetGroupOffer::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<TargetGroupOffer> campaign = std::make_shared<TargetGroupOffer>();

        campaign->id = JsonUtil::GetIntSafely(value, "id");
        campaign->targetGroupId = JsonUtil::GetIntSafely(value, "targetGroupId");
        campaign->offerId = JsonUtil::GetIntSafely(value, "offerId");
        campaign->createdAt = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"createdAt"));
        campaign->updatedAt = DateTimeUtil::parseDateTime(JsonUtil::GetStringSafely(value,"updatedAt"));

        return campaign;
}

std::string TargetGroupOffer::getEntityName() {
        return "TargetGroupOffer";
}
void TargetGroupOffer::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "offerId", offerId, value);

        JsonUtil::addMemberToValue_FromPair (doc, "createdAt", DateTimeUtil::dateTimeToStr(createdAt), value);
        JsonUtil::addMemberToValue_FromPair (doc, "updatedAt", DateTimeUtil::dateTimeToStr(updatedAt), value);
}

std::string TargetGroupOffer::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}

TargetGroupOffer::~TargetGroupOffer() {

}
