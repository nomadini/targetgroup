#ifndef TargetGroupOffer_h
#define TargetGroupOffer_h


class StringUtil;
#include <memory>
#include <string>
#include <vector>
#include <set>
#include <unordered_map>
#include "JsonTypeDefs.h"

#include "Poco/DateTime.h"

class TargetGroupOffer {

private:
public:
int id;
int targetGroupId;
int offerId;
Poco::DateTime createdAt;
Poco::DateTime updatedAt;

TargetGroupOffer();

std::string toJson();

virtual ~TargetGroupOffer();

static std::string getEntityName();
static std::shared_ptr<TargetGroupOffer> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};

#endif
