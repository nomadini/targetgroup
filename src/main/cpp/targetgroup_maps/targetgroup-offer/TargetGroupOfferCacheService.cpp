#include "TargetGroupOffer.h"
#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "TargetGroupCacheService.h"
#include "TargetGroupOfferCacheService.h"
#include "TargetGroup.h"
#include "MySqlTargetGroupOfferService.h"
#include "JsonArrayUtil.h"
#include "TargetGroupOffer.h"
#include "IntegerVectorHolder.h"

TargetGroupOfferCacheService::TargetGroupOfferCacheService(MySqlTargetGroupOfferService* mySqlTargetGroupOfferService,
                                                           HttpUtilService* httpUtilService,
                                                           std::string dataMasterUrl,
                                                           EntityToModuleStateStats* entityToModuleStateStats,std::string appName) :
        CacheService<TargetGroupOffer>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName) {

        mapOfOfferIdToTargetGroupIds =
                std::make_shared<gicapods::ConcurrentHashMap<int, IntegerVectorHolder >  > ();
}

void TargetGroupOfferCacheService::clearOtherCaches() {
        mapOfOfferIdToTargetGroupIds->clear();
}

void TargetGroupOfferCacheService::populateOtherMapsAndLists(std::vector<std::shared_ptr<TargetGroupOffer> > allEntities) {
        for (auto entity : allEntities) {
                auto pair = mapOfOfferIdToTargetGroupIds->getOptional(entity->offerId);
                if (pair == nullptr) {
                        auto intVector = std::make_shared<IntegerVectorHolder>();
                        mapOfOfferIdToTargetGroupIds->put(entity->offerId, intVector);
                        auto pair = mapOfOfferIdToTargetGroupIds->getOptional(entity->offerId);
                        pair->values->push_back(entity->targetGroupId);
                } else {
                        pair->values->push_back(entity->targetGroupId);
                }
        }
}
