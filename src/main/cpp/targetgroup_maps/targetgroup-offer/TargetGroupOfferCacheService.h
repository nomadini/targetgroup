#ifndef TargetGroupOfferCacheService_h
#define TargetGroupOfferCacheService_h

#include <string>
#include <memory>
#include <unordered_map>

class TargetGroupOfferCacheService;
class MySqlDriver;
class MySqlTargetGroupOfferService;
#include "HttpUtilService.h"
#include "CacheService.h"
#include "ConcurrentHashMap.h"
class TargetGroupOffer;
class IntegerVectorHolder;
class EntityToModuleStateStats;

class TargetGroupOfferCacheService;



class TargetGroupOfferCacheService : public CacheService<TargetGroupOffer> {
public:
std::shared_ptr<gicapods::ConcurrentHashMap<int, IntegerVectorHolder > > mapOfOfferIdToTargetGroupIds;

TargetGroupOfferCacheService(MySqlTargetGroupOfferService* mySqlTargetGroupOfferService,
                             HttpUtilService* httpUtilService,
                             std::string dataMasterUrl,
                             EntityToModuleStateStats* entityToModuleStateStats,std::string appName);


virtual void clearOtherCaches();

virtual void populateOtherMapsAndLists(std::vector<std::shared_ptr<TargetGroupOffer> > allEntities);

};


#endif
