#include "MySqlTargetGroupBWListMapService.h"
#include "MySqlDriver.h"
#include "DateTimeUtil.h"


MySqlTargetGroupBWListMapService::MySqlTargetGroupBWListMapService(MySqlDriver* driver) {
        this->driver = driver;
}

MySqlTargetGroupBWListMapService::~MySqlTargetGroupBWListMapService() {

}

void MySqlTargetGroupBWListMapService::insert(std::shared_ptr<TargetGroupBWListMap> obj) {
        std::string queryStr =
                " INSERT INTO `targetgroup_bwlist_map` "
                " ( "
                " `targetgroup_id`,"
                " `bwlist_id`,"
                " `created_at`,"
                " `updated_at`)"
                " VALUES "
                " ( "
                "__targetgroup_id__,"
                "__bwlist_id__,"
                "'__created_at__',"
                "'__updated_at__');";


        MLOG(3) << "inserting new TargetGroupBwListMapPtr in db : " << obj->toJson ();
        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__targetgroup_id__",
                                              StringUtil::toStr (obj->targetGroupId));
        queryStr = StringUtil::replaceString (queryStr, "__bwlist_id__",
                                              StringUtil::toStr (obj->bwListId));
        queryStr = StringUtil::replaceString (queryStr, "__created_at__",
                                              date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__",
                                              date);

        //MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);
        obj->id = driver->getLastInsertedId ();


}


std::vector<std::shared_ptr<TargetGroupBWListMap> > MySqlTargetGroupBWListMapService::readAllEntities() {
        NULL_CHECK(mySqlBWListService);
        std::vector<std::shared_ptr<TargetGroupBWListMap> > all;
        std::string query = "SELECT " //1
                            " `id`,"
                            " `targetgroup_id`,"//2
                            " `bwlist_id` "//3
                            "FROM `targetgroup_bwlist_map` ";

        auto res = driver->executeQuery (query);
        while (res->next ()) {

                std::shared_ptr<TargetGroupBWListMap> tgBWList = std::make_shared<TargetGroupBWListMap> ();
                tgBWList->id = res->getInt (1);
                tgBWList->targetGroupId = res->getInt (2);
                tgBWList->bwListId = res->getInt (3);
                tgBWList->bwList = mySqlBWListService->readWithBwEntries (tgBWList->bwListId);
                MLOG(3) << "TargetGroupBwListMapPtr loaded for tg id :  " <<
                        tgBWList->targetGroupId << ","
                        " tgBwList :  " << tgBWList->toJson ();

                all.push_back (tgBWList);
        }





        MLOG(3) << " " << all.size () << " bwlist read for all target groups";
        return all;


}

std::vector<std::shared_ptr<TargetGroupBWListMap> > MySqlTargetGroupBWListMapService::readAllByTgId(int targetGroupId) {
        NULL_CHECK(mySqlBWListService);
        std::vector<std::shared_ptr<TargetGroupBWListMap> > all;
        std::string query = "SELECT " //1
                            " `targetgroup_id`,"//2
                            " `bwlist_id` "//3
                            " FROM `targetgroup_bwlist_map` where "
                            " targetgroup_id = __targetgroup_id__ ";


        MLOG(3) << "readAll bwlists for tg : " << targetGroupId;
        //each target group has a vector which has 168 numbers which represents each hour of the week
        query = StringUtil::replaceString (query, "__targetgroup_id__", StringUtil::toStr (targetGroupId));
        MLOG(3) << "query : " << query;


        auto res = driver->executeQuery (query);

        while (res->next ()) {

                std::shared_ptr<TargetGroupBWListMap> tgBWList = std::make_shared<TargetGroupBWListMap> ();
                tgBWList->targetGroupId = res->getInt (1);
                tgBWList->bwListId = res->getInt (2);
                tgBWList->bwList = mySqlBWListService->readWithBwEntries (tgBWList->bwListId);
                MLOG(3) << "TargetGroupBwListMapPtr loaded for tg id :  " <<
                        tgBWList->targetGroupId << ","
                        " tgBwList :  " << tgBWList->toJson ();

                all.push_back (tgBWList);
        }




        MLOG(3) << " " << all.size () << " bwlist read for tg id :  " << targetGroupId;
        return all;
}

void MySqlTargetGroupBWListMapService::deleteAll() {
        driver->deleteAll("targetgroup_bwlist_map");
}
