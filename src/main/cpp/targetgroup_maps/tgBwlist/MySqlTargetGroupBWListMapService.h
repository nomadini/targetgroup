#ifndef MySqlTargetGroupBWListMapService_H
#define MySqlTargetGroupBWListMapService_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "MySqlDriver.h"
#include <cppconn/resultset.h>



#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "TargetGroupBWListMap.h"
#include "BWList.h"
#include "DataProvider.h"
#include "MySqlBWListService.h"
class MySqlTargetGroupBWListMapService;



class MySqlTargetGroupBWListMapService : public DataProvider<TargetGroupBWListMap> {

public:
MySqlDriver* driver;

MySqlBWListService* mySqlBWListService;
MySqlTargetGroupBWListMapService(MySqlDriver* driver);

virtual ~MySqlTargetGroupBWListMapService();

void insert(std::shared_ptr<TargetGroupBWListMap> obj);

std::vector<std::shared_ptr<TargetGroupBWListMap> > readAllByTgId(int targetGroupId);

void deleteAll();

std::vector<std::shared_ptr<TargetGroupBWListMap> > readAllEntities();
};

#endif
