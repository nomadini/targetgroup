
#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "TargetGroupBWListCacheService.h"
#include "MySqlTargetGroupBWListMapService.h"
#include "JsonArrayUtil.h"
#include "IntWrapper.h"
#include "TargetGroupCacheService.h"
#include "ObjectVectorHolder.h"
TargetGroupBWListCacheService::TargetGroupBWListCacheService(MySqlTargetGroupBWListMapService* mySqlTargetGroupBWListMapService,
                                                             TargetGroupCacheService* targetGroupCacheService,
                                                             HttpUtilService* httpUtilService,
                                                             std::string dataMasterUrl,
                                                             EntityToModuleStateStats* entityToModuleStateStats,std::string appName)  :
        CacheService<TargetGroupBWListMap>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName) {


        this->targetGroupCacheService = targetGroupCacheService;
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->targetGroupIdToWhiteListsMap = std::make_shared<gicapods::ConcurrentHashMap<int, gicapods::ConcurrentHashMapContainer<std::string, IntWrapper> > > ();
        this->whiteListedDomainsAssignedToTargetGroups = std::make_shared<std::vector<std::string> >();
        this->blackListedDomainsAssignedToTargetGroups = std::make_shared<std::vector<std::string> > ();
        this->whiteListedDomainToListOfTargetGroups = std::make_shared<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > ();
        this->blackListedDomainToListOfTargetGroups = std::make_shared<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > ();

}


void TargetGroupBWListCacheService::addToWhiteListDomainToListOfTargetGroups(std::shared_ptr<BWEntry> bwEntry,
                                                                             std::shared_ptr<TargetGroupBWListMap> map) {

        auto targetGroup = targetGroupCacheService->findByEntityId(map->targetGroupId);
        MLOG(10) << "inserted a target group to whitelisted domain mapping : domain : : " <<
                bwEntry->getDomainName() << " , targetGroupId " << map->targetGroupId;

        auto tgVector = getWhiteListedDomainToListOfTargetGroups ()->getOptional(StringUtil::toLowerCase (bwEntry->getDomainName()));
        if (tgVector == nullptr) {
                tgVector = std::make_shared<ObjectVectorHolder<TargetGroup> >();
        }
        tgVector->values->push_back(targetGroup);
        getWhiteListedDomainToListOfTargetGroups ()->put(StringUtil::toLowerCase (bwEntry->getDomainName()), tgVector);

}

void TargetGroupBWListCacheService::addToBlackListedDomainToListOfTargetGroups(std::shared_ptr<BWEntry> bwEntry,
                                                                               std::shared_ptr<TargetGroupBWListMap> map) {

        auto targetGroup = targetGroupCacheService->findByEntityId(map->targetGroupId);
        MLOG(10) << "inserted a target group to blacklist domain mapping : domain : " <<
                bwEntry->getDomainName() << " , targetGroupId " << map->targetGroupId;

        auto tgVector = getBlackListedDomainToListOfTargetGroups ()->getOptional(StringUtil::toLowerCase (bwEntry->getDomainName()));
        if (tgVector == nullptr) {
                tgVector = std::make_shared<ObjectVectorHolder<TargetGroup> >();
        }
        tgVector->values->push_back(targetGroup);

        getBlackListedDomainToListOfTargetGroups ()->put(StringUtil::toLowerCase (bwEntry->getDomainName()), tgVector);
}

void TargetGroupBWListCacheService::addToTargetGroupIdToWhiteListsMap(
        std::shared_ptr<BWEntry> bwEntry,
        std::shared_ptr<TargetGroupBWListMap> map) {

        MLOG(3) << "adding a whitelist for target group id: " << map->targetGroupId;

        auto mapContainer = targetGroupIdToWhiteListsMap->getOptional (map->targetGroupId);
        if (mapContainer != nullptr) {
                MLOG(3) << "target group id: " << map->targetGroupId << " was found in the whitelistmap";
                auto domainFound = mapContainer->map->exists (StringUtil::toLowerCase (bwEntry->getDomainName()));
                if (!domainFound) {
                        MLOG(3) << "adding  whitelist domina for target group id: " << map->targetGroupId <<
                                " , whitelist domain  of " << bwEntry->getDomainName();
                        auto integerObject = std::make_shared<IntWrapper>(1);
                        mapContainer->map->put(
                                StringUtil::toLowerCase (bwEntry->getDomainName()), integerObject);
                }

        } else {
                MLOG(3) << "adding bwEntry to whitelist map: " << map->targetGroupId << "  bwEntry->domainName : " <<
                        bwEntry->getDomainName();
                auto newInnerMap = std::make_shared<gicapods::ConcurrentHashMapContainer<std::string, IntWrapper> >();
                auto integerObject = std::make_shared<IntWrapper>(1);
                newInnerMap->map->put(StringUtil::toLowerCase (bwEntry->getDomainName()), integerObject);
                targetGroupIdToWhiteListsMap->put(map->targetGroupId, newInnerMap);
        }
}


//TODO : write tests for this //this should not use mysql service, it should have all the data it needs
void TargetGroupBWListCacheService::populateOtherMapsAndLists(std::vector<std::shared_ptr<TargetGroupBWListMap> > allMapping) {
        for(std::shared_ptr<TargetGroupBWListMap> map :  allMapping) {
                MLOG (10)<<"reading TargetGroupBWListMap , targetGroupId : " <<map->targetGroupId << " , bwListId : "<< map->bwListId;
                std::shared_ptr<BWList> bwList = map->bwList;
                if (bwList->getBwEntries ()->empty ()) {
                        continue;
                }


                for(std::shared_ptr<BWEntry> bwEntry :  *bwList->getBwEntries ()) {
                        if (StringUtil::equalsIgnoreCase (bwList->getListType(), BWList::BLACK_LIST_TYPE)) {
                                addToBlackListedDomainToListOfTargetGroups (bwEntry, map);
                                getBlackListedDomainsAssignedToTargetGroups ()->push_back (bwEntry->getDomainName());
                        }
                }

                for(std::shared_ptr<BWEntry> bwEntry :  *bwList->getBwEntries ()) {
                        if (!StringUtil::equalsIgnoreCase (bwList->getListType(), BWList::WHITE_LIST_TYPE)) {
                                continue;
                        }

                        addToWhiteListDomainToListOfTargetGroups (bwEntry, map);
                        addToTargetGroupIdToWhiteListsMap (bwEntry, map);
                        getWhiteListedDomainsAssignedToTargetGroups ()->push_back (bwEntry->getDomainName());
                }

        }
}


std::shared_ptr<std::vector<std::string> > TargetGroupBWListCacheService::getWhiteListedDomainsAssignedToTargetGroups() {
        return this->whiteListedDomainsAssignedToTargetGroups;
}

std::shared_ptr<std::vector<std::string> > TargetGroupBWListCacheService::getBlackListedDomainsAssignedToTargetGroups() {
        return this->blackListedDomainsAssignedToTargetGroups;
}

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > >
TargetGroupBWListCacheService::getWhiteListedDomainToListOfTargetGroups() {
        return this->whiteListedDomainToListOfTargetGroups;
}

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > >
TargetGroupBWListCacheService::getBlackListedDomainToListOfTargetGroups() {
        return this->blackListedDomainToListOfTargetGroups;
}

std::shared_ptr<gicapods::ConcurrentHashMap<int, gicapods::ConcurrentHashMapContainer<std::string, IntWrapper> > >
TargetGroupBWListCacheService::getTargetGroupIdToWhiteListsMap() {
        return targetGroupIdToWhiteListsMap;
};


std::string TargetGroupBWListCacheService::getEntityName() {
        return "TargetGroupBWListMap";
}
void TargetGroupBWListCacheService::clearOtherCaches() {
        MLOG(10) << "clearing all the targetgroup caches";

        this->targetGroupIdToWhiteListsMap->clear ();
        this->whiteListedDomainsAssignedToTargetGroups->clear ();
        this->blackListedDomainsAssignedToTargetGroups->clear ();
        this->whiteListedDomainToListOfTargetGroups->clear ();
        this->blackListedDomainToListOfTargetGroups->clear ();
}
