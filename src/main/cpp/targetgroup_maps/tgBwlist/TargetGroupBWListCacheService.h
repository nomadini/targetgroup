#ifndef TargetGroupBWListCacheService_h
#define TargetGroupBWListCacheService_h

#include <string>
#include <memory>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "TargetGroupBWListMap.h"
#include "MySqlTargetGroupBWListMapService.h"
#include "BWEntry.h"
class MySqlDriver;
#include "TargetGroupBWListMap.h"
class TargetGroupCacheService;
#include "HttpUtilService.h"
#include "CacheService.h"
#include "EntityProviderService.h"
#include "ConcurrentHashMap.h"
#include "ObjectVectorHolder.h"
#include "ConcurrentHashMapContainer.h"
class EntityToModuleStateStats;
class TargetGroupBWListCacheService;
class IntWrapper;

class TargetGroupBWListCacheService :
        public CacheService<TargetGroupBWListMap> {

public:

//USED in Exchange Simulater App
std::shared_ptr<std::vector<std::string> > whiteListedDomainsAssignedToTargetGroups;
//USED in Exchange Simulater App
std::shared_ptr<std::vector<std::string> > blackListedDomainsAssignedToTargetGroups;
//USED in Exchange Simulater App
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > whiteListedDomainToListOfTargetGroups;



//USED In DomainWhiteListedFilter
std::shared_ptr<gicapods::ConcurrentHashMap<int, gicapods::ConcurrentHashMapContainer<std::string, IntWrapper> > > targetGroupIdToWhiteListsMap;
//USED In DomainBlackListedFilter
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > blackListedDomainToListOfTargetGroups;

TargetGroupCacheService* targetGroupCacheService;
EntityToModuleStateStats* entityToModuleStateStats;
TargetGroupBWListCacheService(MySqlTargetGroupBWListMapService* mySqlTargetGroupBWListMapService,
                              TargetGroupCacheService* targetGroupCacheService,
                              HttpUtilService* httpUtilService,
                              std::string dataMasterUrl,
                              EntityToModuleStateStats* entityToModuleStateStats,std::string appName);

void addToBlackListedDomainToListOfTargetGroups(std::shared_ptr<BWEntry> bwEntry, std::shared_ptr<TargetGroupBWListMap> map);

void addToTargetGroupIdToWhiteListsMap(std::shared_ptr<BWEntry> bwEntry, std::shared_ptr<TargetGroupBWListMap> map);

void addToWhiteListDomainToListOfTargetGroups(std::shared_ptr<BWEntry> bwEntry,std::shared_ptr<TargetGroupBWListMap> map);

std::shared_ptr<std::vector<std::string> > getWhiteListedDomainsAssignedToTargetGroups();

std::shared_ptr<std::vector<std::string> > getBlackListedDomainsAssignedToTargetGroups();

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > getWhiteListedDomainToListOfTargetGroups();

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > getBlackListedDomainToListOfTargetGroups();

std::shared_ptr<gicapods::ConcurrentHashMap<int, gicapods::ConcurrentHashMapContainer<std::string, IntWrapper> > > getTargetGroupIdToWhiteListsMap();


/** General Cache Method implementations **/
void clearOtherCaches();

void populateOtherMapsAndLists(std::vector<std::shared_ptr<TargetGroupBWListMap> > allMapping);

std::string getEntityName();
};


#endif
