
#include "StringUtil.h"
#include "MySqlDriver.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "TargetGroupBWListCacheServiceMock.h"
#include "MySqlTargetGroupBWListMapService.h"
#include "MySqlTargetGroupBWListMapService.h"
#include "JsonArrayUtil.h"
#include "TargetGroupCacheService.h"
TargetGroupBWListCacheServiceMock::
TargetGroupBWListCacheServiceMock(MySqlBWListService* mySqlBWListService,
                                  MySqlTargetGroupBWListMapService* mySqlTargetGroupBWListMapService,
                                  TargetGroupCacheService* targetGroupCacheService,
                                  HttpUtilService* httpUtilService,
                                  std::string dataMasterUrl,
                                  EntityToModuleStateStats* entityToModuleStateStats,
                                  std::string appName) :
        TargetGroupBWListCacheService(mySqlTargetGroupBWListMapService,
                                      targetGroupCacheService,
                                      httpUtilService,
                                      dataMasterUrl,
                                      entityToModuleStateStats,
                                      appName)
{


}
