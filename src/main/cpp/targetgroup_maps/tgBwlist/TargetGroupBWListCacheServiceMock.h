#ifndef TargetGroupBWListCacheServiceMock_h
#define TargetGroupBWListCacheServiceMock_h

class TargetGroupBWListCacheService;
class MySqlBWListService;
class MySqlTargetGroupBWListMapService;
class HttpUtilService;
class EntityToModuleStateStats;
class MySqlTargetGroupBWListMapService;
class TargetGroupBWListCacheService;

class TargetGroupBWListCacheServiceMock;
#include "TargetGroupBWListCacheService.h"
#include "gmock/gmock.h"



class TargetGroupBWListCacheServiceMock : public TargetGroupBWListCacheService {


public:
TargetGroupBWListCacheServiceMock(MySqlBWListService* mySqlBWListService,
                                  MySqlTargetGroupBWListMapService* mySqlTargetGroupBWListMapService,
                                  TargetGroupCacheService* targetGroupCacheService,
                                  HttpUtilService* httpUtilService,
                                  std::string dataMasterUrl,
                                  EntityToModuleStateStats* entityToModuleStateStats,
                                  std::string appName);
MOCK_METHOD1(processReadAll, std::string(std::string));
};


#endif
