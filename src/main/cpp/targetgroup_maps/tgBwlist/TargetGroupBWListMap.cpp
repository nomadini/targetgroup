
#include "TargetGroupBWListMap.h"
#include "JsonUtil.h"
#include "StringUtil.h"

TargetGroupBWListMap::TargetGroupBWListMap() {
        id = 0;
        targetGroupId=0;
        bwListId=0;
        bwList = std::make_shared<BWList>();
}

TargetGroupBWListMap::~TargetGroupBWListMap() {

}

void TargetGroupBWListMap::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "bwListId", bwListId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateCreated", dateCreated, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateModified", dateModified, value);
        JsonUtil::addMemberToValue_FromPair (doc, "bwList", bwList, value);
}

std::shared_ptr<TargetGroupBWListMap> TargetGroupBWListMap::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<TargetGroupBWListMap> geoTargetGroupBWListMapList = std::make_shared<TargetGroupBWListMap>();
        geoTargetGroupBWListMapList->id = JsonUtil::GetIntSafely(value, "id");
        geoTargetGroupBWListMapList->targetGroupId  =JsonUtil::GetIntSafely(value,"targetGroupId");
        geoTargetGroupBWListMapList->bwListId = JsonUtil::GetIntSafely(value,"bwListId");
        geoTargetGroupBWListMapList->dateCreated = JsonUtil::GetStringSafely(value,"dateCreated");
        geoTargetGroupBWListMapList->dateModified = JsonUtil::GetStringSafely(value,"dateModified");

        RapidJsonValueTypeNoRef bwListValue(rapidjson::kObjectType);
        bwListValue = value["bwList"];
        geoTargetGroupBWListMapList->bwList = BWList::fromJsonValue(bwListValue);

        return geoTargetGroupBWListMapList;
}

std::string TargetGroupBWListMap::toString() {
        return this->toJson();
}

std::string TargetGroupBWListMap::getEntityName() {
        return "TargetGroupBWListMap";
}

std::string TargetGroupBWListMap::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}

bool TargetGroupBWListMap::equals(std::shared_ptr<TargetGroupBWListMap> v1, std::shared_ptr<TargetGroupBWListMap> v2) {
        return (v1->id == v2->id) &&
               (v1->targetGroupId == v2->targetGroupId) &&
               (v1->bwListId == v2->bwListId) &&
               StringUtil::equalsIgnoreCase(v1->dateCreated, v2->dateCreated) &&
               StringUtil::equalsIgnoreCase(v1->dateModified, v2->dateModified);
}
