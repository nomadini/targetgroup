#ifndef TargetGroupBWListMap_H
#define TargetGroupBWListMap_H




#include "JsonTypeDefs.h"

#include <memory>
#include <string>
#include <vector>
#include "BWList.h"
class TargetGroupBWListMap;



class TargetGroupBWListMap {

private:

public:
int id;
int targetGroupId;
int bwListId;
std::shared_ptr<BWList> bwList;
std::string dateCreated;
std::string dateModified;

TargetGroupBWListMap();

std::string toString();
static std::string getEntityName();
static std::shared_ptr<TargetGroupBWListMap> fromJson(std::string jsonString);

std::string toJson();

virtual ~TargetGroupBWListMap();

static std::shared_ptr<TargetGroupBWListMap> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
static bool equals(std::shared_ptr<TargetGroupBWListMap> v1, std::shared_ptr<TargetGroupBWListMap> v2);
};
#endif
