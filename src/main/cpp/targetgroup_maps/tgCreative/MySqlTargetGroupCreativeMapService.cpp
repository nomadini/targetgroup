#include "MySqlTargetGroupCreativeMapService.h"
#include "MySqlDriver.h"
#include "Creative.h"

#include "JsonArrayUtil.h"
#include "CollectionUtil.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"

MySqlTargetGroupCreativeMapService::MySqlTargetGroupCreativeMapService(MySqlDriver* driver) {
        this->driver = driver;


}

std::vector<std::shared_ptr<TargetGroupCreativeMap> > MySqlTargetGroupCreativeMapService::readAllEntities() {
        return readAllAsObjects();
}

std::vector<std::shared_ptr<TargetGroupCreativeMap> > MySqlTargetGroupCreativeMapService::readAllAsObjects() {
        std::vector<std::shared_ptr<TargetGroupCreativeMap> > allObjects;
        if (mapOfCreatives.empty()) {
                assertAndThrow(mySqlCreativeService != NULL);
                auto allCreatives = mySqlCreativeService->readAllCreatives();
                for (auto && creative : allCreatives) {
                        mapOfCreatives.insert(std::make_pair(creative->getId(), creative));
                }
        }

        assertAndThrow(!mapOfCreatives.empty());

        std::string query = "SELECT targetgroup_creative_map.ID,"
                            "targetgroup_creative_map.targetgroup_id,"
                            "targetgroup_creative_map.creative_id,"
                            "targetgroup_creative_map.created_at,"
                            "targetgroup_creative_map.updated_at "
                            " FROM targetgroup_creative_map "
                            " JOIN creative on creative.ID = targetgroup_creative_map.creative_id "
                            " JOIN targetgroup on targetgroup.ID = targetgroup_creative_map.targetgroup_id "
                            "";



        auto res = driver->executeQuery (query);

        while (res->next ()) {
                auto tgCreativesMap = std::make_shared<TargetGroupCreativeMap>();

                tgCreativesMap->id = res->getInt (1);
                tgCreativesMap->targetGroupId = res->getInt (2);
                tgCreativesMap->creativeId = res->getInt (3);
                tgCreativesMap->dateCreated = MySqlDriver::getString( res, 4);
                tgCreativesMap->dateModified = MySqlDriver::getString( res, 5);
                auto pair = mapOfCreatives.find(tgCreativesMap->creativeId);
                assertAndThrow(pair != mapOfCreatives.end());
                tgCreativesMap->creative = pair->second;
                allObjects.push_back(tgCreativesMap);

        }



        return allObjects;

}
std::unordered_map<int, std::vector<int> > MySqlTargetGroupCreativeMapService::readAll() {
        std::unordered_map<int, std::vector<int> > tgCreativesMap;
        std::string query = "SELECT targetgroup_creative_map.ID,"
                            "targetgroup_creative_map.targetgroup_id,"
                            "targetgroup_creative_map.creative_id,"
                            "targetgroup_creative_map.created_at,"
                            "targetgroup_creative_map.updated_at "
                            " FROM targetgroup_creative_map "
                            " JOIN creative on creative.ID = targetgroup_creative_map.creative_id "
                            " JOIN targetgroup on targetgroup.ID = targetgroup_creative_map.targetgroup_id "
                            "";



        auto res = driver->executeQuery (query);

        while (res->next ()) {
                int tgid = res->getInt (2);
                int crid = res->getInt (3);
                auto tgKeyInMap = tgCreativesMap.find (tgid);
                if (tgKeyInMap != tgCreativesMap.end ()) {
                        //it's in map
                        tgKeyInMap->second.push_back (crid);
                } else {
                        std::vector<int> creativeIdList;
                        creativeIdList.push_back (crid);
                        tgCreativesMap.insert (std::pair<int, std::vector<int> > (tgid, creativeIdList));
                }

        }



        return tgCreativesMap;
}

void MySqlTargetGroupCreativeMapService::deleteAll() {
        driver->deleteAll("targetgroup_creative_map");
}


void MySqlTargetGroupCreativeMapService::insert(std::shared_ptr<TargetGroupCreativeMap> tg) {
        std::string queryStr =
                " INSERT INTO targetgroup_creative_map"
                " ("
                " TARGET_GROUP_ID,"
                " creative_id,"
                " created_at,"
                " updated_at"
                ")"
                " VALUES"
                " ( "
                " '__TARGET_GROUP_ID__',"
                " '__creative_id__',"
                " '__created_at__',"
                " '__updated_at__');";

        MLOG(3) << "inserting new TargetGroupCreativeMap in db : " << tg->toJson ();

        queryStr = StringUtil::replaceString (queryStr, "__TARGET_GROUP_ID__",
                                              StringUtil::toStr (tg->targetGroupId));
        queryStr = StringUtil::replaceString (queryStr, "__creative_id__", StringUtil::toStr (tg->creativeId));

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        driver->executedUpdateStatement (queryStr);
        tg->id = driver->getLastInsertedId ();



}

MySqlTargetGroupCreativeMapService::~MySqlTargetGroupCreativeMapService() {
}
