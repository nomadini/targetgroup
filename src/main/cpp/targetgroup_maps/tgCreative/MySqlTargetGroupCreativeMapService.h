#ifndef MySqlTargetGroupCreativeMapService_h
#define MySqlTargetGroupCreativeMapService_h


#include "TargetGroupCreativeMap.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"
#include "MySqlCreativeService.h"
class MySqlTargetGroupCreativeMapService;




class MySqlTargetGroupCreativeMapService : public DataProvider<TargetGroupCreativeMap> {

public:
MySqlDriver* driver;
std::unordered_map<int, std::shared_ptr<Creative>> mapOfCreatives;
MySqlCreativeService* mySqlCreativeService;
MySqlTargetGroupCreativeMapService(MySqlDriver* driver);


virtual ~MySqlTargetGroupCreativeMapService();

void parseTheAttribute(std::shared_ptr<TargetGroupCreativeMap> obj, std::string attributesFromDB);

std::unordered_map<int, std::vector<int> > readAll();

std::vector<std::shared_ptr<TargetGroupCreativeMap>> readAllAsObjects();

void insert(std::shared_ptr<TargetGroupCreativeMap> TargetGroupCreativeMap);

void deleteAll();

std::vector<std::shared_ptr<TargetGroupCreativeMap>> readAllEntities();
};

#endif
