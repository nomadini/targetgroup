#include "TargetGroup.h"
#include "StringUtil.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "TargetGroupCreativeCacheService.h"
#include "MySqlTargetGroupService.h"
#include "EntityToModuleStateStats.h"
#include "MySqlTargetGroupCreativeMapService.h"
#include "JsonArrayUtil.h"
TargetGroupCreativeCacheService::TargetGroupCreativeCacheService(MySqlTargetGroupCreativeMapService* mySqlTargetGroupCreativeMapService,
                                                                 TargetGroupCacheService* targetGroupCacheService,
                                                                 HttpUtilService* httpUtilService,
                                                                 std::string dataMasterUrl,
                                                                 EntityToModuleStateStats* entityToModuleStateStats,std::string appName)  :
        CacheService<TargetGroupCreativeMap>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName) {

        this->targetGroupCacheService = targetGroupCacheService;
        this->allTargetGroupCreativesMap = std::make_shared<gicapods::ConcurrentHashMap<int,  gicapods::ConcurrentHashMap<int, Creative> > >();
        this->entityToModuleStateStats = entityToModuleStateStats;
}

std::shared_ptr<gicapods::ConcurrentHashMap<int,  gicapods::ConcurrentHashMap<int, Creative>  > > TargetGroupCreativeCacheService::getAllTargetGroupCreativesMap() {
        return allTargetGroupCreativesMap;
}

std::string TargetGroupCreativeCacheService::getEntityName() {
        return "TargetGroupCreativeMap";
}

void TargetGroupCreativeCacheService::clearOtherCaches() {
        allTargetGroupCreativesMap->clear ();
}

void TargetGroupCreativeCacheService::populateOtherMapsAndLists(std::vector<std::shared_ptr<TargetGroupCreativeMap> > allTargetGroupCreatives) {

        entityToModuleStateStats->addStateModuleForEntity ("TG_CREATIVE_MAP_READ_FROM_DB_SIZE : " + StringUtil::toStr(allTargetGroupCreatives.size()),
                                                           "TargetGroupCreativeCacheService",
                                                           "ALL");


        for(std::shared_ptr<TargetGroupCreativeMap> targetGroupCreativeMap :  allTargetGroupCreatives) {

                auto mapOfCreativePair = allTargetGroupCreativesMap->getOptional(targetGroupCreativeMap->targetGroupId);
                if (mapOfCreativePair != nullptr) {
                        mapOfCreativePair->put(targetGroupCreativeMap->creativeId, targetGroupCreativeMap->creative );
                } else {
                        std::shared_ptr<gicapods::ConcurrentHashMap<int, Creative> >  mapOfCreatives =
                                std::make_shared<gicapods::ConcurrentHashMap<int, Creative> >();
                        mapOfCreatives->put(targetGroupCreativeMap->creativeId, targetGroupCreativeMap->creative);
                        allTargetGroupCreativesMap->put(targetGroupCreativeMap->targetGroupId, mapOfCreatives);

                }
        }

        if(!allTargetGroupCreatives.empty()) {
                if(this->allTargetGroupCreativesMap->empty()) {
                        entityToModuleStateStats->addStateModuleForEntity ("ERROR_LOADED_TG_CREATIVES",
                                                                           "TargetGroupCreativeCacheService",
                                                                           "ALL");

                }
                assertAndThrow(this->allTargetGroupCreativesMap->size() > 0);
        }

        entityToModuleStateStats->addStateModuleForEntity ("SizeOfTargetGroupCreativeMap : " + StringUtil::toStr(allTargetGroupCreativesMap->size()),
                                                           "TargetGroupCreativeCacheService",
                                                           "ALL");
}

TargetGroupCreativeCacheService::~TargetGroupCreativeCacheService() {

}
