#ifndef TargetGroupCreativeCacheService_h
#define TargetGroupCreativeCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>

#include "MySqlTargetGroupCreativeMapService.h"
class TargetGroupCacheService;
#include "CacheService.h"
#include "MySqlTargetGroupCreativeMapService.h"
#include "MySqlTargetGroupCreativeMapService.h"
#include "HttpUtilService.h"
#include "DataProvider.h"
#include "EntityProviderService.h"
#include "ConcurrentHashMap.h"
class EntityToModuleStateStats;

class TargetGroupCreativeCacheService;



class TargetGroupCreativeCacheService : public CacheService<TargetGroupCreativeMap>{

private:
std::shared_ptr<gicapods::ConcurrentHashMap<int,  gicapods::ConcurrentHashMap<int, Creative>  > > allTargetGroupCreativesMap;
EntityToModuleStateStats* entityToModuleStateStats;
public:

TargetGroupCacheService* targetGroupCacheService;

TargetGroupCreativeCacheService(MySqlTargetGroupCreativeMapService* mySqlTargetGroupCreativeMapService,
                                TargetGroupCacheService* targetGroupCacheService,
                                HttpUtilService* httpUtilService,
                                std::string dataMasterUrl,
                                EntityToModuleStateStats* entityToModuleStateStats,std::string appName);


std::shared_ptr<gicapods::ConcurrentHashMap<int,  gicapods::ConcurrentHashMap<int, Creative>  > > getAllTargetGroupCreativesMap();

void clearOtherCaches();

void populateOtherMapsAndLists(std::vector<std::shared_ptr<TargetGroupCreativeMap> > allEntities);

std::string getEntityName();

virtual ~TargetGroupCreativeCacheService();

};


#endif
