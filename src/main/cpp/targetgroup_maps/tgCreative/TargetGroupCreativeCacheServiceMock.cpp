#include "TargetGroup.h"
#include "StringUtil.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "TargetGroupCreativeCacheServiceMock.h"
#include "MySqlTargetGroupService.h"
#include "MySqlCreativeService.h"
#include "MySqlTargetGroupCreativeMapService.h"
#include "JsonArrayUtil.h"
TargetGroupCreativeCacheServiceMock::
TargetGroupCreativeCacheServiceMock(MySqlTargetGroupCreativeMapService* mySqlTargetGroupCreativeMapService,
                                    MySqlCreativeService* mySqlCreativeService,
                                    TargetGroupCacheService* targetGroupCacheService,
                                    HttpUtilService* httpUtilService,
                                    std::string dataMasterUrl,
                                    EntityToModuleStateStats* entityToModuleStateStats,
                                    std::string appName) :
        TargetGroupCreativeCacheService(mySqlTargetGroupCreativeMapService,
                                        targetGroupCacheService,
                                        httpUtilService,
                                        dataMasterUrl,
                                        entityToModuleStateStats,
                                        appName)
{

}
