#ifndef TargetGroupCreativeCacheServiceMock_h
#define TargetGroupCreativeCacheServiceMock_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
class HttpUtilService;
class EntityToModuleStateStats;
#include "MySqlTargetGroupCreativeMapService.h"
class TargetGroupCacheService;
#include "MySqlCreativeService.h"
#include "MySqlTargetGroupCreativeMapService.h"
#include "TargetGroupCreativeCacheService.h"
class TargetGroupCreativeCacheService;
#include "gmock/gmock.h"

class TargetGroupCreativeCacheServiceMock;



class TargetGroupCreativeCacheServiceMock : public TargetGroupCreativeCacheService {

public:
TargetGroupCreativeCacheServiceMock(MySqlTargetGroupCreativeMapService* mySqlTargetGroupCreativeMapService,
                                    MySqlCreativeService* mySqlCreativeService,
                                    TargetGroupCacheService* targetGroupCacheService,
                                    HttpUtilService* httpUtilService,
                                    std::string dataMasterUrl,
                                    EntityToModuleStateStats* entityToModuleStateStats,
                                    std::string appName);
MOCK_METHOD1(processReadAll, std::string(std::string));

};

#endif
