#include "GUtil.h"
#include "TargetGroupCreativeMap.h"
#include "ConverterUtil.h"
#include "JsonUtil.h"
#include <string>
#include <memory>
#include "Creative.h"
TargetGroupCreativeMap::TargetGroupCreativeMap() {
        id =0;
        targetGroupId=0;
        creativeId=0;
        creative = std::make_shared<Creative>();
}

std::string TargetGroupCreativeMap::toString() {
        return this->toJson ();
}
std::string TargetGroupCreativeMap::getEntityName() {
        return "TargetGroupCreativeMap";
}


std::shared_ptr<TargetGroupCreativeMap> TargetGroupCreativeMap::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<TargetGroupCreativeMap> obj = std::make_shared<TargetGroupCreativeMap>();

        obj->id =  JsonUtil::GetIntSafely(value, "id");
        obj->targetGroupId = JsonUtil::GetIntSafely(value, "targetGroupId");
        obj->creativeId = JsonUtil::GetIntSafely(value,"creativeId");
        obj->dateCreated = JsonUtil::GetStringSafely(value, "dateCreated");
        obj->dateModified = JsonUtil::GetStringSafely(value, "dateModified");
        RapidJsonValueTypeNoRef creativeValue(rapidjson::kObjectType);
        creativeValue = value["creative"];
        obj->creative = Creative::fromJsonValue(creativeValue);

        return obj;
}

void TargetGroupCreativeMap::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "creativeId", creativeId, value);

        JsonUtil::addMemberToValue_FromPair (doc, "dateCreated", dateCreated, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateModified", dateModified, value);
        JsonUtil::addMemberToValue_FromPair (doc, "creative", creative, value);
}

std::string TargetGroupCreativeMap::toJson() {
        auto doc = JsonUtil::createDcoumentAsObjectDoc();
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}

std::shared_ptr<TargetGroupCreativeMap> TargetGroupCreativeMap::fromJson(std::string jsonString) {
        std::shared_ptr<TargetGroupCreativeMap> tgMap = std::make_shared<TargetGroupCreativeMap> ();

        auto document = parseJsonSafely(jsonString);

        tgMap->id = JsonUtil::GetIntSafely (*document, "id");

        tgMap->targetGroupId = JsonUtil::GetIntSafely (*document, "targetGroupId");
        tgMap->creativeId = JsonUtil::GetIntSafely (*document, "creativeId");
        tgMap->dateCreated = JsonUtil::GetStringSafely (*document, "dateCreated");
        tgMap->dateModified = JsonUtil::GetStringSafely (*document, "dateModified");
        return tgMap;
}



TargetGroupCreativeMap::~TargetGroupCreativeMap() {

}
