#ifndef TargetGroupCreativeMap_H
#define TargetGroupCreativeMap_H




#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "JsonTypeDefs.h"

#include "CreativeTypeDefs.h"
class Creative;

class TargetGroupCreativeMap;



class TargetGroupCreativeMap {

private:

public:
int id;
int targetGroupId;
int creativeId;
std::shared_ptr<Creative> creative;

std::string dateCreated;
std::string dateModified;

TargetGroupCreativeMap();

std::string toString();
static std::shared_ptr<TargetGroupCreativeMap> fromJson(std::string jsonString);

std::string toJson();
static std::string getEntityName();

virtual ~TargetGroupCreativeMap();

static std::shared_ptr<TargetGroupCreativeMap> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};

#endif
