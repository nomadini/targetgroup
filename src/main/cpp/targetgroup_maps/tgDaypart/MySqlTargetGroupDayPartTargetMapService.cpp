#include "MySqlTargetGroupDayPartTargetMapService.h"
#include "MySqlDriver.h"

#include "JsonArrayUtil.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "JsonUtil.h"
#include "StringUtil.h"
#include "GUtil.h"
#include "DateTimeUtil.h"
#include <boost/foreach.hpp>
#include "TargetGroupDayPartTargetMap.h"
MySqlTargetGroupDayPartTargetMapService::MySqlTargetGroupDayPartTargetMapService(MySqlDriver* driver) {
        this->driver = driver;
}

//TODO : pass a target Group cache service and throw exception on any mapping that has a wrong targetGroupId , do this for
//all mysql services related to target Group, you have done it for some , check how they are implemented
std::unordered_map<int, std::unordered_map<int, bool> > MySqlTargetGroupDayPartTargetMapService::readAll() {

        std::unordered_map<int, std::unordered_map<int, bool> > tgHoursMap;

        std::string query = "SELECT `targetgroup_bidhour_map`.`targetgroup_id`,"
                            "`targetgroup_bidhour_map`.`hours`,"
                            "`targetgroup_bidhour_map`.`created_at`,"
                            "`targetgroup_bidhour_map`.`updated_at`"
                            "FROM `targetgroup_bidhour_map`;";

        //each target group has a vector which has 168 numbers which represents each hour of the week


        auto res = driver->executeQuery (query);

        while (res->next ()) {
                int tgid = res->getInt (1);
                std::string hoursInWeekJson = MySqlDriver::getString( res, 2);
                std::unordered_map<int, bool> oneDayHourMap;
                std::vector<int> hours = TargetGroupDayPartTargetMap::convertHoursJsonToIntVector (hoursInWeekJson);

                for(int hour :  hours) {
                        oneDayHourMap.insert(std::make_pair(hour, true));
                }

                tgHoursMap.insert (std::make_pair (tgid, oneDayHourMap));
        }
        MLOG(3) << "tgHoursMap.size() : " << StringUtil::toStr (tgHoursMap.size ());


        return tgHoursMap;

}

std::vector<std::shared_ptr<TargetGroupDayPartTargetMap> >
MySqlTargetGroupDayPartTargetMapService::readAllEntities() {
        return readAllasObjects();
}

std::vector<std::shared_ptr<TargetGroupDayPartTargetMap> >
MySqlTargetGroupDayPartTargetMapService::readAllasObjects() {

        std::vector<std::shared_ptr<TargetGroupDayPartTargetMap> >  allMapping;

        std::string query = "SELECT  `targetgroup_bidhour_map`.`ID`,"
                            "`targetgroup_bidhour_map`.`targetgroup_id`,"
                            "`targetgroup_bidhour_map`.`hours`,"
                            "`targetgroup_bidhour_map`.`created_at`,"
                            "`targetgroup_bidhour_map`.`updated_at`"
                            "FROM `targetgroup_bidhour_map`;";


        //each target group has a vector which has 168 numbers which represents each hour of the week


        auto res = driver->executeQuery (query);

        while (res->next ()) {
                std::shared_ptr<TargetGroupDayPartTargetMap> obj = std::make_shared<TargetGroupDayPartTargetMap>();
                obj->id = res->getInt (1);
                obj->targetGroupId = res->getInt (2);
                obj->hours = MySqlDriver::getString( res, 3);
                obj->hoursMap = TargetGroupDayPartTargetMap::buildHoursMap (obj->hours);
                obj->createdAt = MySqlDriver::getString( res, 4);
                obj->updatedAt = MySqlDriver::getString( res, 5);

                std::string jsonVersionOfObjectRead = obj->toJson();
                auto newObject = TargetGroupDayPartTargetMap::fromJson(jsonVersionOfObjectRead);
                assertAndThrow(newObject->id == obj->id);
                assertAndThrow(newObject->targetGroupId == obj->targetGroupId);
                assertAndThrow(StringUtil::equalsIgnoreCase(newObject->createdAt, obj->createdAt));
                assertAndThrow(StringUtil::equalsIgnoreCase(newObject->updatedAt, obj->updatedAt));


                MLOG(3)<<"originalObject->hours : " << obj->hours;
                MLOG(3)<<"newObject->hours : " << newObject->hours;
                assertAndThrow(StringUtil::equalsIgnoreCase(newObject->hours, obj->hours));
                allMapping.push_back(obj);
        }
        MLOG(3) << "allMapping size() : " <<  allMapping.size ();


        return allMapping;
}

void MySqlTargetGroupDayPartTargetMapService::deleteAll() {
        driver->deleteAll("targetgroup_bidhour_map");
}

void MySqlTargetGroupDayPartTargetMapService::deleteAllMappingFor(std::shared_ptr<TargetGroup> tg) {
        std::string queryStr = " DELETE from `targetgroup_bidhour_map` where"
                               " `targetgroup_bidhour_map`.`targetgroup_id` = "
                               + StringUtil::toStr (tg->getId ());


        //MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);




}

void MySqlTargetGroupDayPartTargetMapService::insert(std::shared_ptr<TargetGroupDayPartTargetMap> tg) {
        std::string queryStr =
                " INSERT INTO `targetgroup_bidhour_map`"
                " ( "
                " `targetgroup_id`,"
                " `hours`,"
                " `created_at`,"
                " `updated_at`"
                ")"
                " VALUES"
                " ( "
                " '__targetgroup_id__',"
                " '__hours__',"
                " '__created_at__',"
                " '__updated_at__');";

        MLOG(3) << "inserting new TargetGroupDayPartTargetMap in db : " << tg->toJson ();


        queryStr = StringUtil::replaceString (queryStr, "__targetgroup_id__",
                                              StringUtil::toStr (tg->targetGroupId));
        queryStr = StringUtil::replaceString (queryStr, "__hours__", tg->hours);

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        //MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);
        tg->id = driver->getLastInsertedId ();



}

MySqlTargetGroupDayPartTargetMapService::~MySqlTargetGroupDayPartTargetMapService() {
}
