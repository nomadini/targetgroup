#ifndef MySqlTargetGroupDayPartTargetMapService_h
#define MySqlTargetGroupDayPartTargetMapService_h


#include "TargetGroupDayPartTargetMap.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include <unordered_map>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "DataProvider.h"

class MySqlTargetGroupDayPartTargetMapService;



class MySqlTargetGroupDayPartTargetMapService  : public DataProvider<TargetGroupDayPartTargetMap> {

public:
      MySqlDriver* driver;

  	  MySqlTargetGroupDayPartTargetMapService(MySqlDriver* driver);

      virtual ~MySqlTargetGroupDayPartTargetMapService();
      void parseTheAttribute(std::shared_ptr<TargetGroupDayPartTargetMap> obj, std::string attributesFromDB);
      void insert(std::shared_ptr<TargetGroupDayPartTargetMap> TargetGroupDayPartTargetMap);
      std::unordered_map<int, std::unordered_map<int, bool>> readAll();
      std::vector<std::shared_ptr<TargetGroupDayPartTargetMap>> readAllasObjects();
      virtual void deleteAll();
      void deleteAllMappingFor(std::shared_ptr<TargetGroup> tg);
      static std::vector<int> convertHoursJsonToIntVector(std::string hoursJson);

      std::vector<std::shared_ptr<TargetGroupDayPartTargetMap>> readAllEntities();
};

#endif
