
#include "TargetGroupDayPartCacheService.h"
#include <boost/foreach.hpp>
#include "JsonArrayUtil.h"

TargetGroupDayPartCacheService::TargetGroupDayPartCacheService(
        MySqlTargetGroupDayPartTargetMapService* mySqlTargetGroupDayPartTargetMapService,
        TargetGroupCacheService* targetGroupCacheService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,std::string appName)   :
        CacheService<TargetGroupDayPartTargetMap>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName) {
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->mapOfTargetGroupsToMapOfHoursInWeek =
                std::make_shared<gicapods::ConcurrentHashMap<int, gicapods::ConcurrentHashMapContainer<int, BooleanObject> > >();
}

std::shared_ptr<gicapods::ConcurrentHashMap<int, gicapods::ConcurrentHashMapContainer<int, BooleanObject> > >
TargetGroupDayPartCacheService::getMapOfTargetGroupsToMapOfHoursInWeek() {
        return mapOfTargetGroupsToMapOfHoursInWeek;
}

void TargetGroupDayPartCacheService::clearOtherCaches() {
        mapOfTargetGroupsToMapOfHoursInWeek->clear ();
}

void TargetGroupDayPartCacheService::populateOtherMapsAndLists(std::vector<std::shared_ptr<TargetGroupDayPartTargetMap> > allEntities) {

        this->allTargetGroupsDayPartMaps =  allEntities;
        MLOG(10) << " " << allTargetGroupsDayPartMaps.size () << " allTargetGroupsDayPartMaps were loaded....";
        for(auto entity :  allTargetGroupsDayPartMaps) {
                auto basicMap = TargetGroupDayPartTargetMap::buildHoursMap(entity->hours);
                auto allHourMap = std::make_shared<gicapods::ConcurrentHashMapContainer<int, BooleanObject> >();
                for (auto&& pair : basicMap) {
                        auto obj = std::make_shared<BooleanObject>();
                        obj->value = pair.second;
                        allHourMap->map->put(pair.first, obj);
                }
                mapOfTargetGroupsToMapOfHoursInWeek->put(entity->targetGroupId, allHourMap);
        }

}
