#ifndef TargetGroupDayPartCacheService_h
#define TargetGroupDayPartCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "TargetGroupDayPartTargetMap.h"
class TargetGroupCacheService;
#include "MySqlTargetGroupDayPartTargetMapService.h"
#include "HttpUtilService.h"
#include "CacheService.h"
#include "ConcurrentHashMap.h"
#include "BooleanObject.h"
#include "IntWrapper.h"
#include "EntityProviderService.h"
#include "ConcurrentHashMapContainer.h"
class EntityToModuleStateStats;
class TargetGroupDayPartCacheService;



class TargetGroupDayPartCacheService : public CacheService<TargetGroupDayPartTargetMap> {

private:


public:

MySqlTargetGroupDayPartTargetMapService* mySqlTargetGroupDayPartTargetMapService;

std::shared_ptr<gicapods::ConcurrentHashMap<int, gicapods::ConcurrentHashMapContainer<int, BooleanObject> > > mapOfTargetGroupsToMapOfHoursInWeek;

std::vector<std::shared_ptr<TargetGroupDayPartTargetMap> > allTargetGroupsDayPartMaps;

EntityToModuleStateStats* entityToModuleStateStats;
TargetGroupDayPartCacheService(MySqlTargetGroupDayPartTargetMapService* mySqlTargetGroupDayPartTargetMapService,
                               TargetGroupCacheService* targetGroupCacheService,
                               HttpUtilService* httpUtilService,
                               std::string dataMasterUrl,
                               EntityToModuleStateStats* entityToModuleStateStats,std::string appName);

std::shared_ptr<gicapods::ConcurrentHashMap<int, gicapods::ConcurrentHashMapContainer<int, BooleanObject> > > getMapOfTargetGroupsToMapOfHoursInWeek();

/** General Cache Method implementations **/
void clearOtherCaches();

void populateOtherMapsAndLists(std::vector<std::shared_ptr<TargetGroupDayPartTargetMap> > allMapping);

};


#endif
