
#include "TargetGroupDayPartCacheServiceMock.h"
#include <boost/foreach.hpp>
#include "JsonArrayUtil.h"

TargetGroupDayPartCacheServiceMock::TargetGroupDayPartCacheServiceMock(
        MySqlTargetGroupDayPartTargetMapService* mySqlTargetGroupDayPartTargetMapService,
        TargetGroupCacheService* targetGroupCacheService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string appName) :
        TargetGroupDayPartCacheService(mySqlTargetGroupDayPartTargetMapService,
                                       targetGroupCacheService,
                                       httpUtilService,
                                       dataMasterUrl,
                                       entityToModuleStateStats,
                                       appName)
{

}
