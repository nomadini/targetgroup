#ifndef TargetGroupDayPartCacheServiceMock_h
#define TargetGroupDayPartCacheServiceMock_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "TargetGroupDayPartTargetMap.h"
class TargetGroupCacheService;
#include "MySqlTargetGroupDayPartTargetMapService.h"
#include "TargetGroupDayPartCacheService.h"
#include "gmock/gmock.h"
class EntityToModuleStateStats;

class TargetGroupDayPartCacheServiceMock;


class TargetGroupDayPartCacheServiceMock : public TargetGroupDayPartCacheService {

public:
TargetGroupDayPartCacheServiceMock(MySqlTargetGroupDayPartTargetMapService* mySqlTargetGroupDayPartTargetMapService,
                                   TargetGroupCacheService* targetGroupCacheService,
                                   HttpUtilService* httpUtilService,
                                   std::string dataMasterUrl,
                                   EntityToModuleStateStats* entityToModuleStateStats,
                                   std::string appName);
MOCK_METHOD1(processReadAll, std::string(std::string));
};


#endif
