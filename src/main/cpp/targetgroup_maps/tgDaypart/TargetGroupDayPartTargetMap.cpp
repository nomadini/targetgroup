#include "GUtil.h"
#include "TargetGroupDayPartTargetMap.h"
#include "ConverterUtil.h"
#include "CollectionUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include <string>
#include <memory>
#include "StringUtil.h"
#include <boost/foreach.hpp>
/**
 * hours is the json string that we store in DB
 * hoursMap is the logical map that we use to do the filtering based on
 */

TargetGroupDayPartTargetMap::TargetGroupDayPartTargetMap() {
        id = 0;
        targetGroupId = 0;
}


std::string TargetGroupDayPartTargetMap::getEntityName() {
        return "TargetGroupDayPartTargetMap";
}

std::string TargetGroupDayPartTargetMap::toString() {
        return this->toJson ();
}

std::shared_ptr<TargetGroupDayPartTargetMap> TargetGroupDayPartTargetMap::fromJson(std::string jsonString) {
        std::shared_ptr<TargetGroupDayPartTargetMap> obj = std::make_shared<TargetGroupDayPartTargetMap>();

        auto doc = parseJsonSafely(jsonString);
        obj->id =  JsonUtil::GetIntSafely(*doc, "id");
        obj->targetGroupId = JsonUtil::GetIntSafely(*doc, "targetGroupId");
        RapidJsonValueType value = *doc;
        obj->hours = JsonUtil::valueToString((*doc)["hours"]);
        obj->createdAt = JsonUtil::GetStringSafely(*doc, "createdAt");
        obj->updatedAt = JsonUtil::GetStringSafely(*doc, "updatedAt");

        obj->hoursMap = buildHoursMap(obj->hours);
        MLOG(10)<<"this is the TargetGroupDayPartTargetMap created from json : "<<obj->toJson();
        return obj;
}

std::unordered_map<int, bool> TargetGroupDayPartTargetMap::buildHoursMap(std::string hoursInWeekJson) {
        MLOG(10)<<" hoursInWeekJson  : "<< hoursInWeekJson;
        std::unordered_map<int, bool> hoursMap;
        std::vector<int> hours = convertHoursJsonToIntVector (hoursInWeekJson);
        MLOG(10)<< "hours : " <<JsonArrayUtil::convertListToJson(hours);

        for(int hour :  hours) {
                hoursMap.insert(std::make_pair(hour, true));
        }
        return hoursMap;
}

std::unordered_map<int, bool> TargetGroupDayPartTargetMap::addHoursToHourMap(std::vector<int> hours) {
        std::unordered_map<int, bool> hoursMap;

        for(int hour :  hours) {
                hoursMap.insert(std::make_pair(hour, true));
        }
        return hoursMap;
}

std::shared_ptr<TargetGroupDayPartTargetMap> TargetGroupDayPartTargetMap::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<TargetGroupDayPartTargetMap> obj = std::make_shared<TargetGroupDayPartTargetMap>();

        obj->id =  JsonUtil::GetIntSafely(value, "id");
        obj->targetGroupId = JsonUtil::GetIntSafely(value, "targetGroupId");
        obj->hours = JsonUtil::valueToString(value["hours"]);
        obj->createdAt = JsonUtil::GetStringSafely(value, "createdAt");
        obj->updatedAt = JsonUtil::GetStringSafely(value, "updatedAt");
        obj->hoursMap = buildHoursMap(obj->hours);
        return obj;
}

void TargetGroupDayPartTargetMap::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, value);

        this->hoursMap = buildHoursMap(hours);

        RapidJsonValueTypeNoRef hourValue(rapidjson::kObjectType);
        convertHoursMapToHoursJsonString(this->hoursMap, hourValue, doc);

        JsonUtil::addNameValuePairAsMemeberToValue (doc, "hours", hourValue, value);
        JsonUtil::addMemberToValue_FromPair (doc, "createdAt", createdAt, value);
        JsonUtil::addMemberToValue_FromPair (doc, "updatedAt", updatedAt, value);
        MLOG(10)<<"done builing daypart json ";
}

void TargetGroupDayPartTargetMap::convertHoursMapToHoursJsonString(
        std::unordered_map<int, bool> hoursMap, RapidJsonValueType hourValue, DocumentType* doc) {
        /**
         * this nastiness is because if I put this in a loop like

           for(...) {
           RapidJsonValueTypeNoRef value1(rapidjson::kArrayType);
           allDayValues.push_back(&value1);
           }
           it doesnt work , as the copy contrstuctor is private and I have to pass the &
           so all the elemnets points to one element!! TODO: fix this later
         */
        std::vector<RapidJsonValueTypePtr> allDayValues;
        RapidJsonValueTypeNoRef value1(rapidjson::kArrayType);
        allDayValues.push_back(&value1);

        RapidJsonValueTypeNoRef value2(rapidjson::kArrayType);
        allDayValues.push_back(&value2);

        RapidJsonValueTypeNoRef value3(rapidjson::kArrayType);
        allDayValues.push_back(&value3);

        RapidJsonValueTypeNoRef value4(rapidjson::kArrayType);
        allDayValues.push_back(&value4);

        RapidJsonValueTypeNoRef value5(rapidjson::kArrayType);
        allDayValues.push_back(&value5);

        RapidJsonValueTypeNoRef value6(rapidjson::kArrayType);
        allDayValues.push_back(&value6);

        RapidJsonValueTypeNoRef value7(rapidjson::kArrayType);
        allDayValues.push_back(&value7);


        assertAndThrow(allDayValues[0] != allDayValues[1]);
        assertAndThrow(allDayValues[1] != allDayValues[2]);


        for(int index=0; index<168; index++) {
                auto pairPtr = hoursMap.find(index); //find the hour entry from the map
                int dayNumber = index/24;
                int hourNumber = index - (dayNumber * 24);

                if (pairPtr != hoursMap.end()) {
                        if (pairPtr->second == true) {
                                MLOG(10) << "found index  in map : " <<index<<
                                        "adding dayNumber in arrays : " <<dayNumber<<
                                        "adding 1 for hourNumber in arrays : " <<hourNumber;
                                RapidJsonValueTypePtr value = allDayValues[dayNumber];
                                JsonArrayUtil::addMemberToArray(doc, StringUtil::toStr("1"), *value);
                        } else {
                                MLOG(10) << "ignoring hour since the value is false : hour : " << index;
                                RapidJsonValueTypePtr value = allDayValues[dayNumber];
                                JsonArrayUtil::addMemberToArray(doc, StringUtil::toStr("0"), *value);
                        }
                } else {
                        MLOG(10) << "ignoring hour since the value is false : hour : " << index;
                        RapidJsonValueTypePtr value = allDayValues[dayNumber];
                        JsonArrayUtil::addMemberToArray(doc, StringUtil::toStr("0"), *value);

                }


        }



        for(int i=0; i<7; i++) {
                RapidJsonValueTypePtr value = allDayValues[i];
                MLOG(10)<<"value "<<i<< " : " <<JsonUtil::valueToString(*value);
        }

        std::vector<RapidJsonValueTypePtr> allDayMemberValues;
        for(int i=0; i<7; i++) {
                RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
                allDayMemberValues.push_back(&value);
        }

        for(int i=0; i<7; i++) {
                if(!(*allDayValues.at(i)).Empty()) {
                        JsonUtil::addNameValuePairAsMemeberToValue
                                (doc,  StringUtil::toStr(i + 1), *allDayValues.at(i), hourValue);
                }
        }
}


std::string TargetGroupDayPartTargetMap::toJson() {
        auto doc = JsonUtil::createDcoumentAsObjectDoc();
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}
TargetGroupDayPartTargetMap::~TargetGroupDayPartTargetMap() {

}

/**
 *
   {"1":["1","1","1","1","1","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"],
   "2":["0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"],
   "3":["0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"],
   "4":["0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"],
   "5":["0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"],
   "6":["0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"],
   "7":["0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"]}

 * this function will turn this hourJson into a vector of [ 0 , 1 , 2 , .., 167]
 */
std::vector<int> TargetGroupDayPartTargetMap::convertHoursJsonToIntVector(std::string hoursJson) {
        MLOG(2) << "hoursJson read from db : " << hoursJson;

        auto document = parseJsonSafely(hoursJson);

        std::vector<int> listOfHours;
        for (int i = 1; i < 8; i++) {

                if (document->HasMember (StringUtil::toStr (i).c_str ())) {
                        RapidJsonValueType dayHours = (*document)[StringUtil::toStr (i).c_str ()];
                        assertAndThrow(dayHours.IsArray());
                        for (rapidjson::SizeType h = 0; h < dayHours.Size (); h++) {
                                std::string hourYesNo = dayHours[h].GetString ();
                                // MLOG(2) << "day : " << i <<" , hourYesNo : " << hourYesNo;
                                if(StringUtil::equalsIgnoreCase(hourYesNo, "1")) {
                                        int realHourInWeek = ( i - 1) * 24 + h;
                                        listOfHours.push_back (realHourInWeek);
                                } else {
                                        //hour is rejected
                                }
                        }
                } else {
                        MLOG(2) << "day  : " << i << " was not found in hour map";
                }
        }
        MLOG(2) << "listOfHours in week : " << JsonArrayUtil::convertListToJson (listOfHours);
        return listOfHours;
}
