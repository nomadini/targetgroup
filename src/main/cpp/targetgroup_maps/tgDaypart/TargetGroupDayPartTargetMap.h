#ifndef TargetGroupDayPartTargetMap_H
#define TargetGroupDayPartTargetMap_H




#include "JsonTypeDefs.h"

#include <vector>
#include <unordered_map>
class TargetGroupDayPartTargetMap;



class TargetGroupDayPartTargetMap {

private:

public:
int id;
int targetGroupId;
/**
 * this is the format of the hourJson in database  : 1 is monday and 7 is Sunday
   {"1":["1","1","1","1","1","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"],
   "2":["0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"],
   "3":["0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"],
   "4":["0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"],
   "5":["0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"],
   "6":["0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"],
   "7":["0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"]}
 */
std::string hours;
/**
 * this map has looks like this  {"0" : true, "2", true, .... , "167": true}
 * each number is the hour in the week
 */
std::unordered_map<int, bool> hoursMap;
std::string createdAt;
std::string updatedAt;

TargetGroupDayPartTargetMap();

static std::unordered_map<int, bool> buildHoursMap(std::string hours);

/**
 * this is the format of the hourJson in database {"0": ["0","1","2"], "1" :["0","22","23"] , "6" :["5","23"]}
 * this function will turn this hourJson into a vector of [ 0 , 1 , 2 , .., 167]
 */
static std::vector<int> convertHoursJsonToIntVector(std::string hoursJson);

/**
   you can use this function to add a certain hour to hours map
 */
static std::unordered_map<int, bool> addHoursToHourMap(std::vector<int> hours);

static void convertHoursMapToHoursJsonString(std::unordered_map<int, bool> hoursMap,
                                             RapidJsonValueType hourValue,
                                             DocumentType* doc);
std::string toString();
static std::string getEntityName();

static std::shared_ptr<TargetGroupDayPartTargetMap> fromJson(std::string jsonString);

std::string toJson();

virtual ~TargetGroupDayPartTargetMap();

static std::shared_ptr<TargetGroupDayPartTargetMap> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};
#endif
