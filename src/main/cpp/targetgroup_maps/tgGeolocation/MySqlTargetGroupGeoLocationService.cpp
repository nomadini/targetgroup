#include "MySqlTargetGroupGeoLocationService.h"
#include "MySqlDriver.h"

#include "JsonArrayUtil.h"
#include "CollectionUtil.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include "StringUtil.h"
#include "GUtil.h"

MySqlTargetGroupGeoLocationService::MySqlTargetGroupGeoLocationService(
        MySqlDriver* driver,
        MySqlGeoLocationService* mySqlGeoLocationService) {
        this->driver = driver;
        this->mySqlGeoLocationService = mySqlGeoLocationService;
}

std::vector<int> MySqlTargetGroupGeoLocationService::readAllGeoLocationIdsByTargetGroupId(int targetGroupId) {
        std::vector<int> allGeoLocationIds;
        std::string queryStr = "SELECT `geolocation_id` "
                               "FROM `targetgroup_geolocation_map` where targetgroup_id = __targetgroup_id__";



        queryStr = StringUtil::replaceString (queryStr, "__targetgroup_id__", StringUtil::toStr (targetGroupId));

        auto res = driver->executeQuery (queryStr);

        while (res->next ()) {
                allGeoLocationIds.push_back(res->getInt (1));
        }

        return allGeoLocationIds;
}

std::vector<std::shared_ptr<TargetGroupGeoLocationList>> MySqlTargetGroupGeoLocationService::readAllEntities() {
        return readAllAsObejcts();
}

std::vector<std::shared_ptr<TargetGroupGeoLocationList>> MySqlTargetGroupGeoLocationService::readAllAsObejcts() {
        std::vector<std::shared_ptr<TargetGroupGeoLocationList>> tgGeoLocationsMap;

        std::string query = "SELECT `ID`,"
                            "`targetgroup_id`,"
                            "`geolocation_id`,"
                            "`created_at`,"
                            "`updated_at` "
                            "FROM `targetgroup_geolocation_map`";



        auto res = driver->executeQuery (query);
        while (res->next ()) {
                std::shared_ptr<TargetGroupGeoLocationList> obj = std::make_shared<TargetGroupGeoLocationList>();
                obj->id = res->getInt (1);
                obj->targetGroupId = res->getInt (2);

                int geoLocaitonId = res->getInt (3);
                std::vector<int> allGeoLocationIds = readAllGeoLocationIdsByTargetGroupId(obj->targetGroupId);
                std::vector<std::shared_ptr<GeoLocation>> allGeoLocations = mySqlGeoLocationService->readAllWithIds(allGeoLocationIds);
                assertAndThrow(!allGeoLocations.empty());
                obj->geoLocationList = allGeoLocations;
                obj->dateCreated = MySqlDriver::getString( res, 4);
                obj->dateModified = MySqlDriver::getString( res, 5);

                tgGeoLocationsMap.push_back(obj);
        }



        return tgGeoLocationsMap;
}

std::unordered_map<int, std::vector<int> > MySqlTargetGroupGeoLocationService::readAll() {
        std::unordered_map<int, std::vector<int> > tgGeoLocationsMap;

        std::string query = "SELECT `ID`,"
                            "`targetgroup_id`,"
                            "`geolocation_id`,"
                            "`created_at`,"
                            "`updated_at` "
                            "FROM `targetgroup_geolocation_map`";



        auto res = driver->executeQuery (query);

        while (res->next ()) {
                int tgid = res->getInt (2);
                int crid = res->getInt (3);
                auto tgKeyInMap = tgGeoLocationsMap.find (tgid);
                if (tgKeyInMap != tgGeoLocationsMap.end ()) {
                        //it's in map
                        tgKeyInMap->second.push_back (crid);

                } else {
                        std::vector<int> geoLocationIdList;
                        geoLocationIdList.push_back (crid);
                        tgGeoLocationsMap.insert (std::make_pair (tgid, geoLocationIdList));
                }

        }


        return tgGeoLocationsMap;
}

void MySqlTargetGroupGeoLocationService::deleteAll() {
        driver->deleteAll("targetgroup_geolocation_map");

}


void MySqlTargetGroupGeoLocationService::insert(std::shared_ptr<TargetGroupGeoLocation> tg) {

        std::string queryStr =
                " INSERT INTO `targetgroup_geolocation_map`"
                " ("
                " `targetgroup_id`,"
                " `geolocation_id`,"
                " `created_at`,"
                " `updated_at`"
                ")"
                " VALUES"
                " ( "
                " '__targetgroup_id__',"
                " '__geolocation_id__',"
                " '__created_at__',"
                " '__updated_at__');";

        MLOG(3) << "inserting new TargetGroupGeoLocation in db : " << tg->toJson ();


        queryStr = StringUtil::replaceString (queryStr, "__targetgroup_id__",
                                              StringUtil::toStr (tg->targetGroupId));
        queryStr = StringUtil::replaceString (queryStr, "__geolocation_id__",
                                              StringUtil::toStr (tg->geoLocationId));

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        //MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);
        tg->id = driver->getLastInsertedId ();

}

MySqlTargetGroupGeoLocationService::~MySqlTargetGroupGeoLocationService() {
}
