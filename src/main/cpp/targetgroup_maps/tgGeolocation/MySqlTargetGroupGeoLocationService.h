#ifndef MySqlTargetGroupGeoLocationService_h
#define MySqlTargetGroupGeoLocationService_h


#include "TargetGroupGeoLocation.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "MySqlGeoLocationService.h"
#include "TargetGroupGeoLocationList.h"
#include "DataProvider.h"

class MySqlTargetGroupGeoLocationService;




class MySqlTargetGroupGeoLocationService : public DataProvider<TargetGroupGeoLocationList> {

public:
MySqlDriver* driver;
MySqlGeoLocationService* mySqlGeoLocationService;
MySqlTargetGroupGeoLocationService(MySqlDriver* driver,
                                   MySqlGeoLocationService* mySqlGeoLocationService);


void parseTheAttribute(std::shared_ptr<TargetGroupGeoLocation> obj, std::string attributesFromDB);

std::vector<int> readAllGeoLocationIdsByTargetGroupId(int targetGroupId);
std::vector<std::shared_ptr<TargetGroupGeoLocationList>> readAllAsObejcts();
std::unordered_map<int, std::vector<int> > readAll();

void insert(std::shared_ptr<TargetGroupGeoLocation> TargetGroupGeoLocation);

virtual void deleteAll();

std::vector<std::shared_ptr<TargetGroupGeoLocationList>> readAllEntities();

virtual ~MySqlTargetGroupGeoLocationService();
};

#endif
