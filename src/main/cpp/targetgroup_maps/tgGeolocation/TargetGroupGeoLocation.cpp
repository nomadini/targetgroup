#include "GUtil.h"
#include "TargetGroupGeoLocation.h"
#include "ConverterUtil.h"
#include "JsonUtil.h"
#include <string>
#include <memory>

TargetGroupGeoLocation::TargetGroupGeoLocation() {
        id = 0;
        targetGroupId = 0;
        geoLocationId = 0;
}


std::string TargetGroupGeoLocation::toString() {
        return this->toJson ();
}

std::string TargetGroupGeoLocation::getEntityName() {
        return "TargetGroupGeoLocation";
}

std::shared_ptr<TargetGroupGeoLocation> TargetGroupGeoLocation::fromJson(std::string jsonString) {
        std::shared_ptr<TargetGroupGeoLocation> tgMap (new TargetGroupGeoLocation ());

        auto document = parseJsonSafely(jsonString);


        if (document->HasMember ("id")) {
                tgMap->id = (*document)["id"].GetInt ();
        }

        if (document->HasMember ("targetGroupId")) {
                tgMap->targetGroupId = JsonUtil::GetIntSafely (*document, "targetGroupId");
        }
        if (document->HasMember ("geoLocationId")) {
                tgMap->geoLocationId = (*document)["geoLocationId"].GetInt ();
        }
        if (document->HasMember ("dateCreated")) {
                tgMap->dateCreated = JsonUtil::GetStringSafely (*document, "dateCreated");
        }
        if (document->HasMember ("dateModified")) {
                tgMap->dateModified = JsonUtil::GetStringSafely (*document, "dateModified");
        }

        //MLOG(3)<<"this is the TargetGroupGeoLocation created from json : "<<tgMap->toJson();
        return tgMap;
}

void TargetGroupGeoLocation::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "geoLocationId", geoLocationId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateCreated", dateCreated, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateModified", dateModified, value);
}
std::shared_ptr<TargetGroupGeoLocation> TargetGroupGeoLocation::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<TargetGroupGeoLocation> geoTargetGroupGeoLocationList = std::make_shared<TargetGroupGeoLocation>();
        geoTargetGroupGeoLocationList->id = JsonUtil::GetIntSafely(value, "id");
        geoTargetGroupGeoLocationList->targetGroupId  =JsonUtil::GetIntSafely(value,"targetGroupId");
        geoTargetGroupGeoLocationList->geoLocationId = JsonUtil::GetIntSafely(value,"geoLocationId");
        geoTargetGroupGeoLocationList->dateCreated = JsonUtil::GetStringSafely(value,"dateCreated");
        geoTargetGroupGeoLocationList->dateModified = JsonUtil::GetStringSafely(value,"dateModified");
        return geoTargetGroupGeoLocationList;
}

std::string TargetGroupGeoLocation::toJson() {
        auto doc = JsonUtil::createDcoumentAsObjectDoc();
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}
TargetGroupGeoLocation::~TargetGroupGeoLocation() {

}
