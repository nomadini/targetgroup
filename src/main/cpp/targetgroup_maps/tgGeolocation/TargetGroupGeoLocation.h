#ifndef TargetGroupGeoLocation_H
#define TargetGroupGeoLocation_H



#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "JsonTypeDefs.h"

#include "GeoLocation.h"
class TargetGroupGeoLocation;




class TargetGroupGeoLocation {

private:

public:
int id;
int targetGroupId;
int geoLocationId;
std::shared_ptr<GeoLocation> geoLocation;
std::string dateCreated;
std::string dateModified;

TargetGroupGeoLocation();

std::string toString();

static std::shared_ptr<TargetGroupGeoLocation> fromJson(std::string jsonString);

std::string toJson();

virtual ~TargetGroupGeoLocation();

static std::string getEntityName();
static std::shared_ptr<TargetGroupGeoLocation> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};


#endif
