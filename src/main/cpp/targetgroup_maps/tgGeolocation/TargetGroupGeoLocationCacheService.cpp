#include "TargetGroupGeoLocationCacheService.h"
#include <boost/foreach.hpp>
#include "TargetGroupGeoLocationList.h"
#include "JsonArrayUtil.h"
#include "MySqlTargetGroupGeoLocationService.h"
TargetGroupGeoLocationCacheService::TargetGroupGeoLocationCacheService(
        MySqlTargetGroupGeoLocationService* mySqlTargetGroupGeoLocationService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,std::string appName) :
        CacheService<TargetGroupGeoLocationList>
                (httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName) {

        this->entityToModuleStateStats = entityToModuleStateStats;

        this->mapOfAllTargetGroupToGeoLocationList =
                std::make_shared<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<GeoLocation> > >();
}

std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<GeoLocation> > > TargetGroupGeoLocationCacheService::getMapOfAllTargetGroupToGeoLocationList() {
        return this->mapOfAllTargetGroupToGeoLocationList;
}

std::string TargetGroupGeoLocationCacheService::getEntityName() {
        return "TargetGroupGeoLocationList";
}

void TargetGroupGeoLocationCacheService::clearOtherCaches() {
        mapOfAllTargetGroupToGeoLocationList->clear ();
}

void TargetGroupGeoLocationCacheService::populateOtherMapsAndLists(
        std::vector<std::shared_ptr<TargetGroupGeoLocationList> > tgGeoLocationsMap) {

        for(auto entity :  tgGeoLocationsMap) {
                MLOG(10) << "loading TargetGroupGeoLocationList : "<< entity->toJson();
                auto obj = std::make_shared<ObjectVectorHolder<GeoLocation> >();
                for (auto o : entity->geoLocationList) {
                        obj->values->push_back(o);
                }
                mapOfAllTargetGroupToGeoLocationList->put(entity->targetGroupId, obj);
        }

        MLOG(10) << mapOfAllTargetGroupToGeoLocationList->size () << " target group geo locations mapping were loaded....";

}

TargetGroupGeoLocationCacheService::~TargetGroupGeoLocationCacheService() {

}
