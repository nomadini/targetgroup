#ifndef TargetGroupGeoLocationCacheService_h
#define TargetGroupGeoLocationCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>

#include "TargetGroupGeoLocationList.h"
class TargetGroupCacheService;
#include "HttpUtilService.h"
#include "GeoLocation.h"
#include "CacheService.h"
#include "ObjectVectorHolder.h"
#include "EntityProviderService.h"
#include "ConcurrentHashMap.h"
class MySqlTargetGroupGeoLocationService;
class EntityToModuleStateStats;

class TargetGroupGeoLocationCacheService;



class TargetGroupGeoLocationCacheService : public CacheService<TargetGroupGeoLocationList> {

private:


public:
EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<GeoLocation> > > mapOfAllTargetGroupToGeoLocationList;

TargetGroupGeoLocationCacheService(
        MySqlTargetGroupGeoLocationService* mySqlTargetGroupGeoLocationService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string appName);

std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<GeoLocation> > > getMapOfAllTargetGroupToGeoLocationList();

void clearOtherCaches();

void populateOtherMapsAndLists(std::vector<std::shared_ptr<TargetGroupGeoLocationList> > allEntities);

std::string getEntityName();

virtual ~TargetGroupGeoLocationCacheService();
};


#endif
