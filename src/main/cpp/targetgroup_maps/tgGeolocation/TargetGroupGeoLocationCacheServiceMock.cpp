#include "TargetGroupGeoLocationCacheServiceMock.h"
#include <boost/foreach.hpp>
#include "JsonArrayUtil.h"

TargetGroupGeoLocationCacheServiceMock::TargetGroupGeoLocationCacheServiceMock(
        MySqlTargetGroupGeoLocationService* mySqlTargetGroupGeoLocationService,
        MySqlGeoLocationService* mySqlGeoLocationService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string appName) :
        TargetGroupGeoLocationCacheService(mySqlTargetGroupGeoLocationService,
                                           httpUtilService,
                                           dataMasterUrl,
                                           entityToModuleStateStats,
                                           appName) {


}
