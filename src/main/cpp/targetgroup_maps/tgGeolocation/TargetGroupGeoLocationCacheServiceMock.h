#ifndef TargetGroupGeoLocationCacheServiceMock_h
#define TargetGroupGeoLocationCacheServiceMock_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>

#include "GeoLocation.h"
#include "MySqlGeoLocationService.h"
#include "TargetGroupGeoLocationCacheService.h"
class TargetGroupGeoLocationCacheService;
#include "gmock/gmock.h"
class EntityToModuleStateStats;
class TargetGroupGeoLocationCacheServiceMock;



class TargetGroupGeoLocationCacheServiceMock : public TargetGroupGeoLocationCacheService {

public:
TargetGroupGeoLocationCacheServiceMock(
        MySqlTargetGroupGeoLocationService* mySqlTargetGroupGeoLocationService,
        MySqlGeoLocationService* mySqlGeoLocationService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string appName);
MOCK_METHOD1(processReadAll, std::string(std::string));
};


#endif
