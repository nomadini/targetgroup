#include "GUtil.h"
#include "TargetGroupGeoLocationList.h"
#include "JsonArrayUtil.h"
#include "JsonUtil.h"
#include <string>
#include <memory>
#include <boost/foreach.hpp>

TargetGroupGeoLocationList::TargetGroupGeoLocationList() {
        id = 0;
        targetGroupId = 0;
}


std::string TargetGroupGeoLocationList::toString() {
        return this->toJson ();
}

std::shared_ptr<TargetGroupGeoLocationList> TargetGroupGeoLocationList::fromJson(std::string jsonString) {
        std::shared_ptr<TargetGroupGeoLocationList> tgMap (new TargetGroupGeoLocationList ());

        auto document = parseJsonSafely(jsonString);


        if (document->HasMember ("id")) {
                tgMap->id = (*document)["id"].GetInt ();
        }

        if (document->HasMember ("targetGroupId")) {
                tgMap->targetGroupId = JsonUtil::GetIntSafely (*document, "targetGroupId");
        }
        if (document->HasMember ("geoLocationList")) {
                tgMap->geoLocationList = parseListOfObjects(*document, "geoLocationList");
        }
        if (document->HasMember ("dateCreated")) {
                tgMap->dateCreated = JsonUtil::GetStringSafely (*document, "dateCreated");
        }
        if (document->HasMember ("dateModified")) {
                tgMap->dateModified = JsonUtil::GetStringSafely (*document, "dateModified");
        }

        MLOG(3)<<"this is the TargetGroupGeoLocationList created from json : "<<tgMap->toJson();
        return tgMap;
}

void TargetGroupGeoLocationList::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, value);

        RapidJsonValueTypeNoRef array(rapidjson::kArrayType);
        for(std::shared_ptr<GeoLocation> geoLocation :  geoLocationList) {
                RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
                geoLocation->addPropertiesToJsonValue(value, doc);
                JsonArrayUtil::addMemberToArray(doc, array, value);
        }
        JsonArrayUtil::addArrayValueAsMemberToValue(doc, "geoLocationList", array, value);

        JsonUtil::addMemberToValue_FromPair (doc, "dateCreated", dateCreated, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateModified", dateModified, value);
}

std::string TargetGroupGeoLocationList::getEntityName() {
        return "TargetGroupGeoLocationList";
}

std::shared_ptr<TargetGroupGeoLocationList> TargetGroupGeoLocationList::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<TargetGroupGeoLocationList> geoTargetGroupGeoLocationListList = std::make_shared<TargetGroupGeoLocationList>();
        geoTargetGroupGeoLocationListList->id = JsonUtil::GetIntSafely(value, "id");
        geoTargetGroupGeoLocationListList->targetGroupId  =JsonUtil::GetIntSafely(value,"targetGroupId");

        geoTargetGroupGeoLocationListList->geoLocationList = parseListOfObjects(value, "geoLocationList");

        geoTargetGroupGeoLocationListList->dateCreated = JsonUtil::GetStringSafely(value,"dateCreated");
        geoTargetGroupGeoLocationListList->dateModified = JsonUtil::GetStringSafely(value,"dateModified");
        return geoTargetGroupGeoLocationListList;
}

std::vector<std::shared_ptr<GeoLocation> >  TargetGroupGeoLocationList::parseListOfObjects(RapidJsonValueType value, std::string nameOfElement) {
        std::vector<RapidJsonValueTypePtr> allData;
        std::vector<std::shared_ptr<GeoLocation> > allGeoLocations;
        JsonArrayUtil::getArrayFromValueMemeber( value, nameOfElement, allData);

        for(RapidJsonValueTypePtr value :  allData) {
                auto geoLocation = GeoLocation::fromJsonValue(*value);
                allGeoLocations.push_back(geoLocation);
        }

        return allGeoLocations;
}

std::string TargetGroupGeoLocationList::toJson() {
        auto doc = JsonUtil::createDcoumentAsObjectDoc();
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}
TargetGroupGeoLocationList::~TargetGroupGeoLocationList() {

}
