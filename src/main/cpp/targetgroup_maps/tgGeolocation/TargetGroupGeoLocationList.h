#ifndef TargetGroupGeoLocationList_H
#define TargetGroupGeoLocationList_H



#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "JsonTypeDefs.h"

#include "GeoLocation.h"

class TargetGroupGeoLocationList;



class TargetGroupGeoLocationList {

private:

public:
int id;
int targetGroupId;
std::vector<std::shared_ptr<GeoLocation> > geoLocationList;
std::string dateCreated;
std::string dateModified;

TargetGroupGeoLocationList();

std::string toString();

static std::shared_ptr<TargetGroupGeoLocationList> fromJson(std::string jsonString);

std::string toJson();

virtual ~TargetGroupGeoLocationList();
static std::vector<std::shared_ptr<GeoLocation> >  parseListOfObjects(RapidJsonValueType value, std::string nameOfElement);
static std::shared_ptr<TargetGroupGeoLocationList> fromJsonValue(RapidJsonValueType value);

static std::string getEntityName();

void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};


#endif
