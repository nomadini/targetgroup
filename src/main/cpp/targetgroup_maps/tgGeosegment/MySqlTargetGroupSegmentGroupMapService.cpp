#include "MySqlTargetGroupSegmentGroupMapService.h"
#include "MySqlDriver.h"

#include "JsonArrayUtil.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "CollectionUtil.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include "MySqlDriver.h"
MySqlTargetGroupSegmentGroupMapService::MySqlTargetGroupSegmentGroupMapService(MySqlDriver* driver) {
        this->driver = driver;
}

std::vector<std::shared_ptr<TargetGroupSegmentGroupMap> > MySqlTargetGroupSegmentGroupMapService::readAllByTgId(int targetGroupId) {
        std::vector<std::shared_ptr<TargetGroupSegmentGroupMap> > all;
        std::string queryStr =
                "SELECT `targetgroup_segment_group_map`.`ID`,"
                "`targetgroup_segment_group_map`.`targetgroup_id`,"
                "`targetgroup_segment_group_map`.`segment_group_id`"
                "FROM `targetgroup_segment_group_map` where targetgroup_id = __targetgroup_id__ ";


        queryStr = StringUtil::replaceString (queryStr, "__targetgroup_id__",
                                              StringUtil::toStr (targetGroupId));
        auto res = driver->executeQuery (queryStr);

        while (res->next ()) {
                std::shared_ptr<TargetGroupSegmentGroupMap> one = std::make_shared<TargetGroupSegmentGroupMap> ();
                MLOG(3) << "targetgroup_segment_group_map obj loaded : id  " << res->getInt (1);
                one->targetGroupId = res->getInt ("targetgroup_id");
                one->segmentGroupId = res->getInt ("segment_group_id");
                all.push_back (one);
        }

        return all;

}

std::vector<std::shared_ptr<TargetGroupSegmentGroupMap> > MySqlTargetGroupSegmentGroupMapService::readAll() {

        std::vector<std::shared_ptr<TargetGroupSegmentGroupMap> > all;
        std::string query = "SELECT `targetgroup_segment_group_map`.`ID`,"
                            "`targetgroup_segment_group_map`.`targetgroup_id`,"
                            "`targetgroup_segment_group_map`.`segment_group_id`"
                            "FROM `targetgroup_segment_group_map`";



        auto res = driver->executeQuery (query);

        while (res->next ()) {
                std::shared_ptr<TargetGroupSegmentGroupMap> one = std::make_shared<TargetGroupSegmentGroupMap> ();
                one->id = res->getInt ("ID");
                one->targetGroupId = res->getInt ("targetgroup_id");
                one->segmentGroupId = res->getInt ("segment_group_id");
                all.push_back (one);
        }

        return all;
}

void MySqlTargetGroupSegmentGroupMapService::deleteAll() {
        driver->deleteAll("targetgroup_segment_group_map");
}


void MySqlTargetGroupSegmentGroupMapService::insert(std::shared_ptr<TargetGroupSegmentGroupMap> tg) {
        std::string queryStr =
                " INSERT INTO `targetgroup_segment_group_map`"
                " ("
                " `targetgroup_id`,"
                " `segment_group_id`,"
                " `created_at`,"
                " `updated_at`"
                ")"
                " VALUES"
                " ( "
                " '__targetgroup_id__',"
                " '__segment_group_id__',"
                " '__created_at__',"
                " '__updated_at__')";

        MLOG(3) << "inserting new TargetGroupSegment in db : " << tg->toJson ();


        queryStr = StringUtil::replaceString (queryStr, "__targetgroup_id__",
                                              StringUtil::toStr (tg->targetGroupId));
        queryStr = StringUtil::replaceString (queryStr, "__segment_group_id__",
                                              StringUtil::toStr (tg->segmentGroupId));

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        //MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);
        tg->id = driver->getLastInsertedId ();


}

std::vector<std::shared_ptr<TargetGroupSegmentGroupMap> > MySqlTargetGroupSegmentGroupMapService::readAllEntities() {
        return this->readAll();
}

MySqlTargetGroupSegmentGroupMapService::~MySqlTargetGroupSegmentGroupMapService() {
}

std::shared_ptr<MySqlTargetGroupSegmentGroupMapService> MySqlTargetGroupSegmentGroupMapService::getInstance(
        gicapods::ConfigService* configService) {

        static std::shared_ptr<MySqlTargetGroupSegmentGroupMapService> ins =
                std::make_shared<MySqlTargetGroupSegmentGroupMapService>(
                        MySqlDriver::getInstance(configService).get());
        return ins;
}
