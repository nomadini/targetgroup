#ifndef MySqlTargetGroupSegmentGroupMapService_h
#define MySqlTargetGroupSegmentGroupMapService_h


#include "TargetGroupSegmentGroupMap.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include <tbb/concurrent_hash_map.h>
#include "Segment.h"
#include "DataProvider.h"

class MySqlTargetGroupSegmentGroupMapService;



class MySqlTargetGroupSegmentGroupMapService : public DataProvider<TargetGroupSegmentGroupMap> {

public:
MySqlDriver* driver;

MySqlTargetGroupSegmentGroupMapService(MySqlDriver* driver);


virtual ~MySqlTargetGroupSegmentGroupMapService();

std::vector<std::shared_ptr<TargetGroupSegmentGroupMap> > readAll();

std::vector<std::shared_ptr<TargetGroupSegmentGroupMap> > readAllByTgId(int targetGroupId);

void insert(std::shared_ptr<TargetGroupSegmentGroupMap> targetGroupSegmentGroupMap);

virtual void deleteAll();

std::vector<std::shared_ptr<TargetGroupSegmentGroupMap> > readAllEntities();
static std::shared_ptr<MySqlTargetGroupSegmentGroupMapService> getInstance(gicapods::ConfigService* configService);
};

#endif
