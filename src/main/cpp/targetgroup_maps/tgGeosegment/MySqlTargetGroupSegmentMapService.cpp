#include "MySqlTargetGroupSegmentMapService.h"
#include "MySqlDriver.h"

#include "JsonArrayUtil.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "CollectionUtil.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include "MySqlDriver.h"
MySqlTargetGroupSegmentMapService::MySqlTargetGroupSegmentMapService(MySqlDriver* driver) {
        this->driver = driver;
}

std::vector<std::shared_ptr<TargetGroupSegmentMap> > MySqlTargetGroupSegmentMapService::readAllByTgId(int targetGroupId) {
        std::vector<std::shared_ptr<TargetGroupSegmentMap> > all;
        std::string queryStr =
                "SELECT `targetgroup_segment_map`.`ID`,"
                "`targetgroup_segment_map`.`targetgroup_id`,"
                "`targetgroup_segment_map`.`segment_id`"
                "FROM `targetgroup_segment_map` where targetgroup_id = __targetgroup_id__ ";


        queryStr = StringUtil::replaceString (queryStr, "__targetgroup_id__",
                                              StringUtil::toStr (targetGroupId));
        auto res = driver->executeQuery (queryStr);

        while (res->next ()) {
                std::shared_ptr<TargetGroupSegmentMap> one = std::make_shared<TargetGroupSegmentMap> ();
                MLOG(3) << "targetgroup_segment_map obj loaded : id  " << res->getInt (1);
                one->targetGroupId = res->getInt (2);
                one->segmentId = res->getInt (3);
                all.push_back (one);
        }

        return all;

}

std::vector<std::shared_ptr<TargetGroupSegmentMap> > MySqlTargetGroupSegmentMapService::readAll() {

        std::vector<std::shared_ptr<TargetGroupSegmentMap> > all;
        std::string query = "SELECT `targetgroup_segment_map`.`ID`,"
                            "`targetgroup_segment_map`.`targetgroup_id`,"
                            "`targetgroup_segment_map`.`segment_id`"
                            "FROM `targetgroup_segment_map`";



        auto res = driver->executeQuery (query);

        while (res->next ()) {
                std::shared_ptr<TargetGroupSegmentMap> one = std::make_shared<TargetGroupSegmentMap> ();
                one->id = res->getInt ("ID");
                one->targetGroupId = res->getInt ("targetgroup_id");
                one->segmentId = res->getInt ("segment_id");
                all.push_back (one);
        }

        return all;
}

void MySqlTargetGroupSegmentMapService::deleteAll() {
        driver->deleteAll("targetgroup_segment_map");
}


void MySqlTargetGroupSegmentMapService::insert(std::shared_ptr<TargetGroupSegmentMap> tg) {
        std::string queryStr =
                " INSERT INTO `targetgroup_segment_map`"
                " ("
                " `targetgroup_id`,"
                " `segment_id`,"
                " `created_at`,"
                " `updated_at`"
                ")"
                " VALUES"
                " ( "
                " '__targetgroup_id__',"
                " '__segment_id__',"
                " '__created_at__',"
                " '__updated_at__')";

        MLOG(3) << "inserting new TargetGroupSegment in db : " << tg->toJson ();


        queryStr = StringUtil::replaceString (queryStr, "__targetgroup_id__",
                                              StringUtil::toStr (tg->targetGroupId));
        queryStr = StringUtil::replaceString (queryStr, "__segment_id__",
                                              StringUtil::toStr (tg->segmentId));

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        //MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);
        tg->id = driver->getLastInsertedId ();


}

std::vector<std::shared_ptr<TargetGroupSegmentMap> > MySqlTargetGroupSegmentMapService::readAllEntities() {
        return this->readAll();
}

MySqlTargetGroupSegmentMapService::~MySqlTargetGroupSegmentMapService() {
}
