#ifndef MySqlTargetGroupSegmentMapService_h
#define MySqlTargetGroupSegmentMapService_h


#include "TargetGroupSegmentMap.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include <tbb/concurrent_hash_map.h>
#include "Segment.h"
#include "DataProvider.h"

class MySqlTargetGroupSegmentMapService;



class MySqlTargetGroupSegmentMapService : public DataProvider<TargetGroupSegmentMap> {

public:
MySqlDriver* driver;

MySqlTargetGroupSegmentMapService(MySqlDriver* driver);


virtual ~MySqlTargetGroupSegmentMapService();

std::vector<std::shared_ptr<TargetGroupSegmentMap> > readAll();

std::vector<std::shared_ptr<TargetGroupSegmentMap> > readAllByTgId(int targetGroupId);

void insert(std::shared_ptr<TargetGroupSegmentMap> TargetGroupSegmentMap);

virtual void deleteAll();

std::vector<std::shared_ptr<TargetGroupSegmentMap> > readAllEntities();

};

#endif
