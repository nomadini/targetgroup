
#include "StringUtil.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "TargetGroupGeoSegmentCacheService.h"
#include "MySqlTargetGroupService.h"
#include "JsonArrayUtil.h"

TargetGroupGeoSegmentCacheService::TargetGroupGeoSegmentCacheService(MySqlTargetGroupGeoSegmentListMapService* mySqlTargetGroupGeoSegmentListMapService,
                                                                     HttpUtilService* httpUtilService,
                                                                     std::string dataMasterUrl,
                                                                     EntityToModuleStateStats* entityToModuleStateStats,std::string appName) {

        this->mySqlTargetGroupGeoSegmentListMapService = mySqlTargetGroupGeoSegmentListMapService;
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->allTgGeoSegmentsListMap = std::make_shared<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<GeoSegmentList> > >();
        this->allTargetGroupGeoSegmentListIdMap = std::make_shared<gicapods::ConcurrentHashMap<int, IntWrapper> >();
}

std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<GeoSegmentList> > >
TargetGroupGeoSegmentCacheService::getAllTgGeoSegmentsListMap() {
        return allTgGeoSegmentsListMap;
}

void TargetGroupGeoSegmentCacheService::clearOtherCaches() {
        getAllTgGeoSegmentsListMap()->clear ();
}


void TargetGroupGeoSegmentCacheService::populateOtherMapsAndLists(std::vector<std::shared_ptr<GeoSegmentList> > allObjects) {

}

TargetGroupGeoSegmentCacheService::~TargetGroupGeoSegmentCacheService() {

}
