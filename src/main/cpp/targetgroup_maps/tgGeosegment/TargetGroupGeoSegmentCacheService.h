#ifndef TargetGroupGeoSegmentCacheService_h
#define TargetGroupGeoSegmentCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "GeoSegmentList.h"
#include "MySqlGeoSegmentListService.h"
#include "MySqlTargetGroupGeoSegmentListMapService.h"
#include "ObjectVectorHolder.h"
#include "IntWrapper.h"
#include "HttpUtilService.h"
#include "ConcurrentHashMap.h"
class EntityToModuleStateStats;

class TargetGroupGeoSegmentCacheService;



class TargetGroupGeoSegmentCacheService {

private:

public:
MySqlTargetGroupGeoSegmentListMapService* mySqlTargetGroupGeoSegmentListMapService;

std::shared_ptr<gicapods::ConcurrentHashMap<int, IntWrapper> > allTargetGroupGeoSegmentListIdMap;

std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<GeoSegmentList> > > allTgGeoSegmentsListMap;

MySqlGeoSegmentService* mySqlGeoSegmentService;
EntityToModuleStateStats* entityToModuleStateStats;

TargetGroupGeoSegmentCacheService
        (MySqlTargetGroupGeoSegmentListMapService* mySqlTargetGroupGeoSegmentListMapService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,std::string appName);


std::shared_ptr<gicapods::ConcurrentHashMap<int, IntWrapper> > getAllTargetGroupGeoSegmentListIdMap();

std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<GeoSegmentList> > > getAllTgGeoSegmentsListMap();

void addTgGeoSegmentListToCache(int targetGroupId, std::shared_ptr<GeoSegmentList> geoSegmentList);

void clearOtherCaches();

void populateOtherMapsAndLists(std::vector<std::shared_ptr<GeoSegmentList> > allEntities);

virtual ~TargetGroupGeoSegmentCacheService();
};


#endif
