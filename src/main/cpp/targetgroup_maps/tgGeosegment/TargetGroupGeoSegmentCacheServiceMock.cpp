#include "StringUtil.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "TargetGroupGeoSegmentCacheServiceMock.h"
#include "MySqlTargetGroupService.h"
#include "MySqlGeoSegmentService.h"
#include "JsonArrayUtil.h"

TargetGroupGeoSegmentCacheServiceMock::
TargetGroupGeoSegmentCacheServiceMock(
        MySqlTargetGroupGeoSegmentListMapService* mySqlTargetGroupGeoSegmentListMapService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string appName) :
        TargetGroupGeoSegmentCacheService(
                mySqlTargetGroupGeoSegmentListMapService,
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName) {
}
