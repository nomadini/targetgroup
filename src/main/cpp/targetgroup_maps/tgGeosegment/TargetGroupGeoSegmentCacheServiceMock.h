#ifndef TargetGroupGeoSegmentCacheServiceMock_h
#define TargetGroupGeoSegmentCacheServiceMock_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "GeoSegmentList.h"
#include "MySqlGeoSegmentListService.h"
#include "TargetGroupGeoSegmentCacheService.h"
#include "MySqlTargetGroupGeoSegmentListMapService.h"
class TargetGroupGeoSegmentCacheService;
#include "gmock/gmock.h"
class EntityToModuleStateStats;

class TargetGroupGeoSegmentCacheServiceMock;



class TargetGroupGeoSegmentCacheServiceMock : public TargetGroupGeoSegmentCacheService {

public:
TargetGroupGeoSegmentCacheServiceMock
        (MySqlTargetGroupGeoSegmentListMapService* mySqlTargetGroupGeoSegmentListMapService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string appName);

MOCK_METHOD1(processReadAll, std::string(std::string));
};


#endif
