#include "TargetGroup.h"
#include "StringUtil.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "TargetGroupSegmentCacheService.h"
#include "SegmentCacheService.h"
#include "TargetGroupCacheService.h"
#include "MySqlTargetGroupService.h"
#include "MySqlSegmentService.h"
#include "MySqlTargetGroupSegmentMapService.h"
#include "JsonArrayUtil.h"
#include "TargetGroup.h"
#include "ConcurrentHashMap.h"
#include "ObjectVectorHolder.h"

TargetGroupSegmentCacheService::TargetGroupSegmentCacheService(MySqlTargetGroupSegmentMapService* mySqlTargetGroupSegmentMapService,
                                                               SegmentCacheService* segmentCacheService,
                                                               TargetGroupCacheService* targetGroupCacheService,
                                                               HttpUtilService* httpUtilService,
                                                               std::string dataMasterUrl,
                                                               EntityToModuleStateStats* entityToModuleStateStats,
                                                               std::string appName)

        : CacheService<TargetGroupSegmentMap>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName){

        this->mySqlTargetGroupSegmentMapService = mySqlTargetGroupSegmentMapService;
        this->targetGroupCacheService = targetGroupCacheService;
        this->segmentCacheService = segmentCacheService;

        allTargetGroupSegmentListMap = std::make_shared<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<Segment> > > ();
        allSegmentTargetGroupListMap = std::make_shared<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<TargetGroup> > > ();

}

std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<Segment> > >
TargetGroupSegmentCacheService::getAllTargetGroupSegmentListMap() {
        return allTargetGroupSegmentListMap;
}

void TargetGroupSegmentCacheService::populateOtherMapsAndLists (std::vector<std::shared_ptr<TargetGroupSegmentMap> > allTargetGroupSegmentMappings) {

        MLOG(10) << "read "<<allTargetGroupSegmentMappings.size() <<"  allTargetGroupSegmentMappings entries";

        if (allTargetGroupSegmentMappings.empty()) {
                return;
        }

        /*
           populating allTargetGroupSegmentListMap
         */
        BOOST_FOREACH(std::shared_ptr<TargetGroupSegmentMap>
                      tgSegmentPair, allTargetGroupSegmentMappings) {
                std::shared_ptr<Segment> segment = segmentCacheService->findByEntityId(tgSegmentPair->segmentId);

                auto allSegments = this->allTargetGroupSegmentListMap->getOptional(tgSegmentPair->targetGroupId);
                if (allSegments == nullptr) {
                        allSegments =
                                std::make_shared<ObjectVectorHolder<Segment> > ();
                }
                allSegments->values->push_back(segment);
                this->allTargetGroupSegmentListMap->put(tgSegmentPair->targetGroupId, allSegments);
        }

        /*
           populating allSegmentTargetGroupListMap
         */
        BOOST_FOREACH(std::shared_ptr<TargetGroupSegmentMap>
                      tgSegmentPair, allTargetGroupSegmentMappings) {
                std::shared_ptr<TargetGroup> tg = this->targetGroupCacheService->findByEntityId(tgSegmentPair->targetGroupId);
                auto allTargetGroups = this->allSegmentTargetGroupListMap->getOptional(tgSegmentPair->segmentId);
                if (allTargetGroups == nullptr) {
                        allTargetGroups =
                                std::make_shared<ObjectVectorHolder<TargetGroup> > ();
                }
                allTargetGroups->values->push_back(tg);
                this->allSegmentTargetGroupListMap->put(tgSegmentPair->segmentId, allTargetGroups);
        }

        if (allSegmentTargetGroupListMap->empty() ||
            allTargetGroupSegmentListMap->empty()) {
                throwEx("one of allTargetGroupSegmentMap is empty");
        }
}

std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<TargetGroup> > > TargetGroupSegmentCacheService::getAllSegmentTargetGroupListMap() {
        return allSegmentTargetGroupListMap;
}


void TargetGroupSegmentCacheService::clearOtherCaches() {
        MLOG(10) << "clearing TargetGroupSegmentCacheService ";
        this->allSegmentTargetGroupListMap->clear();
        this->allTargetGroupSegmentListMap->clear();
}
