#ifndef TargetGroupSegmentCacheService_h
#define TargetGroupSegmentCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "TargetGroupSegmentMap.h"
#include "Segment.h"
#include "MySqlTargetGroupSegmentMapService.h"
#include "MySqlSegmentService.h"
#include "MySqlTargetGroupSegmentMapService.h"
#include "HttpUtilService.h"
#include "CacheService.h"
#include "EntityProviderService.h"
#include "ObjectVectorHolder.h"
class TargetGroupCacheService;

class Segment;
class SegmentCacheService;
class TargetGroup;

class EntityToModuleStateStats;

class TargetGroupSegmentCacheService;



class TargetGroupSegmentCacheService : public CacheService<TargetGroupSegmentMap> {

private:

std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<Segment> > > allTargetGroupSegmentListMap;
std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<TargetGroup> > > allSegmentTargetGroupListMap;

public:

MySqlTargetGroupSegmentMapService* mySqlTargetGroupSegmentMapService;
SegmentCacheService* segmentCacheService;
TargetGroupCacheService* targetGroupCacheService;

TargetGroupSegmentCacheService(MySqlTargetGroupSegmentMapService* mySqlTargetGroupSegmentMapService,
                               SegmentCacheService* segmentCacheService,
                               TargetGroupCacheService* targetGroupCacheService,
                               HttpUtilService* httpUtilService,
                               std::string dataMasterUrl,
                               EntityToModuleStateStats* entityToModuleStateStats,
                               std::string appName);

std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<Segment> > > getAllTargetGroupSegmentListMap();

std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<TargetGroup> > > getAllSegmentTargetGroupListMap();

void populateOtherMapsAndLists (std::vector<std::shared_ptr<TargetGroupSegmentMap> > allTargetGroupSegmentMaps);

void clearOtherCaches();

};


#endif
