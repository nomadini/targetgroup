#include "TargetGroup.h"
#include "StringUtil.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "TargetGroupSegmentGroupCacheService.h"
#include "SegmentCacheService.h"
#include "TargetGroupCacheService.h"
#include "MySqlTargetGroupService.h"
#include "MySqlSegmentService.h"
#include "JsonArrayUtil.h"
#include "TargetGroup.h"
#include "ConfigService.h"
#include "ConcurrentHashMap.h"
#include "ObjectVectorHolder.h"

TargetGroupSegmentGroupCacheService::TargetGroupSegmentGroupCacheService(SegmentCacheService* segmentCacheService,
                                                                         TargetGroupCacheService* targetGroupCacheService,
                                                                         HttpUtilService* httpUtilService,
                                                                         std::string dataMasterUrl,
                                                                         EntityToModuleStateStats* entityToModuleStateStats,
                                                                         std::string appName)

        : CacheService<TargetGroupSegmentGroupMap>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName){

        this->targetGroupCacheService = targetGroupCacheService;
        this->segmentCacheService = segmentCacheService;

        allTargetGroupSegmentListMap = std::make_shared<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<Segment> > > ();
        allSegmentTargetGroupListMap = std::make_shared<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<TargetGroup> > > ();

}

std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<Segment> > >
TargetGroupSegmentGroupCacheService::getAllTargetGroupSegmentListMap() {
        return allTargetGroupSegmentListMap;
}

void TargetGroupSegmentGroupCacheService::populateOtherMapsAndLists (std::vector<std::shared_ptr<TargetGroupSegmentGroupMap> > allTargetGroupSegmentMappings) {

        MLOG(10) << "read "<<allTargetGroupSegmentMappings.size() <<"  allTargetGroupSegmentMappings entries";

        if (allTargetGroupSegmentMappings.empty()) {
                return;
        }

        /*
           populating allTargetGroupSegmentListMap
         */
        BOOST_FOREACH(std::shared_ptr<TargetGroupSegmentGroupMap>
                      tgSegmentPair, allTargetGroupSegmentMappings) {
                std::shared_ptr<Segment> segment = segmentCacheService->findByEntityId(tgSegmentPair->segmentGroupId);

                auto allSegments = this->allTargetGroupSegmentListMap->getOptional(tgSegmentPair->targetGroupId);
                if (allSegments == nullptr) {
                        allSegments =
                                std::make_shared<ObjectVectorHolder<Segment> > ();
                }
                allSegments->values->push_back(segment);
                this->allTargetGroupSegmentListMap->put(tgSegmentPair->targetGroupId, allSegments);
        }

        /*
           populating allSegmentTargetGroupListMap
         */
        BOOST_FOREACH(std::shared_ptr<TargetGroupSegmentGroupMap>
                      tgSegmentPair, allTargetGroupSegmentMappings) {
                std::shared_ptr<TargetGroup> tg = this->targetGroupCacheService->findByEntityId(tgSegmentPair->targetGroupId);
                auto allTargetGroups = this->allSegmentTargetGroupListMap->getOptional(tgSegmentPair->segmentGroupId);
                if (allTargetGroups == nullptr) {
                        allTargetGroups =
                                std::make_shared<ObjectVectorHolder<TargetGroup> > ();
                }
                allTargetGroups->values->push_back(tg);
                this->allSegmentTargetGroupListMap->put(tgSegmentPair->segmentGroupId, allTargetGroups);
        }

        if (allSegmentTargetGroupListMap->empty() ||
            allTargetGroupSegmentListMap->empty()) {
                throwEx("one of allTargetGroupSegmentMap is empty");
        }
}

std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<TargetGroup> > > TargetGroupSegmentGroupCacheService::getAllSegmentTargetGroupListMap() {
        return allSegmentTargetGroupListMap;
}


void TargetGroupSegmentGroupCacheService::clearOtherCaches() {
        MLOG(10) << "clearing TargetGroupSegmentGroupCacheService ";
        this->allSegmentTargetGroupListMap->clear();
        this->allTargetGroupSegmentListMap->clear();
}


std::shared_ptr<TargetGroupSegmentGroupCacheService>
TargetGroupSegmentGroupCacheService::getInstance(gicapods::ConfigService* configService) {
        static auto instance =
                std::make_shared<TargetGroupSegmentGroupCacheService>(
                        SegmentCacheService::getInstance(configService).get(),
                        TargetGroupCacheService::getInstance(configService).get(),
                        HttpUtilService::getInstance(configService).get(),
                        configService->get("dataMasterUrl"),
                        EntityToModuleStateStats::getInstance(configService).get(),
                        configService->get("appName"));

        return instance;
}
