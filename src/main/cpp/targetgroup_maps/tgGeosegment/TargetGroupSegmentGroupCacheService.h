#ifndef TargetGroupSegmentGroupCacheService_h
#define TargetGroupSegmentGroupCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "TargetGroupSegmentGroupMap.h"
#include "Segment.h"
#include "MySqlSegmentService.h"
#include "HttpUtilService.h"
#include "CacheService.h"
#include "EntityProviderService.h"
#include "ObjectVectorHolder.h"
class TargetGroupCacheService;

class Segment;
class SegmentCacheService;
class TargetGroup;

class EntityToModuleStateStats;

class TargetGroupSegmentGroupCacheService;



class TargetGroupSegmentGroupCacheService : public CacheService<TargetGroupSegmentGroupMap> {

private:

std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<Segment> > > allTargetGroupSegmentListMap;
std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<TargetGroup> > > allSegmentTargetGroupListMap;

public:

SegmentCacheService* segmentCacheService;
TargetGroupCacheService* targetGroupCacheService;

TargetGroupSegmentGroupCacheService(SegmentCacheService* segmentCacheService,
                                    TargetGroupCacheService* targetGroupCacheService,
                                    HttpUtilService* httpUtilService,
                                    std::string dataMasterUrl,
                                    EntityToModuleStateStats* entityToModuleStateStats,
                                    std::string appName);

std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<Segment> > > getAllTargetGroupSegmentListMap();

std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<TargetGroup> > > getAllSegmentTargetGroupListMap();

void populateOtherMapsAndLists (std::vector<std::shared_ptr<TargetGroupSegmentGroupMap> > allTargetGroupSegmentMaps);

void clearOtherCaches();
static std::shared_ptr<TargetGroupSegmentGroupCacheService> getInstance(gicapods::ConfigService* configService);
};


#endif
