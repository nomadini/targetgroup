#include "TargetGroupSegmentGroupMap.h"


#include "GUtil.h"
#include "TargetGroupSegmentGroupMap.h"
#include "ConverterUtil.h"
#include "JsonUtil.h"
#include <string>
#include <memory>

TargetGroupSegmentGroupMap::TargetGroupSegmentGroupMap() {
        id = 0;
        targetGroupId = 0;
        segmentGroupId = 0;
}


std::string TargetGroupSegmentGroupMap::toString() {
        return this->toJson ();
}
std::string TargetGroupSegmentGroupMap::getEntityName() {
        return "TargetGroupSegmentGroupMap";
}
std::shared_ptr<TargetGroupSegmentGroupMap> TargetGroupSegmentGroupMap::fromJson(std::string jsonString) {
        std::shared_ptr<TargetGroupSegmentGroupMap> tgMap (new TargetGroupSegmentGroupMap ());

        auto document = parseJsonSafely(jsonString);
        tgMap->id = JsonUtil::GetIntSafely (*document, "id");
        tgMap->targetGroupId = JsonUtil::GetIntSafely (*document, "targetGroupId");
        tgMap->segmentGroupId = JsonUtil::GetIntSafely (*document, "segmentGroupId");
        tgMap->dateCreated = JsonUtil::GetStringSafely (*document, "dateCreated");
        tgMap->dateModified = JsonUtil::GetStringSafely (*document, "dateModified");

        //MLOG(3)<<"this is the TargetGroupSegmentGroupMap created from json : "<<tgMap->toJson();
        return tgMap;
}


void TargetGroupSegmentGroupMap::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "segmentGroupId", segmentGroupId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateCreated", dateCreated, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateModified", dateModified, value);
}
std::shared_ptr<TargetGroupSegmentGroupMap> TargetGroupSegmentGroupMap::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<TargetGroupSegmentGroupMap> geoTargetGroupSegmentGroupMapList = std::make_shared<TargetGroupSegmentGroupMap>();
        geoTargetGroupSegmentGroupMapList->id = JsonUtil::GetIntSafely(value, "id");
        geoTargetGroupSegmentGroupMapList->targetGroupId  =JsonUtil::GetIntSafely(value,"targetGroupId");
        geoTargetGroupSegmentGroupMapList->segmentGroupId = JsonUtil::GetIntSafely(value,"segmentGroupId");
        geoTargetGroupSegmentGroupMapList->dateCreated = JsonUtil::GetStringSafely(value,"dateCreated");
        geoTargetGroupSegmentGroupMapList->dateModified = JsonUtil::GetStringSafely(value,"dateModified");
        return geoTargetGroupSegmentGroupMapList;
}

std::string TargetGroupSegmentGroupMap::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}
TargetGroupSegmentGroupMap::~TargetGroupSegmentGroupMap() {

}
