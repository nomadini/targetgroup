#ifndef TargetGroupSegmentGroupMap_H
#define TargetGroupSegmentGroupMap_H




#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "JsonTypeDefs.h"

class TargetGroupSegmentGroupMap;

class TargetGroupSegmentGroupMap {

private:

public:
int id;
int targetGroupId;
int segmentGroupId;
std::string dateCreated;
std::string dateModified;

TargetGroupSegmentGroupMap();

std::string toString();
static std::shared_ptr<TargetGroupSegmentGroupMap> fromJson(std::string jsonString);

std::string toJson();

virtual ~TargetGroupSegmentGroupMap();

static std::string getEntityName();
static std::shared_ptr<TargetGroupSegmentGroupMap> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};

#endif
