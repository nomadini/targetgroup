#include "TargetGroupSegmentMap.h"


#include "GUtil.h"
#include "TargetGroupSegmentMap.h"
#include "ConverterUtil.h"
#include "JsonUtil.h"
#include <string>
#include <memory>

TargetGroupSegmentMap::TargetGroupSegmentMap() {
        id = 0;
        targetGroupId = 0;
        segmentId = 0;
}


std::string TargetGroupSegmentMap::toString() {
        return this->toJson ();
}
std::string TargetGroupSegmentMap::getEntityName() {
        return "TargetGroupSegmentMap";
}
std::shared_ptr<TargetGroupSegmentMap> TargetGroupSegmentMap::fromJson(std::string jsonString) {
        std::shared_ptr<TargetGroupSegmentMap> tgMap (new TargetGroupSegmentMap ());

        auto document = parseJsonSafely(jsonString);
        tgMap->id = JsonUtil::GetIntSafely (*document, "id");
        tgMap->targetGroupId = JsonUtil::GetIntSafely (*document, "targetGroupId");
        tgMap->segmentId = JsonUtil::GetIntSafely (*document, "segmentId");
        tgMap->dateCreated = JsonUtil::GetStringSafely (*document, "dateCreated");
        tgMap->dateModified = JsonUtil::GetStringSafely (*document, "dateModified");

        //MLOG(3)<<"this is the TargetGroupSegmentMap created from json : "<<tgMap->toJson();
        return tgMap;
}


void TargetGroupSegmentMap::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "segmentId", segmentId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateCreated", dateCreated, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateModified", dateModified, value);
}
std::shared_ptr<TargetGroupSegmentMap> TargetGroupSegmentMap::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<TargetGroupSegmentMap> geoTargetGroupSegmentMapList = std::make_shared<TargetGroupSegmentMap>();
        geoTargetGroupSegmentMapList->id = JsonUtil::GetIntSafely(value, "id");
        geoTargetGroupSegmentMapList->targetGroupId  =JsonUtil::GetIntSafely(value,"targetGroupId");
        geoTargetGroupSegmentMapList->segmentId = JsonUtil::GetIntSafely(value,"segmentId");
        geoTargetGroupSegmentMapList->dateCreated = JsonUtil::GetStringSafely(value,"dateCreated");
        geoTargetGroupSegmentMapList->dateModified = JsonUtil::GetStringSafely(value,"dateModified");
        return geoTargetGroupSegmentMapList;
}

std::string TargetGroupSegmentMap::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}
TargetGroupSegmentMap::~TargetGroupSegmentMap() {

}
