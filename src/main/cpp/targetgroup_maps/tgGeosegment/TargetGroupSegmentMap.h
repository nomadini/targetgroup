#ifndef TargetGroupSegmentMap_H
#define TargetGroupSegmentMap_H




#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "JsonTypeDefs.h"

class TargetGroupSegmentMap;

class TargetGroupSegmentMap {

private:

public:
int id;
int targetGroupId;
int segmentId;
std::string dateCreated;
std::string dateModified;

TargetGroupSegmentMap();

std::string toString();
static std::shared_ptr<TargetGroupSegmentMap> fromJson(std::string jsonString);

std::string toJson();

virtual ~TargetGroupSegmentMap();

static std::string getEntityName();
static std::shared_ptr<TargetGroupSegmentMap> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};

#endif
