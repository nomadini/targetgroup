#include "MySqlTargetGroupGeoSegmentListMapService.h"
#include "MySqlDriver.h"

#include "JsonArrayUtil.h"
#include "CollectionUtil.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"

MySqlTargetGroupGeoSegmentListMapService::MySqlTargetGroupGeoSegmentListMapService(MySqlDriver* driver) {
        this->driver = driver;
}

std::unordered_map<int, int> MySqlTargetGroupGeoSegmentListMapService::readAll() {
        std::unordered_map<int, int> tgGeoSegmentIdMap;
        std::string query = "SELECT `targetgroup_geosegment_map`.`ID`,"
                            "`targetgroup_geosegment_map`.`TARGET_GROUP_ID`,"
                            "`targetgroup_geosegment_map`.`GEO_SEGMENT_LIST_ID`"
                            "FROM `targetgroup_geosegment_map`";

      

        auto res = driver->executeQuery (query);

        while (res->next ()) {
                MLOG(3) << "targetgroup_geosegment_map obj loaded : id  " << res->getInt (1);
                int tgid = res->getInt (2);
                int geoSegmentId = res->getInt (3);
                tgGeoSegmentIdMap.insert (
                        std::pair<int, int> (tgid,
                                             geoSegmentId));
        }

        return tgGeoSegmentIdMap;
}

void MySqlTargetGroupGeoSegmentListMapService::deleteAll() {
        driver->deleteAll("targetgroup_geosegment_map");
}


void MySqlTargetGroupGeoSegmentListMapService::insert(std::shared_ptr<TargetGroupGeoSegmentListMap> tg) {

        std::string queryStr =
                " INSERT INTO `targetgroup_geosegment_map`"
                " ("
                " `TARGET_GROUP_ID`,"
                " `GEO_SEGMENT_LIST_ID`,"
//                        " `STATUS`,"
                " `created_at`,"
                " `updated_at`"
                ")"
                " VALUES"
                " ( "
                " '__TARGET_GROUP_ID__',"
                " '__GEO_SEGMENT_LIST_ID__',"
//                        " '__STATUS__',"
                " '__created_at__',"
                " '__updated_at__');";

        MLOG(3) << "inserting new TargetGroupGeoSegmentList in db : " << tg->toJson ();

        queryStr = StringUtil::replaceString (queryStr, "__TARGET_GROUP_ID__",
                                              StringUtil::toStr (tg->targetGroupId));
        queryStr = StringUtil::replaceString (queryStr, "__GEO_SEGMENT_LIST_ID__",
                                              StringUtil::toStr (tg->geoSegmentListId));

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        //MLOG(3) << "queryStr : " << queryStr;
      
        driver->executedUpdateStatement (queryStr);
        tg->id = driver->getLastInsertedId ();
        

}

MySqlTargetGroupGeoSegmentListMapService::~MySqlTargetGroupGeoSegmentListMapService() {
}
