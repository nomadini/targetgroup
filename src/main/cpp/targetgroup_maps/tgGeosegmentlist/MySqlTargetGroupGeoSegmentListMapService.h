#ifndef MySqlTargetGroupGeoSegmentListMapService_h
#define MySqlTargetGroupGeoSegmentListMapService_h


#include "TargetGroupGeoSegmentListMap.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include <unordered_map>

class MySqlTargetGroupGeoSegmentListMapService;




class MySqlTargetGroupGeoSegmentListMapService {

public:
    MySqlDriver* driver;

    MySqlTargetGroupGeoSegmentListMapService(MySqlDriver* driver);

    virtual ~MySqlTargetGroupGeoSegmentListMapService();

    void parseTheAttribute(std::shared_ptr<TargetGroupGeoSegmentListMap> obj , std::string attributesFromDB);

    std::vector<std::shared_ptr<TargetGroupGeoSegmentListMap>> readAllTargetGroupGeoSegmentListMaps();

    void insert(std::shared_ptr<TargetGroupGeoSegmentListMap> TargetGroupGeoSegmentListMap);

    std::unordered_map<int , int> readAll();

    virtual void deleteAll();

};

#endif