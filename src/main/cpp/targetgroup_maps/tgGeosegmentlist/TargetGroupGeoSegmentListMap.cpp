#include "GUtil.h"
#include "TargetGroupGeoSegmentListMap.h"
#include "ConverterUtil.h"
#include "JsonUtil.h"
#include <string>
#include <memory>

TargetGroupGeoSegmentListMap::TargetGroupGeoSegmentListMap() {
  id = 0;
  targetGroupId = 0;
  geoSegmentListId = 0;
}


std::string TargetGroupGeoSegmentListMap::toString() {
    return this->toJson ();
}

std::shared_ptr<TargetGroupGeoSegmentListMap> TargetGroupGeoSegmentListMap::fromJson(std::string jsonString) {
    std::shared_ptr<TargetGroupGeoSegmentListMap> tgMap = std::make_shared<TargetGroupGeoSegmentListMap> ();

    auto document = parseJsonSafely(jsonString);


    if (document->HasMember ("id")) {
        tgMap->id = JsonUtil::GetIntSafely (*document , "id");
    }

    if (document->HasMember ("targetGroupId")) {
        tgMap->targetGroupId = JsonUtil::GetIntSafely (*document , "targetGroupId");
    }
    if (document->HasMember ("geoSegmentListId")) {
        tgMap->geoSegmentListId = JsonUtil::GetIntSafely (*document , "geoSegmentListId");
    }
    if (document->HasMember ("dateCreated")) {
        tgMap->dateCreated = JsonUtil::GetStringSafely (*document , "dateCreated");
    }
    if (document->HasMember ("dateModified")) {
        tgMap->dateModified = JsonUtil::GetStringSafely (*document , "dateModified");
    }

    MLOG(3) << "this is the TargetGroupGeoSegmentListMap created from json : " << tgMap->toJson ();
    return tgMap;
}

std::shared_ptr<TargetGroupGeoSegmentListMap> TargetGroupGeoSegmentListMap::fromJsonValue(RapidJsonValueType value) {
    std::shared_ptr<TargetGroupGeoSegmentListMap> obj = std::make_shared<TargetGroupGeoSegmentListMap>();

    obj->id =  JsonUtil::GetIntSafely(value, "id");
    obj->targetGroupId = JsonUtil::GetIntSafely(value, "targetGroupId");
    obj->geoSegmentListId = JsonUtil::GetIntSafely(value ,"geoSegmentListId");
    obj->dateCreated = JsonUtil::GetStringSafely(value, "dateCreated");
    obj->dateModified = JsonUtil::GetStringSafely(value, "dateModified");

    return obj;
}

void TargetGroupGeoSegmentListMap::addPropertiesToJsonValue(RapidJsonValueType value , DocumentType* doc) {

    JsonUtil::addMemberToValue_FromPair (doc , "id" , id , value);
    JsonUtil::addMemberToValue_FromPair (doc , "targetGroupId" , targetGroupId , value);
    JsonUtil::addMemberToValue_FromPair (doc , "geoSegmentListId" , geoSegmentListId , value);

    JsonUtil::addMemberToValue_FromPair (doc , "dateCreated" , dateCreated , value);
    JsonUtil::addMemberToValue_FromPair (doc , "dateModified" , dateModified , value);
}

std::string TargetGroupGeoSegmentListMap::toJson() {
  auto doc = JsonUtil::createDcoumentAsObjectDoc();
  RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
  addPropertiesToJsonValue(value, doc.get());
  return JsonUtil::valueToString (value);
}


TargetGroupGeoSegmentListMap::~TargetGroupGeoSegmentListMap() {

}
