#ifndef TargetGroupGeoSegmentListMap_H
#define TargetGroupGeoSegmentListMap_H




#include "JsonTypeDefs.h"


class TargetGroupGeoSegmentListMap;



class TargetGroupGeoSegmentListMap {

private:

public:
    int id;
	int targetGroupId;
	int geoSegmentListId;
	std::string dateCreated;
	std::string dateModified;

	TargetGroupGeoSegmentListMap();

	std::string toString();
	static std::shared_ptr<TargetGroupGeoSegmentListMap> fromJson(std::string jsonString);

	std::string toJson();

	virtual ~TargetGroupGeoSegmentListMap();


    static std::shared_ptr<TargetGroupGeoSegmentListMap> fromJsonValue(RapidJsonValueType value);
    void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};

/*

 std::shared_ptr<TargetGroupGeoSegmentListMap>List TargetGroupGeoSegmentListMapList;
 for (std::shared_ptr<TargetGroupGeoSegmentListMap>List::iterator it = TargetGroupGeoSegmentListMapList.begin();it != std::shared_ptr<TargetGroupGeoSegmentListMap>List.end(); ++it)
 {

 }
 */

#endif
