#include "GUtil.h"
#include "MySqlDriver.h"
#include "CollectionUtil.h"
#include "MySqlTargetGroupInventoryMapService.h"
#include "MySqlDriver.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"

MySqlTargetGroupInventoryMapService::MySqlTargetGroupInventoryMapService(MySqlDriver* driver) {
        this->driver = driver;

}

MySqlTargetGroupInventoryMapService::~MySqlTargetGroupInventoryMapService() {

}

std::vector<std::shared_ptr<TargetGroupInventoryMap>> MySqlTargetGroupInventoryMapService::readAll() {
        std::vector<std::shared_ptr<TargetGroupInventoryMap>> all;

        std::string query =
                " SELECT `ID`," //1
                " `TARGET_GROUP_ID`," //2
                " `INVENTORY_ID`," //3
                " `BID_STRATEGY_TYPE`," //4
                " `BID_VALUE`," //5
                " `BID_INVENTORY_PROFILE_ID`," //6
                " `created_at`," //7
                " `updated_at`" //8
                " FROM  targetgroup_bid_inventory_map ";


      
        auto res = driver->executeQuery (query);

        while (res->next ()) {
                std::shared_ptr<TargetGroupInventoryMap> obj (new TargetGroupInventoryMap ());
                obj->id = res->getInt (1);
                obj->targetGroupId = res->getInt (2);

                obj->inventoryId = res->getInt (3);
                obj->bidStrategyTypeId = res->getInt (4);
                obj->bidValue = res->getDouble (5);
                obj->bidInventoryProfileId = res->getInt (6);

                all.push_back (obj);
        }

        

        return all;
}

std::shared_ptr<TargetGroupInventoryMap> MySqlTargetGroupInventoryMapService::read(int id) {
        throwEx("error in mysql");
}

void MySqlTargetGroupInventoryMapService::update(std::shared_ptr<TargetGroupInventoryMap> obj) {

}

void MySqlTargetGroupInventoryMapService::insert(std::shared_ptr<TargetGroupInventoryMap> obj) {

        MLOG(3) << "inserting new TargetGroupInventoryMap in db : " << obj->toJson ();
        std::string queryStr =
                "INSERT INTO targetgroup_bid_inventory_map"
                " ("
                " TARGET_GROUP_ID,"
                " INVENTORY_ID,"
                " BID_STRATEGY_TYPE,"
                " BID_VALUE,"
                " BID_INVENTORY_PROFILE_ID,"
                " created_at,"
                " updated_at)  "
                " VALUES"
                " ("
                " '__TARGET_GROUP_ID__',"
                " '__INVENTORY_ID__',"
                " '__BID_STRATEGY_TYPE_ID__',"
                " '__BID_VALUE__',"
                " '__BID_INVENTORY_PROFILE_ID__',"
                " '__created_at__',"
                " '__updated_at__'"
                " );";

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__TARGET_GROUP_ID__",
                                              StringUtil::toStr (obj->targetGroupId));
        queryStr = StringUtil::replaceString (queryStr, "__INVENTORY_ID__",
                                              StringUtil::toStr (obj->inventoryId));
        queryStr = StringUtil::replaceString (queryStr, "__BID_STRATEGY_TYPE_ID__",
                                              StringUtil::toStr (obj->bidStrategyTypeId));
        queryStr = StringUtil::replaceString (queryStr, "__BID_VALUE__",
                                              StringUtil::toStr (obj->bidValue));
        queryStr = StringUtil::replaceString (queryStr, "__BID_INVENTORY_PROFILE_ID__",
                                              StringUtil::toStr (obj->bidInventoryProfileId));


        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);

        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        //MLOG(3) << "queryStr : " << queryStr;
      
        driver->executedUpdateStatement (queryStr);
        obj->id = driver->getLastInsertedId ();
        
}

void MySqlTargetGroupInventoryMapService::deleteAll() {
        driver->deleteAll("targetgroup_bid_inventory_map");
}
