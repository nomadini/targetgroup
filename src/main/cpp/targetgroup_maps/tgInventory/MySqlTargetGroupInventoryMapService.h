#ifndef MySqlTargetGroupInventoryMapService_h
#define MySqlTargetGroupInventoryMapService_h




#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "TargetGroupInventoryMap.h"

class MySqlTargetGroupInventoryMapService;




class MySqlTargetGroupInventoryMapService {

public:

    MySqlDriver* driver;

    MySqlTargetGroupInventoryMapService(MySqlDriver* driver);

    std::vector<std::shared_ptr<TargetGroupInventoryMap>> readAll();

    std::shared_ptr<TargetGroupInventoryMap> read(int id);

    void update(std::shared_ptr<TargetGroupInventoryMap> obj);

    void insert(std::shared_ptr<TargetGroupInventoryMap> obj);

    virtual void deleteAll();

    virtual ~MySqlTargetGroupInventoryMapService();

};


#endif
