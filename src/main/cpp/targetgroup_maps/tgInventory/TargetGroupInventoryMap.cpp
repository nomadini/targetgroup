#include "TargetGroupInventoryMap.h"


#include "GUtil.h"
#include "TargetGroupInventoryMap.h"
#include "ConverterUtil.h"
#include "JsonUtil.h"
#include <string>
#include <memory>

TargetGroupInventoryMap::TargetGroupInventoryMap() {
        id = 0;
        targetGroupId = 0;
        inventoryId = 0;
        bidStrategyTypeId = 0;
        bidValue = 0;
        bidInventoryProfileId = 0;
}


std::string TargetGroupInventoryMap::toString() {
        return this->toJson ();
}

std::shared_ptr<TargetGroupInventoryMap> TargetGroupInventoryMap::fromJson(std::string jsonString) {
        std::shared_ptr<TargetGroupInventoryMap> tgMap (new TargetGroupInventoryMap ());

        auto document = parseJsonSafely(jsonString);


        if (document->HasMember ("id")) {
                tgMap->id = JsonUtil::GetIntSafely (*document, "id");
        }

        if (document->HasMember ("targetGroupId")) {
                tgMap->targetGroupId = JsonUtil::GetIntSafely (*document, "targetGroupId");
        }
        if (document->HasMember ("inventoryId")) {
                tgMap->inventoryId = JsonUtil::GetIntSafely (*document, "inventoryId");
        }
        if (document->HasMember ("bidStrategyTypeId")) {
                tgMap->bidStrategyTypeId = JsonUtil::GetIntSafely (*document, "bidStrategyTypeId");
        }
        if (document->HasMember ("bidValue")) {
                tgMap->bidValue = JsonUtil::GetDoubleSafely (*document, "bidValue");
        }
        if (document->HasMember ("bidInventoryProfileId")) {
                tgMap->bidInventoryProfileId = JsonUtil::GetIntSafely (*document, "bidInventoryProfileId");
        }
        if (document->HasMember ("dateCreated")) {
                tgMap->dateCreated = JsonUtil::GetStringSafely (*document, "dateCreated");
        }
        if (document->HasMember ("dateModified")) {
                tgMap->dateModified = JsonUtil::GetStringSafely (*document, "dateModified");
        }

        //MLOG(3)<<"this is the TargetGroupInventoryMap created from json : "<<tgMap->toJson();
        return tgMap;
}

std::string TargetGroupInventoryMap::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "id",
                                            id, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "targetGroupId",
                                            targetGroupId, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "inventoryId",
                                            inventoryId, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "bidStrategyTypeId",
                                            bidStrategyTypeId, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "bidValue", bidValue, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "bidInventoryProfileId",
                                            bidInventoryProfileId, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "dateCreated",
                                            dateCreated, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "dateModified",
                                            dateModified, value);

        return JsonUtil::valueToString (value);
}

TargetGroupInventoryMap::~TargetGroupInventoryMap() {

}
