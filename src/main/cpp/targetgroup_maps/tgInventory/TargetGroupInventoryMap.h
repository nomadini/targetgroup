#ifndef TargetGroupInventoryMap_H
#define TargetGroupInventoryMap_H



#include <memory>
#include <string>
#include <vector>
#include <set>

class TargetGroupInventoryMap;

class TargetGroupInventoryMap {

public:

    int id;
	int targetGroupId;
	int inventoryId;
	int bidStrategyTypeId;
	double bidValue;
	int bidInventoryProfileId;


	std::string dateCreated;
	std::string dateModified;

    static std::shared_ptr<TargetGroupInventoryMap> fromJson(std::string jsonString);

    std::string toJson();
    std::string toString();
    TargetGroupInventoryMap();
    virtual ~TargetGroupInventoryMap();
};

#endif