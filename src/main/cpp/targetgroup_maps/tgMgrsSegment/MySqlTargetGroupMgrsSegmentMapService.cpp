#include "MySqlTargetGroupMgrsSegmentMapService.h"
#include "MySqlDriver.h"
#include "TopMgrsSegment.h"

#include "JsonArrayUtil.h"
#include "CollectionUtil.h"
#include "JsonUtil.h"
#include "TargetGroupMgrsSegmentMap.h"
#include "DateTimeUtil.h"

MySqlTargetGroupMgrsSegmentMapService::MySqlTargetGroupMgrsSegmentMapService(MySqlDriver* driver) {
        this->driver = driver;
}

std::vector<std::shared_ptr<TargetGroupMgrsSegmentMap> > MySqlTargetGroupMgrsSegmentMapService::readAllEntities() {
        return readAllAsObjects();
}

std::vector<std::shared_ptr<TargetGroupMgrsSegmentMap> > MySqlTargetGroupMgrsSegmentMapService::readAllAsObjects() {
        std::vector<std::shared_ptr<TargetGroupMgrsSegmentMap> > allObjects;
        if (mapOfTopMgrsSegments.empty()) {
                assertAndThrow(mySqlTopMgrsSegmentService != NULL);
                auto allTopMgrsSegments = mySqlTopMgrsSegmentService->readAllEntities();
                for (auto && topMgrsSegment : allTopMgrsSegments) {
                        mapOfTopMgrsSegments.insert(std::make_pair(topMgrsSegment->id, topMgrsSegment));
                }
        }

        assertAndThrow(!mapOfTopMgrsSegments.empty());

        std::string query = "SELECT targetgroup_mgrs_map.id,"
                            "targetgroup_mgrs_map.targetgroup_id,"
                            "targetgroup_mgrs_map.mgrs_segment_id,"
                            "targetgroup_mgrs_map.created_at,"
                            "targetgroup_mgrs_map.updated_at "
                            " FROM targetgroup_mgrs_map ";



        auto res = driver->executeQuery (query);

        while (res->next ()) {
                auto tgTopMgrsSegmentsMap = std::make_shared<TargetGroupMgrsSegmentMap>();

                tgTopMgrsSegmentsMap->id = res->getInt (1);
                tgTopMgrsSegmentsMap->targetGroupId = res->getInt (2);
                tgTopMgrsSegmentsMap->mgrsSegmentId = res->getInt (3);
                tgTopMgrsSegmentsMap->dateCreated = MySqlDriver::getString( res, 4);
                tgTopMgrsSegmentsMap->dateModified = MySqlDriver::getString( res, 5);
                auto pair = mapOfTopMgrsSegments.find(tgTopMgrsSegmentsMap->mgrsSegmentId);
                assertAndThrow(pair != mapOfTopMgrsSegments.end());
                tgTopMgrsSegmentsMap->topMgrsSegment = pair->second;
                allObjects.push_back(tgTopMgrsSegmentsMap);

        }



        return allObjects;

}

void MySqlTargetGroupMgrsSegmentMapService::deleteAll() {
        driver->deleteAll("targetgroup_mgrs_map");
}


void MySqlTargetGroupMgrsSegmentMapService::insert(std::shared_ptr<TargetGroupMgrsSegmentMap> tg) {
        std::string queryStr =
                " INSERT INTO targetgroup_mgrs_map"
                " ("
                " TARGET_GROUP_ID,"
                " mgrs_segment_id,"
                " created_at,"
                " updated_at"
                ")"
                " VALUES"
                " ( "
                " '__TARGET_GROUP_ID__',"
                " '__mgrs_segment_id__',"
                " '__created_at__',"
                " '__updated_at__');";

        MLOG(3) << "inserting new TargetGroupMgrsSegmentMap in db : " << tg->toJson ();

        queryStr = StringUtil::replaceString (queryStr, "__TARGET_GROUP_ID__",
                                              StringUtil::toStr (tg->targetGroupId));
        queryStr = StringUtil::replaceString (queryStr, "__mgrs_segment_id__", StringUtil::toStr (tg->mgrsSegmentId));

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        driver->executedUpdateStatement (queryStr);
        tg->id = driver->getLastInsertedId ();



}

MySqlTargetGroupMgrsSegmentMapService::~MySqlTargetGroupMgrsSegmentMapService() {
}
