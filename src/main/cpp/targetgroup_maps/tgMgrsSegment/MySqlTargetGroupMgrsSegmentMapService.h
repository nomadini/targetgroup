#ifndef MySqlTargetGroupMgrsSegmentMapService_h
#define MySqlTargetGroupMgrsSegmentMapService_h


#include "TargetGroupMgrsSegmentMap.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"
#include "MySqlTopMgrsSegmentService.h"
class MySqlTargetGroupMgrsSegmentMapService;
class MySqlTargetGroupMgrsSegmentMapService : public DataProvider<TargetGroupMgrsSegmentMap> {

public:
MySqlDriver* driver;
std::unordered_map<int, std::shared_ptr<TopMgrsSegment> > mapOfTopMgrsSegments;
MySqlTopMgrsSegmentService* mySqlTopMgrsSegmentService;
MySqlTargetGroupMgrsSegmentMapService(MySqlDriver* driver);


virtual ~MySqlTargetGroupMgrsSegmentMapService();

void parseTheAttribute(std::shared_ptr<TargetGroupMgrsSegmentMap> obj, std::string attributesFromDB);

std::unordered_map<int, std::vector<int> > readAll();

std::vector<std::shared_ptr<TargetGroupMgrsSegmentMap> > readAllAsObjects();

void insert(std::shared_ptr<TargetGroupMgrsSegmentMap> TargetGroupMgrsSegmentMap);

void deleteAll();

std::vector<std::shared_ptr<TargetGroupMgrsSegmentMap> > readAllEntities();
};

#endif
