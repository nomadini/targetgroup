#include "MySqlTopMgrsSegmentService.h"
#include "MySqlDriver.h"
#include "TopMgrsSegment.h"

#include "JsonArrayUtil.h"
#include "CollectionUtil.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"

MySqlTopMgrsSegmentService::MySqlTopMgrsSegmentService(MySqlDriver* driver) {
        this->driver = driver;


}

std::vector<std::shared_ptr<TopMgrsSegment> > MySqlTopMgrsSegmentService::readAllEntities() {
        return readAllAsObjects();
}

std::vector<std::shared_ptr<TopMgrsSegment> > MySqlTopMgrsSegmentService::readAllAsObjects() {
        std::string query = "SELECT top_mgrs_segment.id,"
                            "top_mgrs_segment.client_id,"
                            "top_mgrs_segment.mgrs100,"
                            "top_mgrs_segment.created_at,"
                            "top_mgrs_segment.updated_at "
                            " FROM top_mgrs_segment ";

        auto res = driver->executeQuery (query);
        std::vector<std::shared_ptr<TopMgrsSegment> > allObjects;
        while (res->next ()) {
                auto obj = std::make_shared<TopMgrsSegment>();

                obj->id = res->getInt ("id");
                obj->clientId = res->getInt ("client_id");
                obj->mgrs100 = res->getString ("mgrs100");
                obj->dateCreated = MySqlDriver::getString( res, 4);
                obj->dateModified = MySqlDriver::getString( res, 5);
                allObjects.push_back(obj);

        }



        return allObjects;

}

void MySqlTopMgrsSegmentService::deleteAll() {
        driver->deleteAll("top_mgrs_segment");
}


void MySqlTopMgrsSegmentService::insert(std::shared_ptr<TopMgrsSegment> tg) {
        std::string queryStr =
                " INSERT INTO top_mgrs_segment"
                " ("
                " client_id,"
                " mgrs100,"
                " created_at,"
                " updated_at"
                ")"
                " VALUES"
                " ( "
                " '__client_id__',"
                " '__mgrs100__',"
                " '__created_at__',"
                " '__updated_at__');";

        MLOG(3) << "inserting new TopMgrsSegment in db : " << tg->toJson ();

        queryStr = StringUtil::replaceString (queryStr, "__client_id__",
                                              StringUtil::toStr (tg->clientId));
        queryStr = StringUtil::replaceString (queryStr, "__mgrs100__", tg->mgrs100);

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();

        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);

        driver->executedUpdateStatement (queryStr);
        tg->id = driver->getLastInsertedId ();
}

MySqlTopMgrsSegmentService::~MySqlTopMgrsSegmentService() {
}
