#ifndef MySqlTopMgrsSegmentService_h
#define MySqlTopMgrsSegmentService_h


#include "TopMgrsSegment.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include "DataProvider.h"
class MySqlTopMgrsSegmentService;
class MySqlTopMgrsSegmentService : public DataProvider<TopMgrsSegment> {

public:
MySqlDriver* driver;
MySqlTopMgrsSegmentService(MySqlDriver* driver);


virtual ~MySqlTopMgrsSegmentService();

void parseTheAttribute(std::shared_ptr<TopMgrsSegment> obj, std::string attributesFromDB);

std::vector<std::shared_ptr<TopMgrsSegment> > readAllAsObjects();

void insert(std::shared_ptr<TopMgrsSegment> TopMgrsSegment);

void deleteAll();

std::vector<std::shared_ptr<TopMgrsSegment> > readAllEntities();
};

#endif
