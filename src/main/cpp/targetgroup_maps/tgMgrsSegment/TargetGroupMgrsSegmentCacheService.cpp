#include "TargetGroup.h"
#include "StringUtil.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "TargetGroupMgrsSegmentCacheService.h"
#include "MySqlTargetGroupService.h"
#include "EntityToModuleStateStats.h"
#include "MySqlTargetGroupMgrsSegmentMapService.h"
#include "JsonArrayUtil.h"
TargetGroupMgrsSegmentCacheService::TargetGroupMgrsSegmentCacheService(MySqlTargetGroupMgrsSegmentMapService* mySqlTargetGroupMgrsSegmentMapService,
                                                                       TargetGroupCacheService* targetGroupCacheService,
                                                                       HttpUtilService* httpUtilService,
                                                                       std::string dataMasterUrl,
                                                                       EntityToModuleStateStats* entityToModuleStateStats,std::string appName)  :
        CacheService<TargetGroupMgrsSegmentMap>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName) {

        this->targetGroupCacheService = targetGroupCacheService;
        this->allTargetGroupTopMgrsSegmentsMap =
                std::make_shared<gicapods::ConcurrentHashMap<int,  gicapods::ConcurrentHashMap<std::string, TopMgrsSegment> > >();
        this->entityToModuleStateStats = entityToModuleStateStats;
}

std::shared_ptr<gicapods::ConcurrentHashMap<int,  gicapods::ConcurrentHashMap<std::string, TopMgrsSegment>  > > TargetGroupMgrsSegmentCacheService::getAllTargetGroupTopMgrsSegmentsMap() {
        return allTargetGroupTopMgrsSegmentsMap;
}

std::string TargetGroupMgrsSegmentCacheService::getEntityName() {
        return "TargetGroupMgrsSegmentMap";
}

void TargetGroupMgrsSegmentCacheService::clearOtherCaches() {
        allTargetGroupTopMgrsSegmentsMap->clear ();
}

void TargetGroupMgrsSegmentCacheService::populateOtherMapsAndLists(
        std::vector<std::shared_ptr<TargetGroupMgrsSegmentMap> > allTargetGroupTopMgrsSegments) {

        entityToModuleStateStats->addStateModuleForEntity ("TG_MGRS_SEGMENT_MAP_READ_FROM_DB_SIZE : " + StringUtil::toStr(allTargetGroupTopMgrsSegments.size()),
                                                           "TargetGroupMgrsSegmentCacheService",
                                                           "ALL");


        for(std::shared_ptr<TargetGroupMgrsSegmentMap> targetGroupMgrsSegmentMap :  allTargetGroupTopMgrsSegments) {

                auto mapOfTopMgrsSegmentPair = allTargetGroupTopMgrsSegmentsMap->getOptional(targetGroupMgrsSegmentMap->targetGroupId);
                if (mapOfTopMgrsSegmentPair != nullptr) {
                        mapOfTopMgrsSegmentPair->put(
                                StringUtil::toLowerCase(targetGroupMgrsSegmentMap->topMgrsSegment->mgrs100),
                                targetGroupMgrsSegmentMap->topMgrsSegment );
                } else {
                        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TopMgrsSegment> >  mapOfTopMgrsSegments =
                                std::make_shared<gicapods::ConcurrentHashMap<std::string, TopMgrsSegment> >();
                        mapOfTopMgrsSegments->put(
                                StringUtil::toLowerCase(targetGroupMgrsSegmentMap->topMgrsSegment->mgrs100),
                                targetGroupMgrsSegmentMap->topMgrsSegment);
                        allTargetGroupTopMgrsSegmentsMap->put(targetGroupMgrsSegmentMap->targetGroupId, mapOfTopMgrsSegments);
                }
        }

        if(!allTargetGroupTopMgrsSegments.empty()) {
                if(this->allTargetGroupTopMgrsSegmentsMap->empty()) {
                        entityToModuleStateStats->addStateModuleForEntity ("ERROR_LOADED_TG_MGRS_SEGMENT_MAP",
                                                                           "TargetGroupMgrsSegmentCacheService",
                                                                           "ALL");

                }
                assertAndThrow(this->allTargetGroupTopMgrsSegmentsMap->size() > 0);
        }

        entityToModuleStateStats->addStateModuleForEntity ("SizeOfTargetGroupMgrsSegmentMap : " + StringUtil::toStr(allTargetGroupTopMgrsSegmentsMap->size()),
                                                           "TargetGroupMgrsSegmentCacheService",
                                                           "ALL");
}

TargetGroupMgrsSegmentCacheService::~TargetGroupMgrsSegmentCacheService() {

}
