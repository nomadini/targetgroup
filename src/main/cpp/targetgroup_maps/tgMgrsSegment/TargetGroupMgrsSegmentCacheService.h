#ifndef TargetGroupMgrsSegmentCacheService_h
#define TargetGroupMgrsSegmentCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>

#include "MySqlTargetGroupMgrsSegmentMapService.h"
class TargetGroupCacheService;
#include "CacheService.h"
#include "MySqlTargetGroupMgrsSegmentMapService.h"
#include "MySqlTargetGroupMgrsSegmentMapService.h"
#include "HttpUtilService.h"
#include "DataProvider.h"
#include "EntityProviderService.h"
#include "ConcurrentHashMap.h"
class EntityToModuleStateStats;

class TargetGroupMgrsSegmentCacheService;



class TargetGroupMgrsSegmentCacheService : public CacheService<TargetGroupMgrsSegmentMap>{

private:
std::shared_ptr<gicapods::ConcurrentHashMap<int,  gicapods::ConcurrentHashMap<std::string, TopMgrsSegment>  > > allTargetGroupTopMgrsSegmentsMap;
EntityToModuleStateStats* entityToModuleStateStats;
public:

TargetGroupCacheService* targetGroupCacheService;

TargetGroupMgrsSegmentCacheService(MySqlTargetGroupMgrsSegmentMapService* mySqlTargetGroupMgrsSegmentMapService,
                                   TargetGroupCacheService* targetGroupCacheService,
                                   HttpUtilService* httpUtilService,
                                   std::string dataMasterUrl,
                                   EntityToModuleStateStats* entityToModuleStateStats,std::string appName);


std::shared_ptr<gicapods::ConcurrentHashMap<int,  gicapods::ConcurrentHashMap<std::string, TopMgrsSegment>  > > getAllTargetGroupTopMgrsSegmentsMap();

void clearOtherCaches();

void populateOtherMapsAndLists(std::vector<std::shared_ptr<TargetGroupMgrsSegmentMap> > allEntities);

std::string getEntityName();

virtual ~TargetGroupMgrsSegmentCacheService();

};


#endif
