#include "GUtil.h"
#include "TargetGroupMgrsSegmentMap.h"
#include "ConverterUtil.h"
#include "JsonUtil.h"
#include <string>
#include <memory>
#include "TopMgrsSegment.h"
TargetGroupMgrsSegmentMap::TargetGroupMgrsSegmentMap() {
        id =0;
        targetGroupId=0;
        mgrsSegmentId=0;
        topMgrsSegment = std::make_shared<TopMgrsSegment>();
}

std::string TargetGroupMgrsSegmentMap::toString() {
        return this->toJson ();
}
std::string TargetGroupMgrsSegmentMap::getEntityName() {
        return "TargetGroupMgrsSegmentMap";
}


std::shared_ptr<TargetGroupMgrsSegmentMap> TargetGroupMgrsSegmentMap::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<TargetGroupMgrsSegmentMap> obj = std::make_shared<TargetGroupMgrsSegmentMap>();

        obj->id =  JsonUtil::GetIntSafely(value, "id");
        obj->targetGroupId = JsonUtil::GetIntSafely(value, "targetGroupId");
        obj->mgrsSegmentId = JsonUtil::GetIntSafely(value,"mgrsSegmentId");
        obj->dateCreated = JsonUtil::GetStringSafely(value, "dateCreated");
        obj->dateModified = JsonUtil::GetStringSafely(value, "dateModified");
        RapidJsonValueTypeNoRef topMgrsSegmentValue(rapidjson::kObjectType);
        topMgrsSegmentValue = value["topMgrsSegment"];
        obj->topMgrsSegment = TopMgrsSegment::fromJsonValue(topMgrsSegmentValue);

        return obj;
}

void TargetGroupMgrsSegmentMap::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "mgrsSegmentId", mgrsSegmentId, value);

        JsonUtil::addMemberToValue_FromPair (doc, "dateCreated", dateCreated, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateModified", dateModified, value);
        JsonUtil::addMemberToValue_FromPair (doc, "topMgrsSegment", topMgrsSegment, value);
}

std::string TargetGroupMgrsSegmentMap::toJson() {
        auto doc = JsonUtil::createDcoumentAsObjectDoc();
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}

std::shared_ptr<TargetGroupMgrsSegmentMap> TargetGroupMgrsSegmentMap::fromJson(std::string jsonString) {
        std::shared_ptr<TargetGroupMgrsSegmentMap> tgMap = std::make_shared<TargetGroupMgrsSegmentMap> ();

        auto document = parseJsonSafely(jsonString);

        tgMap->id = JsonUtil::GetIntSafely (*document, "id");
        tgMap->targetGroupId = JsonUtil::GetIntSafely (*document, "targetGroupId");
        tgMap->mgrsSegmentId = JsonUtil::GetIntSafely (*document, "mgrsSegmentId");
        tgMap->dateCreated = JsonUtil::GetStringSafely (*document, "dateCreated");
        tgMap->dateModified = JsonUtil::GetStringSafely (*document, "dateModified");
        RapidJsonValueType value = (*document)["topMgrsSegment"];
        tgMap->topMgrsSegment = TopMgrsSegment::fromJsonValue(value);

        return tgMap;
}



TargetGroupMgrsSegmentMap::~TargetGroupMgrsSegmentMap() {

}
