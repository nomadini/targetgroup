#ifndef TargetGroupMgrsSegmentMap_H
#define TargetGroupMgrsSegmentMap_H




#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "JsonTypeDefs.h"

class TopMgrsSegment;

class TargetGroupMgrsSegmentMap;



class TargetGroupMgrsSegmentMap {

private:

public:
int id;
int targetGroupId;
int mgrsSegmentId;
std::shared_ptr<TopMgrsSegment> topMgrsSegment;

std::string dateCreated;
std::string dateModified;

TargetGroupMgrsSegmentMap();

std::string toString();
static std::shared_ptr<TargetGroupMgrsSegmentMap> fromJson(std::string jsonString);

std::string toJson();
static std::string getEntityName();

virtual ~TargetGroupMgrsSegmentMap();

static std::shared_ptr<TargetGroupMgrsSegmentMap> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};

#endif
