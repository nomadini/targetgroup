#include "GUtil.h"
#include "TopMgrsSegment.h"
#include "ConverterUtil.h"
#include "JsonUtil.h"
#include <string>
#include <memory>
#include "TopMgrsSegment.h"
TopMgrsSegment::TopMgrsSegment() {
        id = 0;
        clientId = 0;
}

std::string TopMgrsSegment::toString() {
        return this->toJson ();
}
std::string TopMgrsSegment::getEntityName() {
        return "TopMgrsSegment";
}


std::shared_ptr<TopMgrsSegment> TopMgrsSegment::fromJsonValue(RapidJsonValueType value) {
        std::shared_ptr<TopMgrsSegment> obj = std::make_shared<TopMgrsSegment>();

        obj->id =  JsonUtil::GetIntSafely(value, "id");
        obj->clientId = JsonUtil::GetIntSafely(value, "clientId");
        obj->mgrs100 = JsonUtil::GetStringSafely(value,"mgrs100");
        obj->dateCreated = JsonUtil::GetStringSafely(value, "dateCreated");
        obj->dateModified = JsonUtil::GetStringSafely(value, "dateModified");
        return obj;
}

void TopMgrsSegment::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {

        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "clientId", clientId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "mgrs100", mgrs100, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateCreated", dateCreated, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateModified", dateModified, value);
}

std::string TopMgrsSegment::toJson() {
        auto doc = JsonUtil::createDcoumentAsObjectDoc();
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);
}

std::shared_ptr<TopMgrsSegment> TopMgrsSegment::fromJson(std::string jsonString) {
        std::shared_ptr<TopMgrsSegment> tgMap = std::make_shared<TopMgrsSegment> ();

        auto document = parseJsonSafely(jsonString);
        tgMap->id = JsonUtil::GetIntSafely (*document, "id");
        tgMap->clientId = JsonUtil::GetIntSafely (*document, "clientId");
        tgMap->mgrs100 = JsonUtil::GetStringSafely (*document, "mgrs100");
        tgMap->dateCreated = JsonUtil::GetStringSafely (*document, "dateCreated");
        tgMap->dateModified = JsonUtil::GetStringSafely (*document, "dateModified");
        return tgMap;
}



TopMgrsSegment::~TopMgrsSegment() {

}
