#ifndef TopMgrsSegment_H
#define TopMgrsSegment_H




#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "JsonTypeDefs.h"

class TopMgrsSegment;

class TopMgrsSegment;

class TopMgrsSegment {

private:

public:
int id;
int clientId;
std::string mgrs100;

std::string dateCreated;
std::string dateModified;

TopMgrsSegment();

std::string toString();
static std::shared_ptr<TopMgrsSegment> fromJson(std::string jsonString);

std::string toJson();
static std::string getEntityName();

virtual ~TopMgrsSegment();

static std::shared_ptr<TopMgrsSegment> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};

#endif
