#include "MySqlTargetGroupRecencyModelMapService.h"
#include "MySqlDriver.h"

#include "JsonArrayUtil.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "CollectionUtil.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include "MySqlRecencyModelService.h"
#include "MySqlDriver.h"

MySqlTargetGroupRecencyModelMapService::MySqlTargetGroupRecencyModelMapService(
        MySqlDriver* driver,
        MySqlRecencyModelService* mySqlRecencyModelService
        ) {
        this->driver = driver;
        this->mySqlRecencyModelService = mySqlRecencyModelService;
}

std::vector<std::shared_ptr<TargetGroupRecencyModelMap> > MySqlTargetGroupRecencyModelMapService::readAllByTgId(int targetGroupId) {
        std::vector<std::shared_ptr<TargetGroupRecencyModelMap> > all;
        std::string queryStr =
                "SELECT `id`,"
                "`targetgroup_id`,"
                "`recency_model_id`"
                "FROM `targetgroup_recency_model_map` where targetgroup_id = __targetgroup_id__ ";


        queryStr = StringUtil::replaceString (queryStr, "__targetgroup_id__",
                                              StringUtil::toStr (targetGroupId));
        auto res = driver->executeQuery (queryStr);

        while (res->next ()) {
                std::shared_ptr<TargetGroupRecencyModelMap> one = std::make_shared<TargetGroupRecencyModelMap> ();
                MLOG(3) << "targetgroup_recency_model_map obj loaded : id  " << res->getInt ("id");
                one->targetGroupId = res->getInt ("targetgroup_id");
                one->recencyModelId = res->getInt ("recency_model_id");
                all.push_back (one);
        }

        return all;

}

std::vector<std::shared_ptr<TargetGroupRecencyModelMap> > MySqlTargetGroupRecencyModelMapService::readAll() {
        std::vector<std::shared_ptr<RecencyModel> > allModels = mySqlRecencyModelService->readAllEntities();
        auto mapOfModels = CollectionUtil::convertListToMap(allModels);

        std::vector<std::shared_ptr<TargetGroupRecencyModelMap> > all;
        std::string query = "SELECT `ID`,"
                            "`targetgroup_id`,"
                            "`recency_model_id`"
                            "FROM `targetgroup_recency_model_map`";



        auto res = driver->executeQuery (query);

        while (res->next ()) {
                std::shared_ptr<TargetGroupRecencyModelMap> one = std::make_shared<TargetGroupRecencyModelMap> ();
                one->id = res->getInt ("id");
                one->targetGroupId = res->getInt ("targetgroup_id");
                one->recencyModelId = res->getInt ("recency_model_id");
                auto pairPtr = mapOfModels.find(one->recencyModelId);
                if (pairPtr == mapOfModels.end()) {
                        throwEx("no map found for this mapping entry :" + one->toJson());
                }
                one->model = pairPtr->second;
                all.push_back (one);
        }

        return all;
}

void MySqlTargetGroupRecencyModelMapService::deleteAll() {
        driver->deleteAll("targetgroup_recency_model_map");
}


void MySqlTargetGroupRecencyModelMapService::insert(std::shared_ptr<TargetGroupRecencyModelMap> tg) {
        std::string queryStr =
                " INSERT INTO `targetgroup_recency_model_map`"
                " ("
                " `targetgroup_id`,"
                " `recency_model_id`,"
                " `created_at`,"
                " `updated_at`"
                ")"
                " VALUES"
                " ( "
                " '__targetgroup_id__',"
                " '__recency_model_id__',"
                " '__created_at__',"
                " '__updated_at__')";

        MLOG(3) << "inserting new TargetGroupSegment in db : " << tg->toJson ();


        queryStr = StringUtil::replaceString (queryStr, "__targetgroup_id__",
                                              StringUtil::toStr (tg->targetGroupId));
        queryStr = StringUtil::replaceString (queryStr, "__recency_model_id__",
                                              StringUtil::toStr (tg->recencyModelId));

        Poco::Timestamp now;
        std::string date = DateTimeUtil::getNowInMySqlFormat ();


        queryStr = StringUtil::replaceString (queryStr, "__created_at__", date);
        queryStr = StringUtil::replaceString (queryStr, "__updated_at__", date);


        //MLOG(3) << "queryStr : " << queryStr;

        driver->executedUpdateStatement (queryStr);
        tg->id = driver->getLastInsertedId ();


}

std::vector<std::shared_ptr<TargetGroupRecencyModelMap> > MySqlTargetGroupRecencyModelMapService::readAllEntities() {
        return this->readAll();
}

MySqlTargetGroupRecencyModelMapService::~MySqlTargetGroupRecencyModelMapService() {
}
