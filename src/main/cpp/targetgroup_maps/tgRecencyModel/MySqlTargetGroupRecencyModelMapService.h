#ifndef MySqlTargetGroupRecencyModelMapService_h
#define MySqlTargetGroupRecencyModelMapService_h


#include "TargetGroupRecencyModelMap.h"
#include "MySqlDriver.h"
#include <cppconn/resultset.h>
#include <tbb/concurrent_hash_map.h>
#include "DataProvider.h"
class MySqlRecencyModelService;
class MySqlTargetGroupRecencyModelMapService;



class MySqlTargetGroupRecencyModelMapService : public DataProvider<TargetGroupRecencyModelMap> {

public:
MySqlDriver* driver;
MySqlRecencyModelService* mySqlRecencyModelService;
MySqlTargetGroupRecencyModelMapService(MySqlDriver* driver,
                                       MySqlRecencyModelService* mySqlRecencyModelService);


virtual ~MySqlTargetGroupRecencyModelMapService();

std::vector<std::shared_ptr<TargetGroupRecencyModelMap> > readAll();

std::vector<std::shared_ptr<TargetGroupRecencyModelMap> > readAllByTgId(int targetGroupId);

void insert(std::shared_ptr<TargetGroupRecencyModelMap> TargetGroupRecencyModelMap);

virtual void deleteAll();

std::vector<std::shared_ptr<TargetGroupRecencyModelMap> > readAllEntities();

};

#endif
