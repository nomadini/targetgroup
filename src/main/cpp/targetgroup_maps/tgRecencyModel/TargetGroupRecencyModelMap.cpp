#include "TargetGroupRecencyModelMap.h"


#include "GUtil.h"
#include "TargetGroupRecencyModelMap.h"
#include "ConverterUtil.h"
#include "JsonUtil.h"
#include "RecencyModel.h"
#include <string>
#include <memory>

TargetGroupRecencyModelMap::TargetGroupRecencyModelMap() {
        id = 0;
        targetGroupId = 0;
        recencyModelId = 0;
}


std::string TargetGroupRecencyModelMap::toString() {
        return this->toJson ();
}
std::string TargetGroupRecencyModelMap::getEntityName() {
        return "TargetGroupRecencyModelMap";
}
std::shared_ptr<TargetGroupRecencyModelMap> TargetGroupRecencyModelMap::fromJson(std::string jsonString) {
        auto document = parseJsonSafely(jsonString);
        return fromJsonValue(*document);
}


void TargetGroupRecencyModelMap::addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc) {
        JsonUtil::addMemberToValue_FromPair (doc, "id", id, value);
        JsonUtil::addMemberToValue_FromPair (doc, "targetGroupId", targetGroupId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "recencyModelId", recencyModelId, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateCreated", dateCreated, value);
        JsonUtil::addMemberToValue_FromPair (doc, "dateModified", dateModified, value);
        JsonUtil::addMemberToValue_FromPair (doc, "model", model, value);
}
std::shared_ptr<TargetGroupRecencyModelMap> TargetGroupRecencyModelMap::fromJsonValue(RapidJsonValueType value) {

        std::shared_ptr<TargetGroupRecencyModelMap> mapping = std::make_shared<TargetGroupRecencyModelMap>();
        mapping->id = JsonUtil::GetIntSafely(value, "id");
        mapping->targetGroupId  =JsonUtil::GetIntSafely(value,"targetGroupId");
        mapping->recencyModelId = JsonUtil::GetIntSafely(value,"recencyModelId");
        mapping->dateCreated = JsonUtil::GetStringSafely(value,"dateCreated");
        mapping->dateModified = JsonUtil::GetStringSafely(value,"dateModified");
        assertAndThrow(value.HasMember("model"));
        mapping->model = RecencyModel::fromJsonValue(value["model"]);
        return mapping;
}

std::string TargetGroupRecencyModelMap::toJson() {
        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        addPropertiesToJsonValue(value, doc.get());
        return JsonUtil::valueToString (value);

}

TargetGroupRecencyModelMap::~TargetGroupRecencyModelMap() {

}
