#ifndef TargetGroupRecencyModelMap_H
#define TargetGroupRecencyModelMap_H




#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>
#include "JsonTypeDefs.h"

class RecencyModel;

class TargetGroupRecencyModelMap {

private:

public:
int id;
int targetGroupId;
int recencyModelId;
std::string dateCreated;
std::string dateModified;
std::shared_ptr<RecencyModel> model;

TargetGroupRecencyModelMap();

std::string toString();
static std::shared_ptr<TargetGroupRecencyModelMap> fromJson(std::string jsonString);

std::string toJson();

virtual ~TargetGroupRecencyModelMap();

static std::string getEntityName();
static std::shared_ptr<TargetGroupRecencyModelMap> fromJsonValue(RapidJsonValueType value);
void addPropertiesToJsonValue(RapidJsonValueType value, DocumentType* doc);
};

#endif
