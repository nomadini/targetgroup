
#include "StringUtil.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "TargetGroupRecencyModelMapCacheService.h"
#include "MySqlTargetGroupService.h"
#include "RecencyModel.h"
#include "JsonArrayUtil.h"
#include "ObjectVectorHolder.h"

TargetGroupRecencyModelMapCacheService::TargetGroupRecencyModelMapCacheService(
        MySqlTargetGroupRecencyModelMapService* mySqlTargetGroupRecencyModelMapService,
        HttpUtilService* httpUtilService,
        std::string dataMasterUrl,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string appName) :
        CacheService<TargetGroupRecencyModelMap>(
                httpUtilService,
                dataMasterUrl,
                entityToModuleStateStats,
                appName) {
        this->allTgMaps = std::make_shared<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<RecencyModel>  > >();
}

std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<RecencyModel> > >
TargetGroupRecencyModelMapCacheService::getAllTgRecencyModelListMap() {
        return allTgMaps;
}

void TargetGroupRecencyModelMapCacheService::clearOtherCaches() {
        getAllTgRecencyModelListMap()->clear ();
}
std::vector<std::shared_ptr<RecencyModel> >
TargetGroupRecencyModelMapCacheService::getAllRecentModelsOfTg(int targetGroupId) {

        auto pairPtr = allTgMaps->getOptional(targetGroupId);
        if (pairPtr == nullptr) {
                std::vector<std::shared_ptr<RecencyModel> > empty;
                return empty;
        }
        return *pairPtr->values;
}

void TargetGroupRecencyModelMapCacheService::populateOtherMapsAndLists(
        std::vector<std::shared_ptr<TargetGroupRecencyModelMap> > allEntities) {
        for(auto tgMap : allEntities) {
                MLOG(2)<<"read enitty : "<< tgMap->toJson();
                addRecencyModelToCache(tgMap->targetGroupId, tgMap->model);
        }

}

void TargetGroupRecencyModelMapCacheService::
addRecencyModelToCache(int targetGroupId,
                       std::shared_ptr<RecencyModel> model) {
        auto allModels = allTgMaps->getOptional(targetGroupId);
        if (allModels == nullptr) {
                allModels = std::make_shared<ObjectVectorHolder<RecencyModel> >();
        }
        allModels->values->push_back(model);
        allTgMaps->put(targetGroupId, allModels);
}

TargetGroupRecencyModelMapCacheService::~TargetGroupRecencyModelMapCacheService() {

}
