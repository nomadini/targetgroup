#ifndef TargetGroupRecencyModelMapCacheService_h
#define TargetGroupRecencyModelMapCacheService_h

#include <string>
#include <memory>
#include <unordered_map>
#include <vector>
#include <tbb/concurrent_hash_map.h>

#include "CacheService.h"
#include "RecencyModel.h"
#include "MySqlTargetGroupRecencyModelMapService.h"
#include "ConcurrentHashMap.h"
#include "HttpUtilService.h"
#include "EntityProviderService.h"
#include "ObjectVectorHolder.h"
class EntityToModuleStateStats;

class TargetGroupRecencyModelMapCacheService : public CacheService<TargetGroupRecencyModelMap> {

private:

public:
MySqlTargetGroupRecencyModelMapService* mySqlTargetGroupRecencyModelMapService;

EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<RecencyModel> > > allTgMaps;
TargetGroupRecencyModelMapCacheService(MySqlTargetGroupRecencyModelMapService* mySqlTargetGroupRecencyModelMapService,
                                       HttpUtilService* httpUtilService,
                                       std::string dataMasterUrl,
                                       EntityToModuleStateStats* entityToModuleStateStats,
                                       std::string appName);

std::shared_ptr<gicapods::ConcurrentHashMap<int, ObjectVectorHolder<RecencyModel> > > getAllTgRecencyModelListMap();

void clearOtherCaches();

void populateOtherMapsAndLists(std::vector<std::shared_ptr<TargetGroupRecencyModelMap> > allEntities);

std::vector<std::shared_ptr<RecencyModel> > getAllRecentModelsOfTg(int targetGroupId);

void addRecencyModelToCache(int targetGroupId, std::shared_ptr<RecencyModel> model);

virtual ~TargetGroupRecencyModelMapCacheService();

};


#endif
