#include "TargetGroupProjectTestBase.h"
#include "BeanFactory.h"
#include "TargetGroupCacheService.h"
#include "TargetGroup.h"
#include "TargetGroupTestHelper.h"

TargetGroupProjectTestBase::TargetGroupProjectTestBase() {
        std::string appName = "TargetGroupProjectTestBase";
        std::string appVersion = "1";
        std::string logDirectory = "";
        std::string commonPropertyFileName = "common-test.properties";
        std::string propertyFileName = "common-test.properties";

        beanFactory = std::make_shared<BeanFactory>(appVersion);
        beanFactory->commonPropertyFileName = commonPropertyFileName;
        beanFactory->propertyFileName = propertyFileName;
        beanFactory->appName = appName;
        beanFactory->initializeModules();

}

TargetGroupProjectTestBase::~TargetGroupProjectTestBase() {

}

void TargetGroupProjectTestBase::SetUp() {

}

void TargetGroupProjectTestBase::TearDown() {

}
