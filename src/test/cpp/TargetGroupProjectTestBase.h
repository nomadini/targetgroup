//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef TargetGroupProjectTestBase_H
#define TargetGroupProjectTestBase_H

#include <gtest/gtest.h>
#include <memory>
#include <string>
#include "TestsCommon.h"
class BeanFactory;


class TargetGroupProjectTestBase : public ::testing::Test {
public:

TargetGroupProjectTestBase();

std::shared_ptr<BeanFactory> beanFactory;



virtual ~TargetGroupProjectTestBase();

void SetUp();

void TearDown();

};
#endif //TargetGroupProjectTestBase_H
