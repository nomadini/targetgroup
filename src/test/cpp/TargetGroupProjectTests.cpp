//
// Created by Mahmoud Taabodi on 3/12/16.
//

/*
 *
 *
 *  Created on: March 7, 2016
 *      Author: mtaabodi
 */

#include "GUtil.h"


#include "CassandraDriverInterface.h"
#include "ConfigService.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "TestsCommon.h"
#include "SignalHandler.h"
#include <string>
#include <memory>
#include "TempUtil.h"
#include "HttpUtil.h"

int runGoogleTests(int argc, char** argv) {
        // The following line must be executed to initialize Google Mock
        // (and Google Test) before running the tests.
        ::testing::InitGoogleMock(&argc, argv);
        ::testing::FLAGS_gmock_verbose ="info";

        return RUN_ALL_TESTS();
}


int main(int argc, char** argv) {


        TempUtil::configureLogging("TargetGroupProjectTests", argv);

        runGoogleTests(argc, argv);

}
