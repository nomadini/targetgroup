//
// Created by mtaabodi on 7/13/2016.
//
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TargetGroupRecencyModelMapCacheServiceTest.h"
#include "TargetGroupRecencyModelMapCacheService.h"
#include "RecencyModelCacheService.h"
#include "EventLog.h"
#include "BeanFactory.h"
#include "CollectionUtil.h"
#include "HttpUtilServiceMock.h"
#include "AdvertiserTestHelper.h"
#include "Advertiser.h"

TargetGroupRecencyModelMapCacheServiceTest::TargetGroupRecencyModelMapCacheServiceTest() {

        targetGroupRecencyModelMapCacheService = beanFactory->targetGroupRecencyModelMapCacheService.get();
}

TargetGroupRecencyModelMapCacheServiceTest::~TargetGroupRecencyModelMapCacheServiceTest() {

}


void TargetGroupRecencyModelMapCacheServiceTest::SetUp() {
}

void TargetGroupRecencyModelMapCacheServiceTest::TearDown() {

}

TEST_F(TargetGroupRecencyModelMapCacheServiceTest, testReloadingCaches) {

        // targetGroupRecencyModelMapCacheService->reloadCaches();
}
