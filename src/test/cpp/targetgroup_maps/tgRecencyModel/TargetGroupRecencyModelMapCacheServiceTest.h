//
// Created by mtaabodi on 4/27/2017.
//

#ifndef TargetGroupRecencyModelMapCacheServiceTest_H
#define TargetGroupRecencyModelMapCacheServiceTest_H



#include "Advertiser.h"
class MySqlDriver;
#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "TargetGroupProjectTestBase.h"
#include "Advertiser.h"
class TargetGroupRecencyModelMapCacheService;
#include "HttpUtilServiceMock.h"

class TargetGroupRecencyModelMapCacheServiceTest : public TargetGroupProjectTestBase {

private:

public:

TargetGroupRecencyModelMapCacheService* targetGroupRecencyModelMapCacheService;

void SetUp();

void TearDown();

TargetGroupRecencyModelMapCacheServiceTest();

virtual ~TargetGroupRecencyModelMapCacheServiceTest();

};
#endif //EXCHANGESIMULATOR_MYSQLCREATIVESERVICETEST_H
